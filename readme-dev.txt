== Use and Configure Yii
Download yiiframework.com and put it in the same folder as the app (htdocs)
	/yii
	/e-vend
and make sure the $yii variable in index.php is pointing to it correctly


== Connectionstring 
For development in protected/config/AppEnv.php
we can have code that checks hostName (when $dom == localhost) at the end of init()
You will want to set
	self::$data[$dom] for both [db] and [testEmail]	

== Create the folders (not in git)
	assets
	protected/runtime
	protected/runtime/uploads

== Development Environment
	change change scripts are in protected/sql
	see local-dump.sql to get started
	use PsPad for opening codebase (see dim.ppr) - files are in various places, but organized by modules

== Deployment
	Dont deploy any product images
