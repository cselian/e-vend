<?php
/**
 * CKeypad class file.
 *
 * @author Imran Ali Namazi<imran@cselian.com>
 */

class CKeypad extends CWidget
{
	public function run()
	{
		$js = '  $(".numedit").keypad({ showAnim: "fade", keypadOnly: false, showOn: "button" });';

		$cs = Yii::app()->getClientScript();
		
		$assets = Yii::app()->getAssetManager()->publish(dirname(__FILE__).DIRECTORY_SEPARATOR.'assets');
		$cs->registerCssFile($assets.'/jquery.keypad.css');
		$cs->registerScriptFile($assets.'/jquery.keypad.pack.js',CClientScript::POS_HEAD);
		
		$cs->registerScript(__CLASS__.'#'.$id, $js);
	}
}
