<?php
/**
 * @author Imran Ali Namazi <imran@cselian.com>
 * @link http://tg.cselian.com/showcase/e-vend
 * @copyright Copyright &copy; 2012-2013 cselian.com
 * @license http://tg.cselian.com/licenses/attribution (Cselian Attribution)
 * Virtual Payment Client (Mastercard / Amex) Yii Extension
 * based on the sample by Dialect Holdings (see attached copyright.txt)
 * A sample showing the usage will make it to a gist repo someday
*/

require('PaymentCodesHelper.php');

class PaymentClient
{
	private static function GetConfig()
	{
		return AppEnv::get('vpc', 'Payment Gateway Details');
	}

	public static function GetPGs()
	{
		$r = array();
		$cfg = self::GetConfig();
		$names = AppBuyers::ifConfiguredPGNames($cfg['names']);
		foreach ($names as $key)
			$r[$key] = $cfg[$key]['name'];
		return $r;
	}

	public static function GetSendUrl($order, $url, $pgName, $return = 0)
	{
		$cfg = self::GetConfig();
		
		$amt = $order->amount * AppEnv::valOrDefault($cfg, 'factor', 1); //values upto 5 lakhs will be brought to within 1000 as per test guidelines
		if (isset($cfg['round'])) $amt = round($amt, 0); //apply rounding before paisa
		if (isset($cfg['under'])) $amt = $amt % $cfg['under'];
		$amt = $amt * 100; // in paisa
		//if ($pgName == 'amex') $amt = 100; // 1 Rs to test Production codes
		
		$pg = $cfg[$pgName];
		
		$data = array(
			'Title' => 'PHP VPC 3-Party',
			'vpc_Version' => '1',
			'vpc_Command' => 'pay',
			'vpc_AccessCode' => $pg['access'],
			'vpc_MerchTxnRef' => AppEnv::valOrDefault($cfg, 'txnPrefix', '') . $order->id, //Merchant Transaction Reference
			'vpc_Merchant' => $pg['merchant'],
			'vpc_OrderInfo' => 'Order' . $order->id, // cannot have special character so drop the #
			'vpc_Amount' =>  $amt,
			'vpc_Locale' => 'en',
			'vpc_ReturnURL' => $url,
			//'vpc_TxSourceSubType' => 'SINGLE' // Indicates a single payment to complete order
			// kindly do not pass - It is an optional parameter to be used by merchants from specific industries only.
		);
		
		// *********************
		// START OF MAIN PROGRAM
		// *********************
		
		// Define Constants
		// ----------------
		// This is secret for encoding the MD5 hash
		// This secret will vary from merchant to merchant
		// To not create a secure hash, let SECURE_SECRET be an empty string - ""
		$SECURE_SECRET = $pg['secret'];
		
		// add the start of the vpcURL querystring parameters
		$vpcURL = $pg['url'] . "?";
		
		// The URL link for the receipt to do another transaction.
		// Note: This is ONLY used for this example and is not required for 
		// production code. You would hard code your own URL into your application.
		
		// Get and URL Encode the AgainLink. Add the AgainLink to the array
		// Shows how a user field (such as application SessionIDs) could be added
		//$_POST['AgainLink']=urlencode($HTTP_REFERER);
		
		// Create the request to the Virtual Payment Client which is a URL encoded GET
		// request. Since we are looping through all the data we may as well sort it in
		// case we want to create a secure hash and add it to the VPC data if the
		// merchant secret has been provided.
		$md5HashData = $SECURE_SECRET;
		ksort ($data);
		
		// set a parameter to show the first pair in the URL
		$appendAmp = 0;
		
		foreach($data as $key => $value)
		{
			// create the md5 input and URL leaving out any fields that have no value
			if (strlen($value) == 0) continue;

			if ($appendAmp) $vpcURL .= '&'; else $appendAmp = 1;
			
			$vpcURL .= urlencode($key) . "=" . urlencode($value);
			$md5HashData .= $value;
		}
		
		// Create the secure hash and append it to the Virtual Payment Client Data if
		// the merchant secret has been provided.
		if (strlen($SECURE_SECRET) > 0)
			$vpcURL .= "&vpc_SecureHash=" . strtoupper(md5($md5HashData));
		
		// FINISH TRANSACTION - Redirect the customers using the Digital Order
		// ===================================================================
		//header("Location: ".$vpcURL);
		return $vpcURL;
	}
	
	private static function getResponseDescription($responseCode) {
	
		switch ($responseCode) {
			case "0" : $result = "Transaction Successful"; break;
			case "?" : $result = "Transaction status is unknown"; break;
			case "1" : $result = "Unknown Error"; break;
			case "2" : $result = "Bank Declined Transaction"; break;
			case "3" : $result = "No Reply from Bank"; break;
			case "4" : $result = "Expired Card"; break;
			case "5" : $result = "Insufficient funds"; break;
			case "6" : $result = "Error Communicating with Bank"; break;
			case "7" : $result = "Payment Server System Error"; break;
			case "8" : $result = "Transaction Type Not Supported"; break;
			case "9" : $result = "Bank declined transaction (Do not contact Bank)"; break;
			case "A" : $result = "Transaction Aborted"; break;
			case "C" : $result = "Transaction Cancelled"; break;
			case "D" : $result = "Deferred transaction has been received and is awaiting processing"; break;
			case "F" : $result = "3D Secure Authentication failed"; break;
			case "I" : $result = "Card Security Code verification failed"; break;
			case "L" : $result = "Shopping Transaction Locked (Please try the transaction again later)"; break;
			case "N" : $result = "Cardholder is not enrolled in Authentication scheme"; break;
			case "P" : $result = "Transaction has been received by the Payment Adaptor and is being processed"; break;
			case "R" : $result = "Transaction was not processed - Reached limit of retry attempts allowed"; break;
			case "S" : $result = "Duplicate SessionID (OrderInfo)"; break;
			case "T" : $result = "Address Verification Failed"; break;
			case "U" : $result = "Card Security Code Failed"; break;
			case "V" : $result = "Address Verification and Card Security Code Failed"; break;
			default  : $result = "Unable to be determined"; 
		}
		return $result;
	}
	
	
	
	//  -----------------------------------------------------------------------------
	
	// This method uses the verRes status code retrieved from the Digital
	// Receipt and returns an appropriate description for the QSI Response Code
	
	// @param statusResponse String containing the 3DS Authentication Status Code
	// @return String containing the appropriate description
	
	private static function getStatusDescription($statusResponse) {
		if ($statusResponse == "" || $statusResponse == "No Value Returned") {
			$result = "3DS not supported or there was no 3DS data provided";
		} else {
			switch ($statusResponse) {
				Case "Y"  : $result = "The cardholder was successfully authenticated."; break;
				Case "E"  : $result = "The cardholder is not enrolled."; break;
				Case "N"  : $result = "The cardholder was not verified."; break;
				Case "U"  : $result = "The cardholder's Issuer was unable to authenticate due to some system error at the Issuer."; break;
				Case "F"  : $result = "There was an error in the format of the request from the merchant."; break;
				Case "A"  : $result = "Authentication of your Merchant ID and Password to the ACS Directory Failed."; break;
				Case "D"  : $result = "Error communicating with the Directory Server."; break;
				Case "C"  : $result = "The card type is not supported for authentication."; break;
				Case "S"  : $result = "The signature on the response received from the Issuer could not be validated."; break;
				Case "P"  : $result = "Error parsing input from Issuer."; break;
				Case "I"  : $result = "Internal Payment Server system error."; break;
				default   : $result = "Unable to be determined"; break;
			}
		}
		return $result;
	}
	
	//  -----------------------------------------------------------------------------
	   
	// If input is null, returns string "No Value Returned", else returns input
	private static function null2unknown($data) {
		if ($data == "") {
			return "No Value Returned";
		} else {
			return $data;
		}
	}
	
	public static function GetReturnData($data, $pg)
	{
		$cfg = self::GetConfig();
		$pg = $cfg[$pg];
		$SECURE_SECRET = $pg['secret'];
		
		// If there has been a merchant secret set then sort and loop through all the
		// data in the Virtual Payment Client response. While we have the data, we can
		// append all the fields that contain values (except the secure hash) so that
		// we can create a hash and validate it against the secure hash in the Virtual
		// Payment Client response.
		
		// NOTE: If the vpc_TxnResponseCode in not a single character then
		// there was a Virtual Payment Client error and we cannot accurately validate
		// the incoming data from the secure hash. */
		
		// get and remove the vpc_TxnResponseCode code from the response fields as we
		// do not want to include this field in the hash calculation
		$vpc_Txn_Secure_Hash = $data["vpc_SecureHash"];
		unset($data["vpc_SecureHash"]); 
		
		// set a flag to indicate if hash has been validated
		$errorExists = false;
		
		$hashOk = 0;
		
		if (strlen($SECURE_SECRET) > 0 && $data["vpc_TxnResponseCode"] != "7" && $data["vpc_TxnResponseCode"] != "No Value Returned") {
		
			$md5HashData = $SECURE_SECRET;
		
			// sort all the incoming vpc response fields and leave out any with no value
			foreach($data as $key => $value) {
				if ($key != "vpc_SecureHash" or strlen($value) > 0) {
					$md5HashData .= $value;
				}
			}
			
			// Validate the Secure Hash (remember MD5 hashes are not case sensitive)
			// This is just one way of displaying the result of checking the hash.
			// In production, you would work out your own way of presenting the result.
			// The hash check is all about detecting if the data has changed in transit.
			if (strtoupper($vpc_Txn_Secure_Hash) == strtoupper(md5($md5HashData))) {
				// Secure Hash validation succeeded, add a data field to be displayed
				// later.
				$hashValidated = "<FONT color='#00AA00'><strong>CORRECT</strong></FONT>";
				$hashOk = 1;
			} else {
				// Secure Hash validation failed, add a data field to be displayed
				// later.
				//echo $vpc_Txn_Secure_Hash . ' <- Given / Reqd -> ' . strtoupper(md5($md5HashData));
				$hashValidated = "<FONT color='#FF0066'><strong>INVALID HASH</strong></FONT>";
				$errorExists = true;
			}
		} else {
			// Secure Hash was not validated, add a data field to be displayed later.
			$hashValidated = "<FONT color='orange'><strong>Not Calculated - No 'SECURE_SECRET' present.</strong></FONT>";
		}
		
		// Define Variables
		// ----------------
		// Extract the available receipt fields from the VPC Response
		// If not present then let the value be equal to 'No Value Returned'
		
		// Standard Receipt Data
		$amount          = self::null2unknown($data["vpc_Amount"]);
		$locale          = self::null2unknown($data["vpc_Locale"]);
		$batchNo         = self::null2unknown($data["vpc_BatchNo"]);
		$command         = self::null2unknown($data["vpc_Command"]);
		$message         = self::null2unknown($data["vpc_Message"]);
		$version         = self::null2unknown($data["vpc_Version"]);
		$cardType        = self::null2unknown($data["vpc_Card"]);
		$orderInfo       = self::null2unknown($data["vpc_OrderInfo"]);
		$receiptNo       = self::null2unknown($data["vpc_ReceiptNo"]);
		$merchantID      = self::null2unknown($data["vpc_Merchant"]);
		$authorizeID     = self::null2unknown($data["vpc_AuthorizeId"]);
		$merchTxnRef     = self::null2unknown($data["vpc_MerchTxnRef"]);
		$transactionNo   = self::null2unknown($data["vpc_TransactionNo"]);
		$acqResponseCode = self::null2unknown($data["vpc_AcqResponseCode"]);
		$txnResponseCode = self::null2unknown($data["vpc_TxnResponseCode"]);
		
		
		// 3-D Secure Data
		$verType         = array_key_exists("vpc_VerType", $data)          ? $data["vpc_VerType"]          : "No Value Returned";
		$verStatus       = array_key_exists("vpc_VerStatus", $data)        ? $data["vpc_VerStatus"]        : "No Value Returned";
		$token           = array_key_exists("vpc_VerToken", $data)         ? $data["vpc_VerToken"]         : "No Value Returned";
		$verSecurLevel   = array_key_exists("vpc_VerSecurityLevel", $data) ? $data["vpc_VerSecurityLevel"] : "No Value Returned";
		$enrolled        = array_key_exists("vpc_3DSenrolled", $data)      ? $data["vpc_3DSenrolled"]      : "No Value Returned";
		$xid             = array_key_exists("vpc_3DSXID", $data)           ? $data["vpc_3DSXID"]           : "No Value Returned";
		$acqECI          = array_key_exists("vpc_3DSECI", $data)           ? $data["vpc_3DSECI"]           : "No Value Returned";
		$authStatus      = array_key_exists("vpc_3DSstatus", $data)        ? $data["vpc_3DSstatus"]        : "No Value Returned";
		
		// *******************
		// END OF MAIN PROGRAM
		// *******************
		
		// FINISH TRANSACTION - Process the VPC Response Data
		// =====================================================
		// For the purposes of demonstration, we simply display the Result fields on a
		// web page.
		
		// Show 'Error' in title if an error condition
		$errorTxt = "";
		
		$txnResponseMsg = PaymentCodesHelper::getResultDescription($txnResponseCode);
		$authOk = $authStatus == "Y" || $authStatus == "A"; // vpc_3DSstatus = authStatus
		$txnOk = $txnResponseCode == "0";
		$ok = $txnOk && $authOk;
		if ($txnOk && !$authOk) $message = PaymentCodesHelper::getResultDescription($authStatus);
		if (!$ok)
			$message = 'The payment request failed with the message: ' . $message
				. (isset($pg['capture']) ? '<br/>Capture not attempted due to this Authorisation Failure.' : '');
		
		// Show this page as an error page if vpc_TxnResponseCode equals '7'
		if ($txnResponseCode == "7" || $txnResponseCode == "No Value Returned" || $errorExists) {
			$errorTxt = "Error ";
		}
			
		// This is the display title for 'Receipt' page 
		$title = $data["Title"];
		
		$ret = compact('hashOk', 'hashValidated', 'errorExists',
			'amount', 'locale', 'batchNo', 'command', 'message', 'version', 'cardType', 'orderInfo', 
			'receiptNo', 'merchantID', 'authorizeID', 'merchTxnRef', 'transactionNo', 'acqResponseCode', 'txnResponseCode', 'txnResponseMsg',
			'verType', 'verStatus', 'token', 'verSecurLevel', 'enrolled', 'xid', 'acqECI', 'authStatus', 'errorTxt'
		);
		
		if (isset($pg['capture']) && $ok)
		{
			$capture = self::Capture($pg, $ret);
			$captureOk = $capture['txnResponseCode'] == "0" && ($capture['authStatus'] == "Y" || $capture['authStatus'] == "A");
			if (!$captureOk)
			{
				$ret['authorizeMsg'] = $message; //if capture fails, store the authorizeMsg separately
				$message = 'The capture request failed with the message: ' . $capture['message'];
			}
			$ret['capture'] = $capture;
		}
		else if (isset($pg['capture']))
		{
			$ret['capture'] = array('status' => 'Capture not attempted due to this Authorisation Failure');
		}
		
		return $ret;
	}
	
	public static function AddPayTestMessage()
	{
		$cfg = self::GetConfig();
		$msg = AppEnv::valOrDefault($cfg, 'testMsg', 0);
		if ($msg) FlashMessages::AddCurrent($msg);
	}
	
	public static function GetDetails($data, $pg)
	{
		$vars = array('batchNo', 'cardType', 'receiptNo', 'merchantID', 'authorizeID', 'merchTxnRef', 'transactionNo', 'xid', 'txnResponseCode', 'txnResponseMsg', 'authStatus', 'authorizeMsg');
		$details = array('pg' => $pg);
		foreach ($vars as $var)
		{
			if (!isset($data[$var])) continue;
			$details[$var] = $data[$var];
		}
		
		if (isset($data['capture']))
		{
			foreach ($data['capture'] as $key=>$val)
			{
				$details['capture-' . $key] = $val;
			}
		}
		
		return $details;
	}
	
	public static function GetDummyLinks()
	{
		$cfg = self::GetConfig();
		$dummy = AppEnv::valOrDefault($cfg, 'dummy', 0);
		if (!$dummy) return 0;
		
		$dlinks = '
';
		foreach ($dummy as $txt=>$url)
			$dlinks .= CHtml::link($txt, '', array('href' => $url)) . ' 
';
		
		return $dlinks;
	}
	
	// based on Amex\Virtual Payment Client Sample Code\PHP\Combined_Auth_and_Capture\2Party\PHP_VPC_2Party_Auth_Capture_Order.php
	private static function Capture($pg, $ret)
	{
		include_once 'VPCPaymentConnection.php';
		$conn = new VPCPaymentConnection();
		
		$flds = array(
			"vpc_Version" => "1",
			"vpc_Command" => "capture",
			"vpc_AccessCode" => $pg['access'],
			"vpc_MerchTxnRef" => $ret['merchTxnRef'],
			"vpc_Merchant" => $pg['merchant'],
			"vpc_TransNo" => $ret['transactionNo'],
			"vpc_Amount" => $ret['amount'],
			"vpc_User" => base64_decode($pg['capture']['username']),
			"vpc_Password" => base64_decode($pg['capture']['password']),
		);
		//print_r($flds);

		foreach($flds as $key => $value)
		{
			if (strlen($value) > 0)
				$conn->addDigitalOrderField($key, $value);
		}
		
		$conn->sendMOTODigitalOrder($pg['capture']['url']);
		
		$error = $conn->getErrorMessage();
		$resp = $conn->getResultField('vpc_TxnResponseCode');
		return array(
			'message' => strlen($error) == 0 ? $conn->getResultField("vpc_Message") : $error,
			'authStatus' => $conn->getResultField('vpc_3DSstatus'),
			'txnResponseCode' => $resp,
			'txnResponseDesc' => PaymentCodesHelper::getResultDescription($resp),
		);
	}
}
