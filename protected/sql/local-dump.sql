/*
SQLyog Community Edition- MySQL GUI v6.07
Host - 5.1.37 : Database - dim
*********************************************************************
Server version : 5.1.37
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;



/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

/*Table structure for table `addresses` */

DROP TABLE IF EXISTS `addresses`;

CREATE TABLE `addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `buyer_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `address` varchar(255) NOT NULL,
  `pin` varchar(6) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `lat` decimal(9,6) NOT NULL DEFAULT '0.000000',
  `lon` decimal(9,6) NOT NULL DEFAULT '0.000000',
  `assigned_to_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_addresses_buyer` (`buyer_id`),
  KEY `fk_addresses_assigned` (`assigned_to_id`),
  KEY `fk_addresses_user` (`user_id`),
  CONSTRAINT `fk_addresses_assigned` FOREIGN KEY (`assigned_to_id`) REFERENCES `distributors` (`id`),
  CONSTRAINT `fk_addresses_buyer` FOREIGN KEY (`buyer_id`) REFERENCES `buyers` (`id`),
  CONSTRAINT `fk_addresses_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `addresses` */

insert  into `addresses`(`id`,`buyer_id`,`user_id`,`name`,`address`,`pin`,`phone`,`lat`,`lon`,`assigned_to_id`) values (2,1,1,'Imran','hfgh','600034','9566166880','0.000000','0.000000',1),(3,1,22,'home','sdfsdq','434532','fddsfsdf','0.000000','0.000000',1);

/*Table structure for table `buyers` */

DROP TABLE IF EXISTS `buyers`;

CREATE TABLE `buyers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_added` datetime NOT NULL,
  `name` varchar(100) NOT NULL,
  `type` int(3) NOT NULL,
  `contact` varchar(100) NOT NULL,
  `notify` varchar(512) NOT NULL DEFAULT '',
  `address` varchar(255) NOT NULL,
  `pin` varchar(6) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `lat` decimal(9,6) NOT NULL DEFAULT '0.000000',
  `lon` decimal(9,6) NOT NULL DEFAULT '0.000000',
  `assigned_to_id` int(11) DEFAULT NULL,
  `outstanding` decimal(9,2) NOT NULL DEFAULT '0.00',
  `tax_info` varchar(512) DEFAULT NULL COMMENT 'CST Number etc',
  `priced_only` tinyint(1) NOT NULL DEFAULT '0',
  `approval_required` tinyint(1) NOT NULL DEFAULT '0',
  `payment_types` varchar(32) NOT NULL DEFAULT '1',
  `signup_from` varchar(1024) NOT NULL COMMENT 'Domains to sign up from',
  `sponsored` tinyint(1) NOT NULL DEFAULT '0',
  `purge` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_buyers_assigned` (`assigned_to_id`),
  CONSTRAINT `fk_buyers_assigned` FOREIGN KEY (`assigned_to_id`) REFERENCES `distributors` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `buyers` */

insert  into `buyers`(`id`,`date_added`,`name`,`type`,`contact`,`notify`,`address`,`pin`,`phone`,`mobile`,`lat`,`lon`,`assigned_to_id`,`outstanding`,`tax_info`,`priced_only`,`approval_required`,`payment_types`,`signup_from`,`sponsored`,`purge`) values (1,'2012-12-29 13:57:43','tcs',3,'john','imran@cselian.com; shoaibkhaleeli@gmail.com','#34 sankey road, high grounds','560001','404','405','12.900000','77.483330',1,'0.00','b1\'s tax info',0,1,'1,2','fk.com, gm.com',0,1),(2,'2013-09-23 22:41:21','Infosys',2,'john','silly-us@co.com','addy','560007','566772211','','0.000000','0.000000',1,'0.00','',0,0,'1','us.com',0,0);

/*Table structure for table `distributors` */

DROP TABLE IF EXISTS `distributors`;

CREATE TABLE `distributors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `date_added` datetime NOT NULL,
  `office_email` varchar(100) NOT NULL,
  `address` varchar(255) NOT NULL,
  `pin` varchar(6) NOT NULL,
  `lat` decimal(9,6) NOT NULL,
  `lon` decimal(9,6) NOT NULL,
  `tax_info` varchar(512) DEFAULT NULL,
  `payment_info` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `distributors_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `distributors` */

insert  into `distributors`(`id`,`name`,`date_added`,`office_email`,`address`,`pin`,`lat`,`lon`,`tax_info`,`payment_info`) values (1,'Aikon Mobile','2012-12-29 13:57:07','ramesh@Aikoncellular.com','somewhere','560001','13.055566','77.618530','aikon tax info','SBI #123, Frazer town branch');

/*Table structure for table `help` */

DROP TABLE IF EXISTS `help`;

CREATE TABLE `help` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(128) NOT NULL,
  `topic` varchar(256) NOT NULL,
  `keywords` varchar(512) NOT NULL,
  `description` varchar(512) NOT NULL,
  `content` varchar(4096) NOT NULL,
  `accessible_to` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

/*Data for the table `help` */

insert  into `help`(`id`,`slug`,`topic`,`keywords`,`description`,`content`,`accessible_to`) values (1,'/admin/importing','Admin Importing','import,product,manage,pricing,buyer,distributor','Describes how data is imported into the application','TBD','1'),(2,'/buyer/place-order','Placing Orders','order,checkout','Describes how to place orders','Once you login, you can go to View > Products\r\n\r\nFilter products if you like and enter the quantity and click submit below to add to the cart.\r\n\r\nYou can run different searches and each time add different items to the cart\r\n\r\nFinally, when you are done, go to the cart and review your items.\r\nYou can change the quantity / remove items.\r\n\r\nChoose an address for delivery, pick a payment methods and hit place order.\r\nYou will get a mail once the order is placed.','3'),(3,'/distributor/order','Distributor Order Updating','order,distributor','Describes the changes a distributor can make to the orders','You must confirm an order by doing the action \"begin order\". \r\n\r\nLater, you can \"despatch\" order to let the buyer know to expect it shortly / collect it.\r\n\r\n\"Verify and deliver requires you to collect the verification from the buyer which must also be entered.\r\n\r\nDespatched Date is set when the status is changed to despatched and should contain your reference number too.\r\n\r\nYou can enter payment reference / date only if not paid by gateway.','2,1'),(4,'/buyer/signup','Signing up as Buyer','signup,user,create','Signup process for employees of a Buyer','Typically, a Corporate employee (buyer) signs up on their own. It is expected that their company email id will be used and this must match the domain registered with us for that buyer.\r\n\r\nIn case you get the message \"No buyer has been configured to signup from domain X\", please contact our administrator and we will create an account for you.\r\n\r\nAn activation key will be sent by email to validate the account.','-2,3'),(5,'buyer/introduction','Introduction (Buyer)','intro,buyer','What kinds of buyers are there, how do they signup and place orders','We deal with several types of Buyers viz:\r\nETraders - an online reseller.\r\nCorporates - a company thats wants to buy in bulk for its employees.\r\nB2B - a company that arranges for its employees to get a discount.\r\nCustomer - a buyer who comes directly to our site.\r\n\r\nETraders and Corporates will be given their own accounts when signing on with us.\r\n\r\nB2B Employees and Customers will have to go through our signup / verification process using their company email ids.\r\n\r\nPlease note that we have different pricings for different buyers.\r\n\r\nOnce a Buyer signs in, he will be able to go to our products page (see all / search), enter a quantity and add to cart.\r\nHe is told what % discount he is getting on the MRP.\r\n\r\nHe then proceeds to confirm the order, mentioning his address. He can choose the Office Address in case of an employee or add a new one.\r\n\r\nFor payment type, depending on what is allowed for the Buyer, he can choose from COD, Payment Gateway or NEFT.\r\n\r\nUntil the despatcher picks up the order, he will have the option of cancelling the order, but all this is dealt with in this help topic.','3'),(6,'/buyers/site','Buyer Site','buyer','How to give a user access to the buyer site without logging in','In the Buyers list, there is a Buyer Site link.\r\nThis is the one that has to be shared with buyers.\r\n\r\nTo prevent people gaining access, the link uses a combination of buyer id and name. And to prevent anyone from guessing it, a hash is included. Whenever the name of the buyer changes, the new valid link has to be given.\r\nFor example.\r\nhttp://localhost/dim/?buyer=etrader1&id=1&hash=Nzkz\r\n\r\nWhen a user clicks this, he gets the buyer logo and is directed to the products page. He can add to cart, but has to signup / login before he can place the order.','1'),(7,'admin/pictures','Product and Buyer Pictures','product,buyer,pictures','How to upload product pictures and buyer logos.','If you go to <a href=\"{baseurl}/img/p/ft.php\">products resources</a>, you can add pictures for each model of the format brand-model.jpg (height 90px)\r\n\r\nFor alternate images (shown in product view), you can add jpgs to the folder /img/p/modelname where modelname is in lowercase.\r\n\r\nFor buyer logos, you need to administer them in <a href=\"{baseurl}/img/logos/ft.php\">logo resources</a> of the format buyer-id.jpg (height 30px)','1'),(8,'admin/specs','Specifications for Product','specs,product,report,config','Describes how specs are maintained and the report which double checks what is filled / extra','Specifications for products are maintained <a href=\"https://docs.google.com/a/cselian.com/spreadsheet/ccc?key=0Ao5xd3N1fLModFJzd1hZSGNhM1ZoTFdMSWtFRU9KZHc#gid=0\">in excel</a> and has to be imported into the app by the Developer (data\\specs.tsv).\r\n\r\nThese are shown in the products view page.\r\n\r\nA <a href=\"{baseurl}/reports/run/?name=products-no-specs\" target=\"_new\">special report</a> has been developed which shows product images, which are available and which specifications correspond to products which are not present (usually a typo)','1'),(9,'buyer/products','Viewing Products','product,buying,cart','How to add view products / add them to the cart','The product view shows all available models and the colours available for each model.\r\n\r\nYou can search by model name / filter by smart phone / plain phones.\r\n\r\n<h3>Selecting Colours</h3>\r\nColours can be entered by selecting a colour from the dropdown and entering the quantity (quantity becomes editable).\r\n\r\nIn case multiple colours are wanted, the \"Colours\" value is set in the dropdown and the quantity box is clicked. Then a quantity for each colour can be entered.\r\n\r\nOnly the available colours (In Stock) will be shown\r\n\r\n<h3>Pricing</h3>\r\nPricing is shown as per the discounts available to you (buyer). An indication of MRP, your price, the % saved and the Saving amount is made. Please note that VAT is applied in the price.\r\n\r\n<h3>Detail</h3>\r\nClicking on the picture or model name takes you to a product page where the image (and other images) is shown more clearly and specifications of that model are included\r\n','1,3'),(10,'admin/defaults','Default Distributor','config,distributor,default','How to set default distributor / where its used.','Distributor is linked to address, order and buyer. A default can be set (by the developer) and is available in the Administer > Site > View Configurations screen.\r\n\r\nOrder despatcher (distributor) is set based on the delivery address / buyer\'s assigned_to','1');

/*Table structure for table `order_items` */

DROP TABLE IF EXISTS `order_items`;

CREATE TABLE `order_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` smallint(6) NOT NULL,
  `variation` varchar(32) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `calculation` varchar(128) NOT NULL COMMENT 'description of calculation for easy reading and so that pricing history is not required',
  `stock_nos` varchar(4096) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orderdetail_orderId` (`order_id`),
  KEY `fk_orderItems_product` (`product_id`),
  CONSTRAINT `fk_orderItems_order` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_orderItems_product` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `order_items` */

insert  into `order_items`(`id`,`order_id`,`product_id`,`quantity`,`variation`,`price`,`calculation`,`stock_nos`) values (2,1,21,1,'','5697','MRP: 7384, DP: 6153, Discount: 8.00 for Buyer','da'),(3,3,34,1,'Black','901','MRP: 1169, DP: 973, Discount: 8.00 for Buyer',''),(4,4,34,1,'Black','901','MRP: 1169, DP: 973, Discount: 8.00 for Buyer','');

/*Table structure for table `orders` */

DROP TABLE IF EXISTS `orders`;

CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `buyer_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `address_id` int(11) DEFAULT NULL,
  `billing_address_id` int(11) NOT NULL,
  `order_date` datetime NOT NULL,
  `amount` decimal(10,0) NOT NULL,
  `verification` varchar(24) NOT NULL,
  `status` tinyint(3) NOT NULL,
  `approved` tinyint(1) DEFAULT NULL,
  `archived` tinyint(1) DEFAULT '0',
  `despatched_date` datetime DEFAULT NULL,
  `despatcher_reference` varchar(128) DEFAULT NULL COMMENT 'bill / DC number',
  `despatcher_id` int(11) DEFAULT NULL,
  `payment_type` tinyint(3) NOT NULL,
  `payment_ref` varchar(128) DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL,
  `payment_confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `payment_details` varchar(2048) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_orders_buyer` (`buyer_id`),
  KEY `fk_orders_address` (`address_id`),
  KEY `FK_orders_despatcher` (`despatcher_id`),
  KEY `fk_orders_user` (`user_id`),
  KEY `fk_orders_billingAddress` (`billing_address_id`),
  CONSTRAINT `fk_orders_address` FOREIGN KEY (`address_id`) REFERENCES `addresses` (`id`),
  CONSTRAINT `fk_orders_billingAddress` FOREIGN KEY (`billing_address_id`) REFERENCES `addresses` (`id`),
  CONSTRAINT `fk_orders_buyer` FOREIGN KEY (`buyer_id`) REFERENCES `buyers` (`id`),
  CONSTRAINT `FK_orders_despatcher` FOREIGN KEY (`despatcher_id`) REFERENCES `distributors` (`id`),
  CONSTRAINT `fk_orders_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `orders` */

insert  into `orders`(`id`,`buyer_id`,`user_id`,`address_id`,`billing_address_id`,`order_date`,`amount`,`verification`,`status`,`approved`,`archived`,`despatched_date`,`despatcher_reference`,`despatcher_id`,`payment_type`,`payment_ref`,`payment_date`,`payment_confirmed`,`payment_details`) values (1,1,1,NULL,2,'2013-09-05 21:32:47','5697','sobi',1,-1,0,NULL,NULL,1,1,NULL,NULL,0,NULL),(3,1,1,NULL,2,'2013-09-14 05:24:09','901','tixo',1,-1,1,NULL,NULL,1,1,'sdfdsf',NULL,0,NULL),(4,1,22,3,3,'2013-09-23 23:11:34','901','evef',1,-1,1,NULL,NULL,1,1,NULL,NULL,0,NULL);

/*Table structure for table `pricing` */

DROP TABLE IF EXISTS `pricing`;

CREATE TABLE `pricing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `buyer_id` int(11) DEFAULT NULL,
  `buyer_type` int(3) DEFAULT NULL,
  `discount` decimal(5,2) DEFAULT NULL COMMENT 'percentage (if not flat price)',
  `price` decimal(7,2) DEFAULT NULL COMMENT 'only for specific products',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_pricing` (`product_id`,`buyer_id`,`buyer_type`),
  KEY `fk_pricing_product` (`product_id`),
  KEY `fk_pricing_buyers` (`buyer_id`),
  CONSTRAINT `fk_pricing_buyer` FOREIGN KEY (`buyer_id`) REFERENCES `buyers` (`id`),
  CONSTRAINT `fk_pricing_product` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;

/*Data for the table `pricing` */

insert  into `pricing`(`id`,`product_id`,`buyer_id`,`buyer_type`,`discount`,`price`) values (1,1,1,NULL,NULL,'1700.00'),(2,2,1,NULL,NULL,'4300.00'),(3,3,1,NULL,NULL,'2500.00'),(4,4,1,NULL,NULL,'3600.00'),(49,NULL,1,NULL,'8.00',NULL),(50,NULL,2,NULL,'5.00',NULL);

/*Table structure for table `products` */

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brand` varchar(100) NOT NULL,
  `category` varchar(32) NOT NULL,
  `model` varchar(64) NOT NULL,
  `mrp` decimal(10,0) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `variations` varchar(255) NOT NULL,
  `specifications` varchar(2048) NOT NULL,
  `promote` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;

/*Data for the table `products` */

insert  into `products`(`id`,`brand`,`category`,`model`,`mrp`,`price`,`variations`,`specifications`,`promote`) values (1,'nokia','regular','C1-01','1900','2300','blue,warm-grey,red,dark-grey','todo',0),(2,'nokia','regular','C2-01','4399','4002','black,silver','todo',1),(3,'nokia','regular','C2-00','2767','2306','white,dark-grey','todo',0),(4,'nokia','regular','C2-02','3709','3364','chrome-black,golden-white','todo',0),(5,'nokia','regular','C2-03','4229','3844','chrome-black,golden-white','todo',0),(6,'nokia','regular','C2-06','5739','4423','graphite,golden-buff','todo',1),(7,'nokia','smart','C3-00','7729','6441','golden-white,pink,slate-grey','todo',1),(8,'nokia','smart','C5-00','9144','7620','black,warm-grey,white','todo',1),(9,'nokia','smart','C5-03','10285','8571','graphite-black,grey','todo',0),(10,'nokia','smart','C5-05','7426','6188','black-aluminium-grey,black-lilac,white-grey','todo',0),(11,'nokia','smart','C6-00','12570','10475','white,black','todo',0),(12,'nokia','smart','E5-00','9999','9143','amethyst,chrome,dark-grey,red','todo',1),(13,'nokia','smart','E6-00','20381','16984','silver,white,black','todo',1),(14,'nokia','smart','E7-00','24905','20754','dark-grey,silver-white','todo',0),(15,'nokia','smart','Lumia 710','18283','15236','white,black,blue,pink','todo',0),(16,'nokia','smart','Lumia800','28301','23584','black,blue,pink','todo',0),(17,'nokia','regular','N100','1409','1169','p.black,l.blue,o.blue','todo',1),(18,'nokia','regular','N101','1749','1453','coral-red,metallic-grey,phantom-black','todo',1),(19,'nokia','regular','N1280','1153','961','black,grey','todo',0),(20,'nokia','regular','ASHA200','4730','3942','white,pink,aqua,graphite','todo',0),(21,'nokia','smart','ASHA300','7384','6153','graphite,white,red','todo',0),(22,'nokia','smart','ASHA303','9317','7764','red,graphite','todo',0),(23,'nokia','smart','N500','11429','9524','white-silver,black-green/red,black','todo',0),(24,'nokia','smart','N603','15427','12856','white,black','todo',0),(25,'nokia','smart','N700','16000','13333','white-silver,black-grey,black','todo',0),(26,'nokia','smart','N701','21277','17731','light-silver,dark-steel','todo',0),(27,'nokia','regular','X1-01','2190','1825','dark-grey,o.blue,orange,red,white','todo',0),(28,'nokia','regular','X2-00','5768','4807','black-red,silver-blue','todo',0),(29,'nokia','regular','X2-01','4266','3555','blue,silver,deep-grey,red,bright-red,dark-silver','todo',0),(30,'nokia','smart','X3-02','9140','7617','white,dark-grey','todo',0),(31,'nokia','smart','ASHA 302','7358','6132','red,grey,white','todo',0),(32,'nokia','regular','ASHA 202','4613','3844','white,grey,black','todo',0),(33,'nokia','regular','C1-011','2300','1900','white,grey,red,teal','specs',1),(34,'Nokia','regular','1280','1169','973','Black, Grey','1280 RM-647  ',1),(35,'Nokia','regular','N112','3019','2515','D Grey, White, Red, Cyan','NOKIA 112  ',1),(36,'Nokia','regular','N114','2719','2265','Black, Cyan, D Purple','NOKIA 114  ',1),(37,'Nokia','regular','N200','4519','4106','Graphite, P White','Nokia 200  ',1),(38,'Nokia','regular','N201','4219','3804','Graphite, P White, L pink','Nokia 201  ',1),(39,'Nokia','regular','N202','4319','4000','Black, S White, D Grey','Nokia 202  ',1),(40,'Nokia','regular','N205','3799','3454','D Rose, White','NOKIA 205  ',1),(41,'Nokia','regular','N206','4019','3655','Black, White, Cyan, Black, Magenta','NOKIA 206  ',1),(42,'Nokia','smart','N302','7209','6547','D Grey, White, P red','NOKIA 302  ',1),(43,'Nokia','regular','N305','4799','4309','D Grey, Red, S White, Mid Blue','NOKIA 305  ',1),(44,'Nokia','regular','N306','4269','3866','D Grey, Red, S White, Mid Blue','NOKIA 306  ',1),(45,'Nokia','smart','N308','5679','5141','G light, Black','NOKIA 308  ',1),(46,'Nokia','regular','N309','5199','4726','Black, white','NOKIA 309  ',1),(47,'Nokia','smart','N310','6319','5491','G light, Black, White','NOKIA 310  ',1),(48,'Nokia','smart','N311','6879','6250','D Grey, Rose Red, Sand White','NOKIA 311  ',1),(49,'Nokia','smart','N510','10999','9999','Black, Red, White, Yellow','Lumia 510  ',1),(50,'Nokia','smart','N610','15019','12380','Black, White, Cyan, Magenta','LUMIA 610  ',1),(51,'Nokia','smart','N620','15999','14230','Black, White, Cyan, Yellow, Green, Magenta','NOKIA 620  ',1),(52,'Nokia','smart','N710','17959','12999','Black, White, Cyan, Fuchsia','NOKIA LUMIA 710 RM-803 ',1),(53,'Nokia','smart','N808','34499','24999','Black, white','NOKIA 808 RM-807  ',1),(54,'Nokia','smart','N820','25849','23499','Cyan, Yellow, red, White, Black','NOKIA 820 RM-826 ',1),(55,'Nokia','smart','N900','34249','31131','Black, White, Cyan','Nokia 900 RM-823  ',1),(56,'Nokia','smart','N920','41639','37512','Black, White, Yellow, Red','NOKIA 920.1 RM-821 ',1),(57,'Nokia','regular','X2-02','3599','3269','Dark Silver, Bright Red, Ocean Blue','X2-02 RM-694 ',1),(58,'nokia','regular','ACC011','2300','1900','white,grey,red,teal','<ul class=\"info g-list-none\" style=\"margin-top: 1em; margin-right: 0px; margin-bottom: 0px; padding-top: 1em; padding-bottom: 1em; padding-left: 0px; border-top-width: 1px; border-bottom-width: 1px; border-top-style: solid; border-bottom-style: solid; border-top-color: rgb(238, 238, 238); border-bottom-color: rgb(238, 238, 238); outline: 0px; font-size: 13px; list-style: none; color: rgb(119, 119, 119); font-family: Arial, sans-serif; font-style: normal; font-variant: normal; line-height: 19px;\"><li style=\"outline: 0px; font-size: 13px; background-color: transparent; list-style: none;\"><b style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background-color: transparent;\">st updated</b>: Mar 6, 2012</li><li style=\"outline: 0px; font-size: 13px; background-color: transparent; list-style: none;\"><b style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background-color: transparent;\">Tags</b>:&nbsp;<a href=\"http://www.yiiframework.com/extensions/?tag=nicedit+Yii+extensions\" style=\"outline: 0px; font-size: 13px; background-color: transparent; color: rgb(153, 204, 0); text-decoration: none;\">nicedit Yii extensions</a>,<a href=\"http://www.yiiframework.com/extensions/?tag=niceditor\" style=\"outline: 0px; font-size: 13px; background-color: transparent; color: rgb(0, 153, 204); text-decoration: none;\">niceditor</a></li></ul>',1),(59,'nokia','regular','ACC012','2300','1900','white','specs',1);

/*Table structure for table `products_old` */

DROP TABLE IF EXISTS `products_old`;

CREATE TABLE `products_old` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brand` varchar(100) NOT NULL,
  `code` varchar(64) NOT NULL,
  `model` varchar(64) NOT NULL,
  `mrp` decimal(10,0) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `description` varchar(255) NOT NULL,
  `specifications` varchar(2048) NOT NULL,
  `basic` tinyint(1) NOT NULL DEFAULT '0',
  `promote` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `material_code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=latin1;

/*Data for the table `products_old` */

insert  into `products_old`(`id`,`brand`,`code`,`model`,`mrp`,`price`,`description`,`specifications`,`basic`,`promote`) values (1,'nokia','002T427','C1-01','1900','2300','BLUE','todo',0,0),(2,'nokia','002T420','C1-01','1900','2300','DARK GREY','todo',1,0),(3,'nokia','002T430','C1-01','1900','2300','RED','todo',0,0),(4,'nokia','002T432','C1-01','1900','2300','Warm Grey','todo',0,0),(5,'nokia','002V2R4','C2-01','4615','3846','BLACK','todo',1,1),(6,'nokia','002W219','C2-00','2767','2306','DARK GREY','todo',0,0),(7,'nokia','002W309','C2-00','2767','2306','WHITE','todo',1,0),(8,'nokia','002V2R3','C2-01','4615','3846','SILVER','todo',0,0),(9,'nokia','002X4P7','C2-02','4038','3365','CHROME BLACK','todo',1,0),(10,'nokia','002X0M8','C2-02','4038','3365','GOLDEN WHITE','todo',0,0),(11,'nokia','002W4D8','C2-03','4615','3846','CHROME BLACK','todo',1,0),(12,'nokia','002W0M7','C2-03','4615','3846','GOLDEN WHITE','todo',0,0),(13,'nokia','002W4F8','C2-06','5309','4424','GOLDEN BUFF','todo',1,1),(14,'nokia','002W4F6','C2-06','5309','4424','Graphite','todo',0,0),(15,'nokia','002T4Q1','C3-00','7729','6441','GOLDEN WHITE','todo',1,1),(16,'nokia','002R738','C3-00','7729','6441','PINK','todo',0,0),(17,'nokia','002T9N3','C3-00','7729','6441','SLATE GREY','todo',0,0),(18,'nokia','002W340','C5-00','9144','7620','BLACK','todo',0,0),(19,'nokia','002W338','C5-00','9144','7620','WARM GREY','todo',1,1),(20,'nokia','002W339','C5-00','9144','7620','WHITE','todo',0,0),(21,'nokia','002V252','C5-03','10285','8571','GRAPHITE BLACK','todo',1,0),(22,'nokia','002V259','C5-03','10285','8571','GREY','todo',0,0),(23,'nokia','002Z1X0','C5-05','7426','6188','BLACK ALUMINIUM GREY','todo',1,0),(24,'nokia','002Z1X2','C5-05','7426','6188','BLACK LILAC','todo',0,0),(25,'nokia','002Z1X1','C5-05','7426','6188','WHITE GREY','todo',0,0),(26,'nokia','002S090','C6-00','12570','10475','BLACK','todo',0,0),(27,'nokia','002S0F3','C6-00','12570','10475','WHITE','todo',1,0),(28,'nokia','002X5X1','E5-00','10970','9142','AMETHYST','todo',0,0),(29,'nokia','002X5X0','E5-00','10970','9142','CHROME','todo',0,0),(30,'nokia','002X5W9','E5-00','10970','9142','DARK GREY','todo',1,1),(31,'nokia','002X5X2','E5-00','10970','9142','RED','todo',0,0),(32,'nokia','002V6M2','E6-00','20381','16984','BLACK','todo',1,1),(33,'nokia','002V6M3','E6-00','20381','16984','SILVER','todo',0,0),(34,'nokia','002V6M4','E6-00','20381','16984','White','todo',0,0),(35,'nokia','002T9F2','E7-00','24905','20754','DARK GREY','todo',1,0),(36,'nokia','002T9F6','E7-00','24905','20754','SILVER WHITE','todo',0,0),(37,'nokia','002Z0Q2','Lumia710','18283','15236','BLACK','todo',0,0),(38,'nokia','002Z0Q5','Lumia710','18283','15236','BLUE','todo',0,0),(39,'nokia','002Z0Q6','Lumia710','18283','15236','PINK','todo',0,0),(40,'nokia','002Z0Q3','Lumia710','18283','15236','WHITE','todo',1,0),(41,'nokia','002X767','Lumia800','28301','23584','BLACK','todo',1,0),(42,'nokia','002Z101','Lumia800','28301','23584','BLUE','todo',0,0),(43,'nokia','002Z217','Lumia800','28301','23584','PINK','todo',0,0),(44,'nokia','002X3S2','N100','1386','1155','L.BLUE','todo',0,0),(45,'nokia','002X3S1','N100','1386','1155','O.BLUE','todo',0,0),(46,'nokia','002Z7X5','N100','1386','1155','P.Black','todo',1,1),(47,'nokia','002X3S7','N101','1729','1441','CORAL RED','todo',1,1),(48,'nokia','002X3S5','N101','1729','1441','PHANTOM BLACK','todo',0,0),(49,'nokia','002X5M4','N101','1729','1441','METALLIC GREY','todo',0,0),(50,'nokia','002P9Z5','N1280','1153','961','BLACK','todo',1,0),(51,'nokia','002P9Z6','N1280','1153','961','GREY','todo',0,0),(52,'nokia','A00003911','ASHA200','4730','3942','AQUA','todo',1,0),(53,'nokia','A00003882','ASHA200','4730','3942','GRAPHITE','todo',0,0),(54,'nokia','A00003910','ASHA200','4730','3942','PINK','todo',0,0),(55,'nokia','A00003921','ASHA200','4730','3942','WHITE','todo',0,0),(56,'nokia','A00003281','ASHA300','7384','6153','GRAPHITE','todo',1,0),(57,'nokia','A00003280','ASHA300','7384','6153','Red','todo',0,0),(58,'nokia','A00004246','ASHA300','7384','6153','White','todo',0,0),(59,'nokia','A00004300','ASHA303','9317','7764','GRAPHITE','todo',1,0),(60,'nokia','A00004299','ASHA303','9317','7764','RED','todo',0,0),(61,'nokia','002X410','N500','11429','9524','BLACK','todo',1,0),(62,'nokia','002X8F3','N500','11429','9524','BLACK GREEN/RED','todo',0,0),(63,'nokia','002X989','N500','11429','9524','WHITE SILVER','todo',0,0),(64,'nokia','002X823','N603','15427','12856','BLACK','todo',1,0),(65,'nokia','002X824','N603','15427','12856','WHITE','todo',0,0),(66,'nokia','002X2F1','N700','16000','13333','BLACK','todo',1,0),(67,'nokia','002X2D9','N700','16000','13333','BLACK GREY','todo',0,0),(68,'nokia','002X2F0','N700','16000','13333','WHITE SILVER','todo',0,0),(69,'nokia','002W6Q7','N701','21277','17731','DARK STEEL','todo',1,0),(70,'nokia','002W6Q4','N701','21277','17731','LIGHT SILVER','todo',0,0),(71,'nokia','002X0D6','X1-01','2190','1825','DARK GREY','todo',1,0),(72,'nokia','002X158','X1-01','2190','1825','O.Blue','todo',0,0),(73,'nokia','002X159','X1-01','2190','1825','Orange','todo',0,0),(74,'nokia','002X160','X1-01','2190','1825','RED','todo',0,0),(75,'nokia','002X161','X1-01','2190','1825','White','todo',0,0),(76,'nokia','002V5X8','X2-00','5768','4807','BLACK-RED','todo',1,0),(77,'nokia','002V5Z0','X2-00','5768','4807','SILVER BLUE','todo',0,0),(78,'nokia','002T850','X2-01','4266','3555','DEEP GREY','todo',1,0),(79,'nokia','002T855','X2-01','4266','3555','RED','todo',0,0),(80,'nokia','002T856','X2-01','4266','3555','Silver','todo',0,0),(81,'nokia','002X0C1','X2-01','3924','3270','BRIGHT RED','todo',0,0),(82,'nokia','002W656','X2-01','3924','3270','DARK SILVER','todo',0,0),(83,'nokia','002X0C7','X2-01','3924','3270','BLUE','todo',0,0),(84,'nokia','002W7T9','X3-02','9140','7617','DARK GREY','todo',1,0),(85,'nokia','002W7V0','X3-02','9140','7617','White','todo',0,0),(86,'nokia','A00005243','ASHA 302','7358','6132','GREY','todo',1,0),(87,'nokia','A00005245','ASHA 302','7541','6284','RED','todo',0,0),(88,'nokia','A00005244','ASHA 302','7541','6284','WHITE','todo',0,0),(89,'nokia','A00005240','ASHA 202','4613','3844','BLACK','todo',1,0),(90,'nokia','A00005242','ASHA 202','4613','3844','GREY','todo',0,0),(91,'nokia','A00005241','ASHA 202','4613','3844','WHITE','todo',0,0),(92,'nokia','A00006160','N110','2711','2259','CYAN','todo',0,0),(93,'nokia','sometestcode','C1-011','2300','1900','white','specs',1,1);

/*Table structure for table `products_variations` */

DROP TABLE IF EXISTS `products_variations`;

CREATE TABLE `products_variations` (
  `model` varchar(64) NOT NULL,
  `variations` varchar(255) NOT NULL,
  `codes` varchar(255) NOT NULL,
  PRIMARY KEY (`model`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `products_variations` */

insert  into `products_variations`(`model`,`variations`,`codes`) values ('ASHA 202','white,grey,black','A00005241,A00005242,A00005240'),('ASHA 302','red,grey,white','A00005245,A00005243,A00005244'),('ASHA200','white,pink,aqua,graphite','A00003921,A00003910,A00003911,A00003882'),('ASHA300','graphite,white,red','A00003281,A00004246,A00003280'),('ASHA303','red,graphite','A00004299,A00004300'),('C1-01','blue,warm-grey,red,dark-grey','002T427,002T432,002T430,002T420'),('C1-011','white','sometestcode'),('C2-00','white,dark-grey','002W309,002W219'),('C2-01','black,silver','002V2R4,002V2R3'),('C2-02','chrome-black,golden-white','002X4P7,002X0M8'),('C2-03','chrome-black,golden-white','002W4D8,002W0M7'),('C2-06','graphite,golden-buff','002W4F6,002W4F8'),('C3-00','golden-white,pink,slate-grey','002T4Q1,002R738,002T9N3'),('C5-00','black,warm-grey,white','002W340,002W338,002W339'),('C5-03','graphite-black,grey','002V252,002V259'),('C5-05','black-aluminium-grey,black-lilac,white-grey','002Z1X0,002Z1X2,002Z1X1'),('C6-00','white,black','002S0F3,002S090'),('E5-00','amethyst,chrome,dark-grey,red','002X5X1,002X5X0,002X5W9,002X5X2'),('E6-00','silver,white,black','002V6M3,002V6M4,002V6M2'),('E7-00','dark-grey,silver-white','002T9F2,002T9F6'),('Lumia710','white,black,blue,pink','002Z0Q3,002Z0Q2,002Z0Q5,002Z0Q6'),('Lumia800','black,blue,pink','002X767,002Z101,002Z217'),('N100','p.black,l.blue,o.blue','002Z7X5,002X3S2,002X3S1'),('N101','coral-red,metallic-grey,phantom-black','002X3S7,002X5M4,002X3S5'),('N110','cyan','A00006160'),('N1280','black,grey','002P9Z5,002P9Z6'),('N500','white-silver,black-green/red,black','002X989,002X8F3,002X410'),('N603','white,black','002X824,002X823'),('N700','white-silver,black-grey,black','002X2F0,002X2D9,002X2F1'),('N701','light-silver,dark-steel','002W6Q4,002W6Q7'),('X1-01','dark-grey,o.blue,orange,red,white','002X0D6,002X158,002X159,002X160,002X161'),('X2-00','black-red,silver-blue','002V5X8,002V5Z0'),('X2-01','blue,silver,deep-grey,red,bright-red,dark-silver','002X0C7,002T856,002T850,002T855,002X0C1,002W656'),('X3-02','white,dark-grey','002W7V0,002W7T9');

/*Table structure for table `reports` */

DROP TABLE IF EXISTS `reports`;

CREATE TABLE `reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(32) NOT NULL,
  `name` varchar(32) NOT NULL,
  `group` varchar(64) NOT NULL,
  `description` varchar(256) NOT NULL,
  `returns` varchar(64) NOT NULL,
  `condition` varchar(256) NOT NULL,
  `accessible_to` varchar(256) NOT NULL,
  `last_count` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `reports` */

insert  into `reports`(`id`,`slug`,`name`,`group`,`description`,`returns`,`condition`,`accessible_to`,`last_count`) values (1,'prod-invalid-prices','Invalid Product Prices','Config','Shows products with prices / mrps not set','products','mrp = 0 or price = 0','superadmin',NULL),(5,'distributors-nolocation','Distributors Not on Map','Config','Shows Distributors whose lat/lon has not been set','distributors','lat = 0 or lon = 0','superadmin',NULL),(2,'address-no-despatcher','Addresses no Despatcher','No Despatcher','Shows Addresses with no Despatcher','addresses','assigned_to_id is null','superadmin',NULL),(3,'order-no-despatcher','Orders no Despatcher','No Despatcher','Shows Orders with no Despatcher','orders','despatcher_id is null','superadmin',NULL),(4,'buyer-no-despatcher','Buyers no Despatcher','No Despatcher','Shows Buyers with no Despatcher','buyers','assigned_to_id is null','superadmin',NULL),(6,'order-not-confirmed','Orders not Confirmed','Orders','Shows Orders not confirmed by the Despatcher','orders','status = 1','superadmin',NULL),(7,'order-new','New Orders for Despatcher','Orders','Shows new Orders for the current Despatcher','orders','status = 1 and despatcher_id = {distributor}','distributor',NULL),(8,'order-inprogress','In Progress (for Despatcher)','Despatcher','Shows Orders for the current Despatcher that are In Progress','orders','status = 2 and despatcher_id = {distributor}','distributor',NULL),(9,'products-no-specs','No Specifications','Config','Products with no Specifications','products/_report-specs','1 = 1 order by model','superadmin',NULL),(10,'products-pricing','Pricing Calculation','Config','Product Pricing Calculation','products/_report-pricing','1 = 1 order by model','superadmin',NULL),(11,'product-stock','Product Stock','Product','Product Stock','products/_report-stock','1 = 1 order by model','superadmin,distributor',NULL),(12,'buyer-orders','Buyer Orders','Buyer','Order report for Buyer','orders/_report-buyer','archived = 0 and buyer_id = {buyer} order by ID desc','buyeradmin',NULL);

/*Table structure for table `stock` */

DROP TABLE IF EXISTS `stock`;

CREATE TABLE `stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `distributor_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `variation` varchar(32) NOT NULL DEFAULT '',
  `balance` int(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_stock_distributor` (`distributor_id`),
  KEY `fk_stock_product` (`product_id`),
  CONSTRAINT `fk_stock_distributor` FOREIGN KEY (`distributor_id`) REFERENCES `distributors` (`id`),
  CONSTRAINT `fk_stock_product` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `stock` */

insert  into `stock`(`id`,`distributor_id`,`product_id`,`variation`,`balance`) values (1,1,34,'grey',24),(2,1,34,'Black',10),(3,1,58,'white',56),(4,1,58,'grey',44),(5,1,58,'red',54),(6,1,20,'white',-16),(7,1,20,'pink',-20);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `employee_id` varchar(100) DEFAULT '',
  `password` varchar(32) NOT NULL,
  `date_added` datetime NOT NULL,
  `type` int(3) NOT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `distributor_id` int(11) DEFAULT NULL,
  `buyer_id` int(11) DEFAULT NULL,
  `reset_code` varchar(32) DEFAULT NULL,
  `reset_date` datetime DEFAULT NULL,
  `deactive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email` (`email`),
  KEY `order_fk` (`distributor_id`),
  KEY `buyer_fk` (`buyer_id`),
  CONSTRAINT `fk_users_buyer` FOREIGN KEY (`buyer_id`) REFERENCES `buyers` (`id`),
  CONSTRAINT `fk_users_distributor` FOREIGN KEY (`distributor_id`) REFERENCES `distributors` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`employee_id`,`password`,`date_added`,`type`,`admin`,`distributor_id`,`buyer_id`,`reset_code`,`reset_date`,`deactive`) values (1,'imran','ian1@fk.com','IN386','d8a276cc59b0890d7c01f28ec54a7acd','2013-09-05 21:05:38',3,1,NULL,1,NULL,NULL,0),(19,'imran','ian@fk.com','E78','68e625ad212914ebd5791aa84bffe88b','2013-09-23 01:39:23',3,0,NULL,1,NULL,NULL,0),(20,'Ian','me@us.com','','notset','2013-09-23 22:42:37',3,0,NULL,2,'vegovaqidi2eda2uvoxi','2013-09-23 22:42:37',0),(21,'imran','ian@gmail.com','dfsdf','94b13b93d73cebee376410e6796c0171','2013-09-23 22:44:37',3,0,NULL,1,NULL,NULL,0),(22,'Uab','ua@gmail.com','sdfsd','6680a6f7746471b0a4b093f2d0310a32','2013-09-23 23:11:06',3,0,NULL,1,NULL,NULL,0),(23,'Imran us','ian@us.com','','notset','2013-10-06 00:29:58',3,0,NULL,2,'iduvi4usuxagiqu8u4aq','2013-10-06 00:29:58',0),(24,'imran','imranq@cselian.com','','notset','2013-10-06 01:06:30',3,0,NULL,NULL,'awucazukuha3eho5o9i9','2013-10-06 01:06:30',0),(25,'imran','imran@cselian.com','386','c1cbefe9d10c589d6d0dcd262d57b929','2013-10-06 01:44:02',3,0,NULL,1,NULL,NULL,0);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
