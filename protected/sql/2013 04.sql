/* April 3rd */
ALTER TABLE `addresses` ADD COLUMN `user_id` int(11) DEFAULT NULL AFTER `buyer_id`;
ALTER TABLE `addresses` ADD CONSTRAINT `fk_addresses_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/* 9th */
/* Copy products to database (complete change) */
ALTER TABLE `order_items` ADD COLUMN `variation` VARCHAR(32) NOT NULL  AFTER `quantity`;

/* 30th */
/* empty orders before running this */
ALTER TABLE `orders` ADD COLUMN `billing_address_id` int(11) NOT NULL  AFTER `address_id`;
ALTER TABLE `orders` ADD CONSTRAINT `fk_orders_billingAddress` FOREIGN KEY (`billing_address_id`) REFERENCES `addresses` (`id`);
