ALTER TABLE `users` ADD COLUMN `admin` tinyint(1) NOT NULL DEFAULT '0' AFTER `type`;

ALTER TABLE `order_items` MODIFY COLUMN  `calculation` varchar(128) NOT NULL 
COMMENT 'description of calculation for easy reading and so that pricing history is not required';

ALTER TABLE `orders` ADD COLUMN `payment_details` VARCHAR(2048) NULL  AFTER `payment_confirmed` ;

ALTER TABLE `buyers` ADD COLUMN `priced_only` TINYINT(1) NOT NULL DEFAULT 0  AFTER `tax_info` ;

/* 23rd */
ALTER TABLE `buyers` ADD COLUMN `payment_types` varchar(32) NOT NULL DEFAULT '1';

CREATE TABLE `help` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(128) NOT NULL,
  `topic` varchar(256) NOT NULL,
  `keywords` varchar(512) NOT NULL,
  `description` varchar(512) NOT NULL,
  `content` varchar(4096) NOT NULL,
  `accessible_to` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/* 27th */
ALTER TABLE `buyers` ADD COLUMN `signup_from` varchar(1024) NOT NULL COMMENT 'Domains to sign up from'
