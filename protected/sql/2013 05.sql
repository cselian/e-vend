/* May 6th */
ALTER TABLE `products` ADD COLUMN `category` VARCHAR(32) NOT NULL  AFTER `brand`;
update products set category = 'smart' where price > 5000;
update products set category = 'regular' where price < 5000;

/* May 7th */
ALTER TABLE `addresses` DROP COLUMN `mobile`;

/* May 16th */
ALTER TABLE `buyers` ADD COLUMN `approval_required` tinyint(1) NOT NULL DEFAULT 0 AFTER `priced_only`;
ALTER TABLE `buyers` ADD COLUMN `notify` varchar(512) NOT NULL DEFAULT '' AFTER `contact`;

ALTER TABLE `orders` ADD COLUMN `approved` tinyint(1) NULL  AFTER `status`;

/* May 22nd */
CREATE TABLE `stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `distributor_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `variation` varchar(32) NOT NULL default '',
  `balance` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDb DEFAULT CHARSET=latin1;
ALTER TABLE `stock` ADD CONSTRAINT `fk_stock_distributor` FOREIGN KEY (`distributor_id`) REFERENCES `distributors` (`id`);
ALTER TABLE `stock` ADD CONSTRAINT `fk_stock_product` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

insert  into `reports`(`id`,`slug`,`name`,`group`,`description`,`returns`,`condition`,`accessible_to`,`last_count`) values 
(11,'product-stock','Product Stock','Product','Product Stock','products/_report-stock','1 = 1 order by model','superadmin',NULL);

