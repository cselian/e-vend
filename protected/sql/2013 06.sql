/* June 8th */
ALTER TABLE `buyers` ADD COLUMN `sponsored` tinyint(1) NOT NULL DEFAULT 0;

ALTER TABLE `users` ADD COLUMN `deactive` tinyint(1) NOT NULL DEFAULT 0;
