/* Sep 13th */
ALTER TABLE `buyers` ADD COLUMN `purge` tinyint(1) NOT NULL DEFAULT 0;

/* Sep 22nd */
ALTER TABLE `users` ADD COLUMN `employee_id` varchar(100) NULL DEFAULT '' AFTER `email`;

/* Oct 7th */
ALTER TABLE `orders` ADD COLUMN `archived` tinyint(1) DEFAULT 0 AFTER `approved`;

/*
-- local
update orders set archived = 1 where buyer_id = 1 and id < 4
--beta
update orders set archived = 1 where buyer_id = 4 and id <= 100
--live
update orders set archived = 1 where buyer_id = 2 and id <= 135
*/
