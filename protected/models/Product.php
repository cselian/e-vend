<?php
/**
 * @author Imran Ali Namazi <imran@cselian.com>
 * @link http://tg.cselian.com/showcase/e-vend
 * @copyright Copyright &copy; 2012-2013 cselian.com
 * @license http://tg.cselian.com/licenses/e-vend
 */

/**
 * This is the model class for table "products".
 *
 * The followings are the available columns in table 'products':
 * @property integer $id
 * @property string $brand
 * @property string $category
 * @property string $model
 * @property string $mrp
 * @property string $price
 * @property string $variations
 * @property string $specifications
 * @property integer $promote
 *
 * The followings are the available model relations:
 * @property OrderItem[] $orderItems
 * @property Pricing[] $pricings
 */
class Product extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Product the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function format($data)
	{
		if (is_null($data)) return '';
		return sprintf('<span class="prod-model">%s</span>', $data->model);
	}
	
	public function imageAndLink()
	{
		//Used in OrderItem. Dont put in span since tooltip requires product.js
		//previously in products/_single.php
		return ImgHelper::product_img($this) .
			' ' . CHtml::link('(view)', array('/products/view', 'id'=>$this->id));
	}

	public function adjustVariations($for = 'view')
	{
		if ($for == 'view') {
			$list = explode(',', $this->variations);
			foreach ($list as $k=>$v) $list[$k] = self::variation_r($v);
			$this->variations = implode(' / ', $list);
		} else if ($for == 'save') {
		} else if ($for == 'edit') {
		} else if ($for == 'array') {
			return explode(',', $this->variations);
		}
	}
	
	public static function variation_r($v)
	{
		return ucwords(str_replace('-', ' ', $v));
	}
	
	public static function getAllSpecifications()
	{
		$specs = Yii::app()->controller->site_file('protected/data/specs.tsv', 1);
		$specs = Formatter::tsvToArray($specs);
		$res = array();
		foreach ($specs[0] as $ix=>$val)
			if ($ix > 1) $res[$val] = 1;
		return $res;
	}
	
	public function getSpecifications()
	{
		if ($this->specifications != '')
			return $this->specifications;
		
		$specs = Yii::app()->controller->site_file('protected/data/specs.tsv', 1);
		$specs = Formatter::tsvToArray($specs);

		$res = '<b>Main Features:</b><ul>';
		
		$p = -1;
		foreach ($specs[0] as $ix=>$val)
		{
			if ($val == $this->model)
			{
				$p = $ix;
				break;
			}
		}
		if ($p == -1) return 'Not available yet.';
		
		$main = 1;
		foreach ($specs as $ix=>$row)
		{
			if ($ix == 0) continue;
			if ($row[0] == 'highlight')
			{
				if ($row[$p] == '') continue;
				$res .= '<li>' . $row[$p] . '</li>';
			}
			else
			{
				if ($main) { $res .= '</ul><table id="specs">'; $main = 0; }
				if ($row[0] == 'section')
					$res .= sprintf('<tr><th></th><th>%s</th></tr>', $row[1]);
				else
					$res .= sprintf('<tr><td>%s</td><td>%s</td></tr>', $row[1], $row[$p]);
			}
		}
		$res .= '</table>';
		return $res;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'products';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('brand, category, model, mrp, price, variations, specifications', 'required'),
			array('promote', 'numerical', 'integerOnly'=>true),
			array('brand', 'length', 'max'=>100),
			array('category', 'length', 'max'=>32),
			array('model', 'length', 'max'=>64),
			array('mrp, price', 'length', 'max'=>10),
			array('variations', 'length', 'max'=>255),
			array('specifications', 'length', 'max'=>2048),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, brand, category, model, mrp, price, variations, specifications, promote', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'orderItems' => array(self::HAS_MANY, 'OrderItem', 'product_id'),
			'pricings' => array(self::HAS_MANY, 'Pricing', 'product_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'brand' => 'Brand',
			'category' => 'Category',
			'model' => 'Model',
			'mrp' => 'Mrp',
			'price' => 'Price',
			'variations' => 'Colours',
			'specifications' => 'Specifications',
			'promote' => 'Promote',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('brand',$this->brand,true);
		$criteria->compare('category',$this->category,true);
		$criteria->compare('model',$this->model,true);
		$criteria->compare('mrp',$this->mrp,true);
		$criteria->compare('price',$this->price,true);
		$criteria->compare('variations',$this->variations,true);
		$criteria->compare('specifications',$this->specifications,true);
		$criteria->compare('promote',$this->promote);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
