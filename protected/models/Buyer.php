<?php
/**
 * @author Imran Ali Namazi <imran@cselian.com>
 * @link http://tg.cselian.com/showcase/e-vend
 * @copyright Copyright &copy; 2012-2013 cselian.com
 * @license http://tg.cselian.com/licenses/e-vend
 */

/**
 * This is the model class for table "buyers".
 *
 * The followings are the available columns in table 'buyers':
 * @property integer $id
 * @property string $date_added
 * @property string $name
 * @property integer $type
 * @property string $contact
 * @property string $notify
 * @property string $address
 * @property string $pin
 * @property string $phone
 * @property string $mobile
 * @property string $lat
 * @property string $lon
 * @property integer $assigned_to_id
 * @property string $outstanding
 * @property string $tax_info
 * @property integer $priced_only
 * @property integer $approval_required
 * @property string $payment_types
 * @property string $signup_from
 * @property integer $sponsored
 * @property integer $purge
 *
 * The followings are the available model relations:
 * @property Address[] $addresses
 * @property Distributor $assignedTo
 * @property Order[] $orders
 * @property Pricing[] $pricings
 * @property User[] $users
 */
class Buyer extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Buyer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function formatApproval()
	{
		return intval($this->approval_required) === 2 ? 'yes (post pyt)' : Formatter::bool($this->approval_required);
	}

	public static function format($model, $nr = 0)
	{
		return sprintf('%s (%s%s)', $model->name, 
			AppLookups::BuyerType($model->type),
			$nr ? ' #' . $model->id : '');
	}
	
	public static function isEnteringSite($flash = 0)
	{
		if (!isset($_GET['buyer']) || !isset($_GET['id']) || !isset($_GET['hash']))
			return false;
		$id = $_GET['id'];
		$model = self::model()->findByPk($id);
		if ($model == null)
			throw new Exception('Buyer with id' . $id . 'not found');
		if ($model->name != $_GET['buyer'])
			throw new Exception('Buyer id and name do not match. Please ask your admin for the correct url.');
		if (self::hash($model) != $_GET['hash'])
			throw new Exception('Buyer hash invalid. Please ask your admin for the correct url.');

		self::enterSite($id, $model, $flash);
		return true;
	}
	
	public static function isEnteringGlobalBuyerSite($c)
	{
		if ($c->user_is('notloggedin') &&  !$c->user_is('buyer')
			&& 	$c->getId() == 'products' && $id = AppEnv::get('default_buyer', 'Global Buyer Id', 0) )
		{
			self::enterSite($id);
		}
	}
	
	private static function enterSite($id, $model = null, $flash = 0)
	{
		if ($model == null) $model = self::model()->findByPk($id);
		UserIdentity::setContext('buyer', $id);
		UserIdentity::setContext('buyer_type', $model->type); // to throw pricing error
		UserIdentity::setContext('buyerName', $model->name);
		UserIdentity::setUserType(AppLookups::$userTypeBuyer);
		if ($flash) FlashMessages::Add('Feel free to look at the products and add to cart. But before placing an order, you will need to ' . UserIdentity::signupOrLogin() . '.');
	}
	
	public function siteEntryLink()
	{
		$url = Yii::app()->createAbsoluteUrl(sprintf('/?buyer=%s&id=%s&hash=%s', $this->name, $this->id, self::hash($this)));
		return CHtml::link('Buyer Site', $url);
	}
	
	private static function hash($o)
	{
		$id = $o->id;
		$l = str_split($o->name);
		foreach ($l as $a) $id += ord($a);
		return base64_encode($id);
	}
	
	public function adjustPaymentTypes($for = 'view')
	{
		if ($for == 'save')
			$this->payment_types = $this->payment_types != '' ? implode(',', $this->payment_types) : '';
		else if ($for == 'edit')
			$this->payment_types = Formatter::csvToInts($this->payment_types); //checkBoxList uses in_array
		else if ($for == 'view')
			$this->payment_types = AppLookups::PaymentTypes($this->payment_types, 1);
	}
	
	public function nullAddressCaption()
	{
		$admin = UserIdentity::context('admin');
		return $admin ? 'Self' : 'Office';
	}
	
	public function myAddresses()
	{
		$admin = UserIdentity::context('admin');
		$id = UserIdentity::context('user');
		$cbo = array(); //TODO: Office addresses - null => $admin ? 'Self' : 'Office');
		$bill = array();
		$obj = array();
		foreach ($this->addresses as $v)
		{
			// TODO: depends on buyer type? or admin should not be using the admin account for personal orders?
			if (!$admin && $v->user_id != $id) continue;
			$obj[] = $v;
			$cbo[$v->id] = $v->name;
			$bill[$v->id] = $v->name;
		}
		return  array('cbo' => $cbo, 'bill' => $bill, 'obj' => $obj);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'buyers';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('date_added, name, type, contact, address, pin, phone, signup_from', 'required'),
			array('type, assigned_to_id, priced_only, approval_required, sponsored, purge', 'numerical', 'integerOnly'=>true),
			array('name, contact', 'length', 'max'=>100),
			array('notify, tax_info', 'length', 'max'=>512),
			array('address', 'length', 'max'=>255),
			array('pin', 'length', 'max'=>6),
			array('phone, mobile', 'length', 'max'=>50),
			array('lat, lon, outstanding', 'length', 'max'=>9),
			array('payment_types', 'length', 'max'=>32),
			array('signup_from', 'length', 'max'=>1024),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, date_added, name, type, contact, notify, address, pin, phone, mobile, lat, lon, assigned_to_id, outstanding, tax_info, priced_only, approval_required, payment_types, signup_from, sponsored, purge', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'addresses' => array(self::HAS_MANY, 'Address', 'buyer_id'),
			'assignedTo' => array(self::BELONGS_TO, 'Distributor', 'assigned_to_id'),
			'orders' => array(self::HAS_MANY, 'Order', 'buyer_id'),
			'pricings' => array(self::HAS_MANY, 'Pricing', 'buyer_id'),
			'users' => array(self::HAS_MANY, 'User', 'buyer_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'date_added' => 'Date Added',
			'name' => 'Name',
			'type' => 'Type',
			'contact' => 'Contact',
			'notify' => 'Notify',
			'address' => 'Address',
			'pin' => 'Pin',
			'phone' => 'Phone',
			'mobile' => 'Mobile',
			'lat' => 'Lat',
			'lon' => 'Lon',
			'assigned_to_id' => 'Assigned To',
			'outstanding' => 'Outstanding',
			'tax_info' => 'Tax Info',
			'priced_only' => 'Priced Only',
			'approval_required' => 'Approval Required',
			'payment_types' => 'Payment Types',
			'signup_from' => 'Signup From',
			'sponsored' => 'Sponsored',
			'purge' => 'Purge',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('date_added',$this->date_added,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('type',$this->type);
		$criteria->compare('contact',$this->contact,true);
		$criteria->compare('notify',$this->notify,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('pin',$this->pin,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('mobile',$this->mobile,true);
		$criteria->compare('lat',$this->lat,true);
		$criteria->compare('lon',$this->lon,true);
		$criteria->compare('assigned_to_id',$this->assigned_to_id);
		$criteria->compare('outstanding',$this->outstanding,true);
		$criteria->compare('tax_info',$this->tax_info,true);
		$criteria->compare('priced_only',$this->priced_only);
		$criteria->compare('approval_required',$this->approval_required);
		$criteria->compare('payment_types',$this->payment_types,true);
		$criteria->compare('signup_from',$this->signup_from,true);
		$criteria->compare('sponsored',$this->sponsored);
		$criteria->compare('purge',$this->purge);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
