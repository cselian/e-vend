<?php
/**
 * @author Imran Ali Namazi <imran@cselian.com>
 * @link http://tg.cselian.com/showcase/e-vend
 * @copyright Copyright &copy; 2012-2013 cselian.com
 * @license http://tg.cselian.com/licenses/e-vend
 */

/**
 * This is the model class for table "distributors".
 *
 * The followings are the available columns in table 'distributors':
 * @property integer $id
 * @property string $name
 * @property string $date_added
 * @property string $office_email
 * @property string $address
 * @property string $pin
 * @property string $lat
 * @property string $lon
 * @property string $tax_info
 * @property string $payment_info
 */
class Distributor extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Distributor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	function Deflt($ret = 'id')
	{
		$id = AppEnv::get('default_distributor', 'Default Distributor', false);
		if (!$id) return $ret == 'link' ? 'null' : ($ret == 'id' ? null : false);
		if ($ret == 'link')
		{
			$model = self::model()->findByPK($id);
			return CHtml::link(sprintf('%s (#%s)', $model->name, $model->id), array('/distributors/' . $model->id));
		}
		
		return $id;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'distributors';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, date_added, office_email, address, pin, lat, lon', 'required'),
			array('name, office_email', 'length', 'max'=>100),
			array('address', 'length', 'max'=>255),
			array('pin', 'length', 'max'=>6),
			array('lat, lon', 'length', 'max'=>9),
			array('tax_info, payment_info', 'length', 'max'=>512),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, date_added, office_email, address, pin, lat, lon, tax_info, payment_info', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'date_added' => 'Date Added',
			'office_email' => 'Office Email',
			'address' => 'Address',
			'pin' => 'Pin',
			'lat' => 'Lat',
			'lon' => 'Lon',
			'tax_info' => 'Tax Info',
			'payment_info' => 'Payment Info',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('date_added',$this->date_added,true);
		$criteria->compare('office_email',$this->office_email,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('pin',$this->pin,true);
		$criteria->compare('lat',$this->lat,true);
		$criteria->compare('lon',$this->lon,true);
		$criteria->compare('tax_info',$this->tax_info,true);
		$criteria->compare('payment_info',$this->payment_info,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
