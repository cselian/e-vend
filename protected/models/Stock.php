<?php
/**
 * @author Imran Ali Namazi <imran@cselian.com>
 * @link http://tg.cselian.com/showcase/e-vend
 * @copyright Copyright &copy; 2012-2013 cselian.com
 * @license http://tg.cselian.com/licenses/e-vend
 */

/**
 * This is the model class for table "Stock".
 *
 * The followings are the available columns in table 'Stock':
 * @property integer $id
 * @property integer $distributor_id
 * @property integer $product_id
 * @property string $variation
 * @property integer $balance
 *
 * The followings are the available model relations:
 * @property Product $product
 * @property Distributor $distributor
 */
class Stock extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Stock the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	// returns balance of stock of given distributor with product-variation as the key
	public static function getList($id, $under)
	{
		$res = array(); $prods = array();
		$where = 'distributor_id=' . $id;
		if ($under !== false) $where .= ' AND balance <= ' . $under;
		$data = self::model()->findAll($where);
		foreach ($data as $r) {
			$res[self::variationKey($r['product_id'], $r['variation'])] = $r['balance'];
			$prods[$r['product_id']] = 1;
		}
		return array('stock' => $res, 'products' => $prods);
	}
	
	public static function reduceVariationsToInStock($pid, $list, $stock)
	{
		$res = array();
		foreach ($list as $v)
		{
			$vk = Stock::variationKey($pid, $v);
			if (isset($stock[$vk])) $res[] = $v;
		}
		return $res;
	}
	
	public static function variationKey($pid, $varn)
	{
		return sprintf('%s-%s', $pid, trim(strtolower($varn)));
	}
	
	public static function genCsv($data = 0)
	{
		$file = '/data/import stock gen.csv';
		if ($data)
			file_put_contents(Controller::site_file($file), $data);
		
		return Yii::app()->baseUrl . $file;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'stock';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('distributor_id, product_id, balance', 'required'),
			array('distributor_id, product_id, balance', 'numerical', 'integerOnly'=>true),
			array('variation', 'length', 'max'=>32),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, distributor_id, product_id, variation, balance', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'product' => array(self::BELONGS_TO, 'Product', 'product_id'),
			'distributor' => array(self::BELONGS_TO, 'Distributor', 'distributor_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'distributor_id' => 'Distributor',
			'product_id' => 'Product',
			'variation' => 'Variation',
			'balance' => 'Balance',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('distributor_id',$this->distributor_id);
		$criteria->compare('product_id',$this->product_id);
		$criteria->compare('variation',$this->variation,true);
		$criteria->compare('balance',$this->balance);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
