<?php
/**
 * @author Imran Ali Namazi <imran@cselian.com>
 * @link http://tg.cselian.com/showcase/e-vend
 * @copyright Copyright &copy; 2012-2013 cselian.com
 * @license http://tg.cselian.com/licenses/e-vend
 */

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $employee_id
 * @property string $password
 * @property string $date_added
 * @property integer $type
 * @property integer $admin
 * @property integer $distributor_id
 * @property integer $buyer_id
 * @property string $reset_code
 * @property string $reset_date
 * @property integer $deactive
 *
 * The followings are the available model relations:
 * @property Address[] $addresses
 * @property Buyer $buyer
 * @property Distributor $distributor
 */
class User extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public static function link($model, $buyer)
	{
		if ($model == null) return 'Not Set';
		if (Controller::user_is('buyernonadmin')) return sprintf('%s (Me)', $model->name);
		$atts = $model->deactive ? array('title' => $model->email . ' (deactive)', 'style' => 'text-decoration: line-through;')
			: array('title' => $model->email);
		if (strlen($model->employee_id) > 0) $atts['title'] .= ', ID: ' . $model->employee_id;
		$edit = Controller::user_is('buyernonadmin') && $buyer->sponsored
			? ' ' . CHtml::link('&hellip;', array('/users/update/' . $model->id)) : '';
		return CHtml::link($model->name, array('/users/' . $model->id), $atts) . $edit;
	}

	public static function createFor($model, $retUrl = 0, $lbl3 = 'Impersonate ')
	{
		$name = get_class($model);
		if ($name == 'Distributor')
		{ $type = '2'; $ut = 'distributor'; $na = 'Salesman'; }
		else if ($name == 'Buyer')
		{ $type = '3'; $ut = 'buyer'; $na = 'Employee'; }
		$url1 = array('label'=>'Create User', 'url' => 
			array('/users/create', 'id' => $type, 'name' => $ut, 'for' => $model->id));
		$url2 = array('label'=>'Impersonate Admin', 'url' => 
			array('/users/impersonate', 'type' => $type, 'what' => $ut, 'a' => '1', 'name' => $model->name, 'id' => $model->id));
		$url3 = array('label' => $lbl3 . $na, 'url' => array_merge($url2['url'], array('a' => '0')) );
		if ($retUrl == 1) return $url1;
		else if ($retUrl == 2) return $url2;
		else if ($retUrl == 3) return $url3;
		return CHtml::link($url1['label'], $url1['url'])
			. '<br /> ' . CHtml::link($url2['label'], $url2['url'])
			. ', ' . CHtml::link($url3['label'], $url3['url']);
	}

	public static function forWhom($model)
	{
		$for = $model->buyer != null ? $model->buyer : $model->distributor;
		if ($for != null)
			$for = sprintf('%s #%s - %s', get_class($for), $for->id, $for->name);
		else
			$for = AppLookups::UserType($model->type);
		return $for;
	}

	public function setResetCode()
	{
		$this->reset_code = AppHelper::generatePassword(20, 1); // no special characters to keep the url clean
		$this->reset_date = Formatter::dateForSql();
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		$reqId = AppBuyers::getBool('employee_id') ? ', employee_id' : '';
		return array(
			array('name, email, password, date_added, type' . $reqId, 'required'),
			array('type, admin, distributor_id, buyer_id, deactive', 'numerical', 'integerOnly'=>true),
			array('name, email, employee_id', 'length', 'max'=>100),
			array('password, reset_code', 'length', 'max'=>32),
			array('email', 'email','checkMX'=>false),
			array('email', 'unique'),
			array('reset_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, email, employee_id, password, date_added, type, admin, distributor_id, buyer_id, reset_code, reset_date, deactive', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'addresses' => array(self::HAS_MANY, 'Address', 'user_id'),
			'buyer' => array(self::BELONGS_TO, 'Buyer', 'buyer_id'),
			'distributor' => array(self::BELONGS_TO, 'Distributor', 'distributor_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'email' => 'Email',
			'employee_id' => 'Employee ID',
			'password' => 'Password',
			'date_added' => 'Date Added',
			'type' => 'Type',
			'admin' => 'Admin',
			'distributor_id' => 'Distributor',
			'buyer_id' => 'Buyer',
			'reset_code' => 'Reset Code',
			'reset_date' => 'Reset Date',
			'deactive' => 'Deactive',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('date_added',$this->date_added,true);
		$criteria->compare('type',$this->type);
		$criteria->compare('admin',$this->admin);
		$criteria->compare('distributor_id',$this->distributor_id);
		$criteria->compare('buyer_id',$this->buyer_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
