<?php
/**
 * @author Imran Ali Namazi <imran@cselian.com>
 * @link http://tg.cselian.com/showcase/e-vend
 * @copyright Copyright &copy; 2012-2013 cselian.com
 * @license http://tg.cselian.com/licenses/e-vend
 */

/**
 * Mail class.
 * Mail is the data structure for keeping email templates.
 * It is used for emailing orders and user info etc.
 */
class Mail extends CFormModel
{
	public $id;
	public $name;
	public $buyer;
	public $body;
	public $original;
	public $trial;
	public $html;
	public $remove;
	public $date;
	public $isNewRecord = 1;
	
	public static function names()
	{
		$names = AppMails::names();
		$res = array();
		foreach ($names as $n)
			$res[$n] = self::name_r($n);
		return $res;
	}
	
	public static function name_r($name)
	{
		return ucwords(str_replace('-', ' ', $name));
	}
	
	public static function findAll()
	{
		$items = array();
		$dir = AppMails::getDir();
		$files = scandir($dir);
		foreach ($files as $name)
		{
			if ($name == '.' || $name == '..') continue;
			$mail = self::load($name, $dir);
			$items[$mail->sortId()] = $mail;
		}
		ksort($items);
		return $items;
	}
	
	public static function findMatch($dir, $name, $buyer)
	{
		$res = array();
		$canTrial = Controller::user_is('admin');
		if ($buyer != null)
		{
			if ($canTrial) $res[] = sprintf('%s-buyer-%s-trial',$name, $buyer);
			$res[] = sprintf('%s-buyer-%s',$name, $buyer);
		}
		if ($canTrial) $res[] = sprintf('%s-trial',$name);
		$res[] = $name;
		
		foreach ($res as $name)
		{
			$file = $dir . $name . '.html';
			if (file_exists($file)) return array('name' => $name, 'file' => $file, 'html' => true);

			$file = $dir . $name . '.txt';
			if (file_exists($file)) return array('name' => $name, 'file' => $file, 'html' => false);
		}

		return false;
	}
	
	public static function load($name, $dir = null, $loadBody = false)
	{
		if ($dir == null) $dir = AppMails::getDir();
		$itm = new Mail();
		$itm->id = $name;
		$itm->date = filemtime($dir . $name);
		//die(date('d m Y',$itm->date));
		$itm->isNewRecord = 0;
		$itm->html = stripos($name, '.html') !== false;
		$name = str_ireplace('.html', '', str_ireplace('.txt', '', $name));
		if (strpos($name, '-trial') !== false)
		{
			$itm->trial = 1;
			$name = str_ireplace('-trial', '', $name);
		}
		
		$bits = explode('-buyer-', $name);
		$itm->name = $bits[0];
		if (count($bits) > 1) $itm->buyer = intval($bits[1]);
		if ($loadBody) $itm->loadBody($loadBody);
		
		return $itm;
	}
	
	public function save()
	{
		if (!$this->validate()) return false;
		if ($this->id != '')
		{
			$file = AppMails::getDir() . $this->id;
			if (file_exists($file)) unlink($file);
		}
		
		if ($this->remove) return true;
		
		$name = $this->name;
		if ($this->buyer != 0) $name .= '-buyer-' . $this->buyer;
		if ($this->trial) $name .= '-trial';
		$file = AppMails::getDir() . $name . ($_POST['Mail']['html'] ? '.html' : '.txt'); // strange error whereby html is not parsed into attributes. should have had isHtml...
		if (file_exists($file))
		{
			$this->addError('buyer', 'Changing the buyer to a mail template that already exists isnt possible. Filename collision: ' . $file);
			return false;
		}
		file_put_contents($file, $this->body);
		$this->id = $name;
		
		return true;
	}
	
	public function sortId()
	{
		return ($this->buyer == null ? 0 : $this->buyer) . $this->name;
	}
	
	public function formatBuyer($fmt = false)
	{
		if ($fmt) return $this->buyer == null ? '' : sprintf($fmt, AppBuyers::getName($this->buyer));
		return $this->buyer == null ? Formatter::$nullMsg
			: CHtml::link(AppBuyers::getName($this->buyer), array('/buyers/' . $this->buyer));
	}
	
	public function loadBody($how)
	{
		$file = AppMails::getDir() . $this->id;
		$this->original = Controller::site_file('protected/data/' . $this->name . '.txt', 1);
		if ($how == 'create') {
			// do nothing
		} else if (!file_exists($file)) {
			$this->body = 'File not found:' . $file;
		} else {
			$this->body = file_get_contents($file);
		}
		
		if ($how == 'view')
		{
			$this->body = AppMails::adjustBody($this->body, $this->html);
			$this->original = AppMails::adjustBody($this->original);
		}
	}
	
	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			// name, email, subject and body are required
			array('name, body', 'required'),
			// email has to be a valid email address
			array('buyer, trial, remove', 'numerical', 'integerOnly'=>true),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			'html' => 'Is Html',
			'body' => 'Message Body',
			'original' => 'Original (Builtin) Template'
		);
	}
}