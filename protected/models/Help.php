<?php
/**
 * @author Imran Ali Namazi <imran@cselian.com>
 * @link http://tg.cselian.com/showcase/e-vend
 * @copyright Copyright &copy; 2012-2013 cselian.com
 * @license http://tg.cselian.com/licenses/e-vend
 */

/**
 * This is the model class for table "help".
 *
 * The followings are the available columns in table 'help':
 * @property integer $id
 * @property string $slug
 * @property string $topic
 * @property string $keywords
 * @property string $description
 * @property string $content
 * @property string $accessible_to
 */
class Help extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Help the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function link($data)
	{
		$dev = Yii::app()->controller->user_is('nowdev');
		$edit = !$dev ? '' : ' ' . CHtml::link('&hellip;', array('/help/update', 'id'=>$data->id), array('class' => 'right'));
		return $edit . CHtml::link($data->topic, array('/help/view', 'id'=>$data->id, 'topic' => strtolower(str_replace(' ', '-', $data->topic))));
	}
	
	public static function linkToKeyword($word, $retUrl = 0)
	{
		$url = array('/help/index', 'keyword' => $word);
		return $retUrl ? array('label'=>$word, 'url' => $url) : CHtml::link($word, $url);
	}
	
	public static function linkToFolder($word, $retUrl = 0)
	{
		$url = array('/help/index', 'folder' => $word);
		return $retUrl ? array('label'=>$word, 'url' => $url) : CHtml::link($word, $url);
	}
	
	public function adjustAll()
	{
		$this->adjustContent();
		$this->adjustAccesibleTo();
		$this->adjustKeywords();
		$this->adjustSlug();
	}

	public function adjustKeywords()
	{
		$res = array();
		$list = explode(',', $this->keywords);
		foreach ($list as $word)
			$res[] = self::linkToKeyword($word);
		$this->keywords = implode(' ', $res);
	}

	public function adjustSlug()
	{
		$res = array();
		$list = explode('/', substr($this->slug, 1));
		unset($list[count($list) - 1]);
		foreach ($list as $word)
		{
			$res[] = self::linkToFolder($word);
			$slug = $word . '/';
		}
		$this->slug = implode(' / ', $res);
	}

	public function adjustContent()
	{
		$subs = array(
			'
' => '<br/>
',
			'{baseurl}' => Yii::app()->baseUrl,
			'{imgfol}' => Yii::app()->baseUrl . '/img/help/'
	);
		foreach ($subs as $what=>$with)
			$this->content = str_replace($what, $with, $this->content);
	}
	
	public function adjustAccesibleTo($for = 'view')
	{
		if ($for == 'save') {
			$this->accessible_to = implode(',', $this->accessible_to);
			$this->keywords = str_replace(' ', '', $this->keywords);
		} else if ($for == 'edit') {
			$this->accessible_to = AppLookups::UserTypes($this->accessible_to);
			$this->keywords = str_replace(',', ', ', $this->keywords);
		} else if ($for == 'view') {
			$this->accessible_to = AppLookups::UserTypes($this->accessible_to, 1);
		}
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'help';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('slug, topic, keywords, description, content, accessible_to', 'required'),
			array('slug', 'length', 'max'=>128),
			array('topic, accessible_to', 'length', 'max'=>256),
			array('keywords, description', 'length', 'max'=>512),
			array('content', 'length', 'max'=>4096),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, slug, topic, keywords, description, content, accessible_to', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'slug' => 'Folder',
			'topic' => 'Topic',
			'keywords' => 'Keywords',
			'description' => 'Description',
			'content' => 'Content',
			'accessible_to' => 'Accessible To',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('slug',$this->slug,true);
		$criteria->compare('topic',$this->topic,true);
		$criteria->compare('keywords',$this->keywords,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('accessible_to',$this->accessible_to,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
