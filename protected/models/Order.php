<?php
/**
 * @author Imran Ali Namazi <imran@cselian.com>
 * @link http://tg.cselian.com/showcase/e-vend
 * @copyright Copyright &copy; 2012-2013 cselian.com
 * @license http://tg.cselian.com/licenses/e-vend
 */

/**
 * This is the model class for table "orders".
 *
 * The followings are the available columns in table 'orders':
 * @property integer $id
 * @property integer $buyer_id
 * @property integer $user_id
 * @property integer $address_id
 * @property integer $billing_address_id
 * @property string $order_date
 * @property string $amount
 * @property string $verification
 * @property integer $status
 * @property integer $approved
 * @property string $despatched_date
 * @property string $despatcher_reference
 * @property integer $despatcher_id
 * @property integer $payment_type
 * @property string $payment_ref
 * @property string $payment_date
 * @property integer $payment_confirmed
 * @property string $payment_details
 *
 * The followings are the available model relations:
 * @property OrderItem[] $orderItems
 * @property Address $billingAddress
 * @property Address $address
 * @property Buyer $buyer
 * @property Distributor $despatcher
 * @property User $user
 */
class Order extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Order the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function includeArchival(&$cri)
	{
		if (Controller::user_is('dev')) return $cri;
		$sql = 'archived = 0';
		
		if (is_array($cri))
		{
			if (isset($cri['condition']))
				$cri['condition'] = $sql . ' AND ' . $cri['condition'];
			else
				$cri['condition'] = $sql;
		}
	}
	
	public function formatAddress($bill = 0)
	{
		$add = $bill ? $this->billingAddress : $this->address;
		return Address::format($add, $this->buyer);
	}
	
	// Used to test formatStatus (below) in the admin action
	public static function SampleStatuses()
	{
		$list = array(
			array('status' => 9, 'notreqd' => 1, 'what' => 'Cancelled'),
			array('status' => 1, 'approved' => -1, 'what' => 'Approval Pending'),
			array('status' => 1, 'approved' => 0, 'what' => 'Rejected'),
			array('status' => 1, 'approved' => 1, 'what' => 'Approved'),
			array('status' => 1, 'approved' => 1, 'payment_confirmed' => 1, 'what' => 'Paid'),
			array('status' => 3, 'what' => 'Despatched'),
			array('status' => 4, 'what' => 'Delivered'),
		);

		$reqd = new Buyer; $reqd->approval_required = 1;
		$notReqd = new Buyer;

		$res = array();
		foreach ($list as $props)
		{
			$o = new Order;
			$o->buyer = isset($props['notreqd']) ? $notReqd : $reqd;
			unset($props['notreqd']);
			$o->payment_details = $props['what'];
			$o->attributes = $props;
			$res[] = $o;
		}
		return $res;
	}
	
	public function formatStatus()
	{
		//1 Created, 2  Under Processing, 3 Despatched, 4 Delivered, 9 Cancelled,
		if ($this->status <> 1) return AppLookups::OrderStatus($this->status);
		
		// Pending for approval, Approved & Pending for Payment Clearance , Payment Cleared & Under Processing, Device Dispatched, Delivered
		if ($this->needsApprovalOrRejection())
			return 'Pending Approval';
		if ($this->isRejected())
			return 'Rejected';
		if ($this->approved && $this->payment_confirmed == 0)
			return 'Approved & Pending for Payment Clearance';
		if ($this->approved && $this->payment_confirmed == 1)
			return 'Payment Cleared & Under Processing';
		
		return 'Unknown Status';
	}
	
	public function formatDespatcherRef()
	{
		$res = '';
		if ($this->despatcher_reference != '') $res .= $this->despatcher_reference;
		if ($this->despatched_date != '') $res .= ', dt:' . Formatter::date($this->despatched_date);
		return $res;
	}
	
	public function formatPaymentType($noneIfGateway = 1)
	{
		if ($noneIfGateway && $this->isGateway()) return '';
		return AppLookups::PaymentType($this->payment_type);
	}
	
	public function formatApproval()
	{
		return !$this->buyer->approval_required ? 'Not Reqd' :
			(intval($this->approved) === -1 ? 'Approval Pending' : Formatter::bool($this->approved));
	}
	
	public function needsApprovalOrRejection($beforePyt = 0)
	{
		if ($beforePyt)
			return intval($this->buyer->approval_required) === 1 && intval($this->approved) == -1;
		return intval($this->buyer->approval_required) !== 0 && intval($this->approved) == -1;
	}

	public function isRejected()
	{
		return intval($this->buyer->approval_required) === 1 && intval($this->approved) === 0;
	}
	
	public function gatewayAndUnpaid()
	{
		return $this->isGateway() && $this->isUnpaid();
	}
	
	public function isGateway()
	{
		return $this->payment_type == AppLookups::$paymentTypeGateway;
	}
	
	public function isUnpaid()
	{
		return $this->payment_ref == '';
	}
	
	public function paymentLinkOrStatus()
	{
		if (!$this->isGateway())
			return AppLookups::canUserSee('paymentDetails') == false ? '' : $this->paymentStatus();

		return CHtml::link($this->canUser('pay') ? 'pay' : $this->paymentStatus(), 
			array('/orders/pay', 'id'=>$this->id), array('title' => 'online payment details'));
	}

	public function paymentStatus()
	{
		return $this->payment_confirmed ? 'paid' : 'unpaid';
	}
	
	public function addMenu($what)
	{
		if ($what == 'pay' && Controller::user_is('buyernonadmin') && $this->gatewayAndUnpaid())
				MenuHelper::AddMenuItem('Pay Order', array('/orders/pay/' . $this->id));
		else if ($what == 'purge' && $this->canUser('purge'))
				MenuHelper::AddMenuItem('Purge Order', array('/orders/purge/' . $this->id));
	}
	
	public function purgeLink()
	{
		if (!$this->canUser('purge')) return '';
		return CHtml::link('purge',  array('/orders/purge/' . $this->id));
	}
	
	private function canUser($what)
	{
		if ($what == 'pay')
			return Controller::user_is('buyer') && $this->isUnpaid();
		else if ($what == 'purge')
			return Controller::user_is('superadmin') && AppBuyers::canPurge($this->buyer_id) && !$this->isUnpaid();
	}
	
	public static function hideVerification($order)
	{
		$dis = Yii::app()->controller->user_is('distributor');
		if ($dis && $order->status != 4)
			return '<span class="null">Not shown</span>';
		return $order->verification;
	}

	public function getDetails($forDespatcher)
	{
		$d = array('Product', 'Colour', 'Price', 'Quantity', 'Amount');
		if ($forDespatcher) $d[] = 'Calculation';
		$tbl = AppHelper::buildTable($d);
		
		foreach ($this->orderItems as $itm)
		{
			$d = array(Product::format($itm->product), Product::variation_r($itm->variation), $itm->price, 
				$itm->quantity . ' unit' . ($itm->quantity > 1 ? 's' : ''), 
				Formatter::amount($itm->quantity * $itm->price));
			if ($forDespatcher) $d[] = $itm->calculation;
			$tbl->add_row($d);
		}
		
		$tbl->set_template(array('table_open' => '<table border="1" cellpadding="4" style="border-collapse: collapse;">',
			'heading_cell_start' => '<th style="background-color: #FDF287; color: #52005E">', 'heading_cell_end' => '</th>'));
		return $tbl->generate();
	}

	public function getDetailsInline()
	{
		$res = array();
		foreach ($this->orderItems as $itm)
			$res[] = sprintf('%s %s %s %s', $itm->quantity, $itm->product->brand, 
				$itm->product->model, Product::variation_r($itm->variation));
		return implode(', ', $res);
	}
	
	public function otherOrders()
	{
		$sql = sprintf('select id, status from Orders where user_id = %s and id <> %s', $this->user_id, $this->id);
		$items = self::model()->findAllBySql($sql);
		if (count($items) == 0) return false;
		
		$res = array();
		foreach ($items as $o) $res[] = CHtml::link(
			sprintf('%s (%s)', $o->id, AppLookups::OrderStatus($o->status)), array('/orders/' . $o->id) );
		return $res;
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'orders';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('buyer_id, user_id, billing_address_id, order_date, amount, verification, status, payment_type', 'required'),
			array('buyer_id, user_id, address_id, billing_address_id, status, approved, despatcher_id, payment_type, payment_confirmed', 'numerical', 'integerOnly'=>true),
			array('amount', 'length', 'max'=>10),
			array('verification', 'length', 'max'=>24),
			array('despatcher_reference, payment_ref', 'length', 'max'=>128),
			array('payment_details', 'length', 'max'=>2048),
			array('despatched_date, payment_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, buyer_id, user_id, address_id, billing_address_id, order_date, amount, verification, status, approved, despatched_date, despatcher_reference, despatcher_id, payment_type, payment_ref, payment_date, payment_confirmed, payment_details', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'orderItems' => array(self::HAS_MANY, 'OrderItem', 'order_id'),
			'billingAddress' => array(self::BELONGS_TO, 'Address', 'billing_address_id'),
			'address' => array(self::BELONGS_TO, 'Address', 'address_id'),
			'buyer' => array(self::BELONGS_TO, 'Buyer', 'buyer_id'),
			'despatcher' => array(self::BELONGS_TO, 'Distributor', 'despatcher_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'buyer_id' => 'Buyer',
			'user_id' => 'User',
			'address_id' => 'Address',
			'billing_address_id' => 'Billing Address',
			'order_date' => 'Order Date',
			'amount' => 'Amount',
			'verification' => 'Verification',
			'status' => 'Status',
			'approved' => 'Approved',
			'despatched_date' => 'Despatched Date',
			'despatcher_reference' => 'Despatcher Reference',
			'despatcher_id' => 'Despatcher',
			'payment_type' => 'Payment Type',
			'payment_ref' => 'Payment Ref',
			'payment_date' => 'Payment Date',
			'payment_confirmed' => 'Payment Confirmed',
			'payment_details' => 'Payment Details',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('buyer_id',$this->buyer_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('address_id',$this->address_id);
		$criteria->compare('billing_address_id',$this->billing_address_id);
		$criteria->compare('order_date',$this->order_date,true);
		$criteria->compare('amount',$this->amount,true);
		$criteria->compare('verification',$this->verification,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('approved',$this->approved);
		$criteria->compare('despatched_date',$this->despatched_date,true);
		$criteria->compare('despatcher_reference',$this->despatcher_reference,true);
		$criteria->compare('despatcher_id',$this->despatcher_id);
		$criteria->compare('payment_type',$this->payment_type);
		$criteria->compare('payment_ref',$this->payment_ref,true);
		$criteria->compare('payment_date',$this->payment_date,true);
		$criteria->compare('payment_confirmed',$this->payment_confirmed);
		$criteria->compare('payment_details',$this->payment_details,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
