<?php
/**
 * @author Imran Ali Namazi <imran@cselian.com>
 * @link http://tg.cselian.com/showcase/e-vend
 * @copyright Copyright &copy; 2012-2013 cselian.com
 * @license http://tg.cselian.com/licenses/e-vend
 */

/**
 * This is the model class for table "reports".
 *
 * The followings are the available columns in table 'reports':
 * @property integer $id
 * @property string $slug
 * @property string $name
 * @property string $group
 * @property string $description
 * @property string $returns
 * @property string $condition
 * @property string $accessible_to
 * @property integer $last_count
 */
class Report extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Report the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function canAccess($model, $what = 0)
	{
		$allowed = explode(',', $model->accessible_to);
		foreach ($allowed as $who)
		{
			if (!$what) {
				if (Yii::app()->controller->user_is(trim($who))) return true;
			} else {
				if ($what == trim($who)) return true;
			}
		}
		return false;
	}
	
	public function runUrl($dev = 0)
	{
		$txt = $dev ? 'run' : $this->name;
		if ($this->slug[0] == '/')
			return CHtml::link($txt, array($this->slug), array('target' => '_new'));
		return CHtml::link($txt, array('run', 'name'=>$this->slug));
	}
	
	public function getUrl($r, $csv = 0)
	{
		return Yii::app()->createUrl('reports/run/?name=' . $r->slug) . ($csv ? '&csv=1' : '');
	}
	
	public function isUrlCsv()
	{
		return AppHelper::formGetVal('csv') === '1';
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'reports';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('slug, name, group, description, returns, condition, accessible_to', 'required'),
			array('last_count', 'numerical', 'integerOnly'=>true),
			array('slug, name', 'length', 'max'=>32),
			array('group, returns', 'length', 'max'=>64),
			array('description, condition, accessible_to', 'length', 'max'=>256),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, slug, name, group, description, returns, condition, accessible_to, last_count', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'slug' => 'Slug',
			'name' => 'Name',
			'group' => 'Group',
			'description' => 'Description',
			'returns' => 'Returns',
			'condition' => 'Condition',
			'accessible_to' => 'Accessible To',
			'last_count' => 'Last Count',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('slug',$this->slug,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('group',$this->group,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('returns',$this->returns,true);
		$criteria->compare('condition',$this->condition,true);
		$criteria->compare('accessible_to',$this->accessible_to,true);
		$criteria->compare('last_count',$this->last_count);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
