<?php
/**
 * @author Imran Ali Namazi <imran@cselian.com>
 * @link http://tg.cselian.com/showcase/e-vend
 * @copyright Copyright &copy; 2012-2013 cselian.com
 * @license http://tg.cselian.com/licenses/e-vend
 */

/**
 * This is the model class for table "addresses".
 *
 * The followings are the available columns in table 'addresses':
 * @property integer $id
 * @property integer $buyer_id
 * @property integer $user_id
 * @property string $name
 * @property string $address
 * @property string $pin
 * @property string $phone
 * @property string $lat
 * @property string $lon
 * @property integer $assigned_to_id
 *
 * The followings are the available model relations:
 * @property Distributor $assignedTo
 * @property Buyer $buyer
 * @property User $user
 * @property Order[] $orders
 */
class Address extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Address the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function format($address, $buyer, $self = 0)
	{
		$pfx = '#A';
		$name = '';
		
		if (($model = $address) == null)
		{
			$model = $buyer;
			if ($self) $name = 'Self: '; else $pfx = sprintf('#B%s (%s) - #A', $buyer->id, $buyer->name);
		}
		else
		{
			$name = sprintf($self ? '%s: ' : ' (%s) - ', $model->name);
		}
		if ($self) $pfx = $name; else $pfx .= $model->id . ' ' . $name;

		return sprintf('%s%s, %s', $pfx, $model->address, $model->pin);
	}
	
	public static function formatEx($model)
	{
		return sprintf('%s, %s', $model->address, $model->pin);
	}

	public static function linkToDistances($model)
	{
		if (!Yii::app()->controller->user_is('superadmin')) return;
		$url = array('/addresses/distances/', 'id' => $model->id);
		if (get_class($model) == 'Buyer') $url['type'] = 'buyer';
		return ' ' .CHtml::link('Distances', $url);
	}
	
	private static $citiesByPin;
	
	public function cityOrPincode()
	{
		if (self::$citiesByPin == null)
		{
			$pins = Yii::app()->controller->site_file('protected/data/cities.tsv', 1);
			$pins = Formatter::tsvToArray($pins);
			self::$citiesByPin = array();
			foreach ($pins as $r)
				self::$citiesByPin[$r[0]] = $r[1];
			// print_r(self::$citiesByPin);
		}
		$pfx = substr($this->pin, 0, 3);
		return isset(self::$citiesByPin[$pfx]) ? self::$citiesByPin[$pfx] : $this->pin;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'addresses';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('buyer_id, name, address, pin, phone', 'required'),
			array('buyer_id, user_id, assigned_to_id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>100),
			array('address', 'length', 'max'=>255),
			array('pin', 'length', 'max'=>6),
			array('phone', 'length', 'max'=>50),
			array('lat, lon', 'length', 'max'=>9),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, buyer_id, user_id, name, address, pin, phone, lat, lon, assigned_to_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'assignedTo' => array(self::BELONGS_TO, 'Distributor', 'assigned_to_id'),
			'buyer' => array(self::BELONGS_TO, 'Buyer', 'buyer_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			//'orders' => array(self::HAS_MANY, 'Order', 'address_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'buyer_id' => 'Buyer',
			'user_id' => 'User',
			'name' => 'Name',
			'address' => 'Address',
			'pin' => 'Pincode',
			'phone' => 'Phone',
			'lat' => 'Lat',
			'lon' => 'Lon',
			'assigned_to_id' => 'Assigned To',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('buyer_id',$this->buyer_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('pin',$this->pin,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('lat',$this->lat,true);
		$criteria->compare('lon',$this->lon,true);
		$criteria->compare('assigned_to_id',$this->assigned_to_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
