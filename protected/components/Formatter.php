<?php
/**
 * @author Imran Ali Namazi <imran@cselian.com>
 * @link http://tg.cselian.com/showcase/e-vend
 * @copyright Copyright &copy; 2012-2013 cselian.com
 * @license http://tg.cselian.com/licenses/e-vend
 * Common formatting stuff
 */

class Formatter
{
	function dateForSql($value = null, $format = 'MM/dd/yyyy HH:mm')
	{
		if ($value == null) return date('Y-m-d H:i:s');
		
		$ts = CDateTimeParser::parse($value,$format,array('month'=>1,'day'=>1,'hour'=>0,'minute'=>0,'second'=>0));
		if($ts !== false)
		{
			$dt = CTimestamp::formatDate('Y-m-d H:i:s', $ts);
			//echo $dt;
			return $dt;
		}
		
		throw new CHttpException(100, sprintf('Could not parse date %s. Expected format %s', $value, $format));
	}

	public static function csvToInts($csv)
	{
		$list = array();
		$csv = explode(',', $csv);
		foreach ($csv as $i)
			$list[] = intval($i);
		return $list;
	}

	function tsvToArray($data)
	{
		$r = array();
		$lines = explode('
', $data);
		foreach ($lines as $lin)
		{
			if ($lin == '' || $lin[0] == '#') continue;
			$r[] = explode("	", $lin);
		}
		return $r;
	}

	
	public static function amount($val)
	{
		return $val == null ? '' : 'Rs ' . $val;
	}
	
	public static function bool($bool)
	{
		return $bool ? 'yes' : 'no';
	}
	
	public static function dateCsv($date, $csv = 1)
	{
		if (!$csv) return self::date($date);
		if ($date == null || $date == '0000-00-00 00:00:00') return "";
		$dt = strtotime($date);
		return date('Y-m-d', $dt);
	}
	
	public static function date($date, $time = false, $compact = 0)
	{
		if ($date == null || $date == '0000-00-00 00:00:00') return "";
		$dt = is_string($date) ? strtotime($date) : $date;
		if ($compact) return sprintf('<span class="date compact" title="%s">%s</span>', date('d M Y', $dt), date('D h:i', $dt));
		$time = $time ? ' h:i a' : '';
		return date('D, d M Y' . $time, $dt);
	}
	
	public static function multiline($txt)
	{
		return str_replace('
', '<br/>
', $txt);
	}
	
	public static function altRow($ix, $ret = 0)
	{
		$cls = $ix === false ? "head" : ($ix % 2 == 0 ? "even" : "odd");
		$r = sprintf(' class="%s"', $cls);
		if ($ret) return $r;
		echo $r; 
	}
	
	public static $nullMsg = '<span class="null">Not Set</span>';
	
	public static function displayObject($model, $nullMsg = '<span class="null">Not Set</span>', $name = 'name')
	{
		return $model == null ? $nullMsg : $model->$name;
	}
	
	public static function errors($m, $glue = ', ')
	{
		// based on CHtml::errorSummary
		$content = array();
		foreach($m->getErrors() as $errors)
		{
			foreach($errors as $error)
			{
				if($error!='')
					$content[]= $error;
			}
		}
		return implode($glue, $content);
	}

	public static function includeBlank($list, $text = 'Not Set')
	{
		//array_merge was messing up the keys using the index
		$data = array('' => $text);
		foreach ($list as $key=>$value) $data[$key] = $value;
		return $data;
	}
	
	public static function Concat($sep = null, $p1, $p2, $p3 = null, $p4 = null)
	{
		$op = '';
		if ($p1 != '' && $p1 != null) $op = $p1;
		if ($p2 != '' && $p2 != null) $op .= ($op != '' ? $sep : '') . $p2;
		if ($p3 != '' && $p3 != null) $op .= ($op != '' ? $sep : '') . $p3;
		if ($p4 != '' && $p4 != null) $op .= ($op != '' ? $sep : '') . $p4;
		return $op;
	}
	
	public static function ConcatArray($sep, $items, $fmt = 0)
	{
		$op = '';
		
		foreach ($items as $k=>$v)
		{
			if ($v == '' || $v == null) continue;
			$op .= ($op != '' ? $sep : '') . ($fmt ? sprintf($fmt, $k, $v) : sprintf($k, $v));
		}
		
		return $op;
	}
	
	public static function Capitalize($text)
	{
		return ucwords(strtolower($text));
	}
	
	public static function Singular($text)
	{
		include_once 'ci_inflector.php';
		return Inflector::singular($text);
	}
}
?>
