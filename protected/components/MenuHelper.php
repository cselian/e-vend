<?php
/**
 * @author Imran Ali Namazi <imran@cselian.com>
 * @link http://tg.cselian.com/showcase/e-vend
 * @copyright Copyright &copy; 2012-2013 cselian.com
 * @license http://tg.cselian.com/licenses/e-vend
 */

class MenuHelper
{
	private static function Can($allowed, $action)
	{
		return $allowed == '*' || array_search($action, $allowed) !== false;
	}
	
	private static $helpLinks = array('keywords', 'folders', 'topics');
	
	public static function AddHelpLink($word, $type)
	{
		// TODO: Implementation
	}
	
	public static function HelpMenu($model = null)
	{
		$c = Yii::app()->controller;
		$cname = Formatter::Singular($c->id);
		$res = array(Help::linkToKeyword($cname, 1));
		if ($cname == 'help') $res[0]['visible'] = 0;
		
		$action = $c->getAction()->getId();
		if (in_array($action, array('map', 'import')))
			$res[] = Help::linkToKeyword($action, 1);
			
		if ($action == 'create' &&  in_array($cname, array('pricing', 'product', 'buyer', 'distributor')) )
			$res[] = Help::linkToKeyword('import', 1);
		
		return $res;
	}
	
	public static function AddMenuItem($lbl, $url)
	{
		self::$menuItems[] = array('label'=> $lbl, 'url'=>$url);
	}
	
	private static $menuItems = array();
	
	public static function Menu($model = null)
	{
		$c = Yii::app()->controller;
		$cname = $c->id;
		$action = $c->getAction()->getId();
		$plu = $cname == 'help' ? 'Help Topics' : ucwords($c->getId());
		$sin = Formatter::Singular($plu);

		$can = $c->getAccessRules(1);
		$items = self::$menuItems;
		$hasMap = $sin == 'Order' || $sin == 'Buyer' || $sin == 'Distributor';
		
		if ($action == 'create') {
			if (self::Can($can, 'index')) $items[] = array('label'=>'List ' . $plu, 'url'=>array('index'));
			if (self::Can($can, 'admin')) $items[] = array('label'=>'Manage ' . $plu, 'url'=>array('admin'));
		}

		if ($action == 'admin') {
			if (self::Can($can, 'index')) $items[] = array('label'=>'List ' . $plu, 'url'=>array('index'));
			if ($hasMap && self::Can($can, 'map')) $items[] = array('label'=>'View on Map', 'url'=>array('map'));
			if (self::Can($can, 'create')) $items[] = array('label'=>'Create ' . $sin, 'url'=>array('create'));
		}

		if ($action == 'index') {
			if (self::Can($can, 'create')) $items[] = array('label'=>'Create ' . $sin, 'url'=>array('create'));
			if (self::Can($can, 'admin')) $items[] = array('label'=>'Manage ' . $plu, 'url'=>array('admin'));
			if ($hasMap && self::Can($can, 'map')) $items[] = array('label'=>'View on Map', 'url'=>array('map'));
		}

		if ($action == 'map') {
			if (self::Can($can, 'index')) $items[] = array('label'=>'List ' . $plu, 'url'=>array('index'));
		}

		if ($action == 'update' || $action == 'pay' || $action == 'purge') { //anything that shd link back to view (order)
			if ($model == null) throw new Exception("model is expected");
			if (self::Can($can, 'view')) $items[] = array('label'=>'View ' . $sin, 'url'=>array('view', 'id'=>$model->id));
			if (self::Can($can, 'index')) $items[] = array('label'=>'List ' . $plu, 'url'=>array('index'));
			if (self::Can($can, 'create')) $items[] = array('label'=>'Create ' . $sin, 'url'=>array('create'));
			if (self::Can($can, 'admin')) $items[] = array('label'=>'Manage ' . $plu, 'url'=>array('admin'));
		}

		if ($action == 'view' || $action == 'me') {
			if ($model == null) throw new Exception("model is expected");
			if (self::Can($can, 'index')) $items[] = array('label'=>'List ' . $plu, 'url'=>array('index'));
			if (self::Can($can, 'create')) $items[] = array('label'=>'Create ' . $sin, 'url'=>array('create'));
			if (self::Can($can, 'update')) $items[] = array('label'=>'Update ' . $sin, 'url'=>array('update', 'id'=>$model->id));
			if (self::Can($can, 'delete')) $items[] = array('label'=>'Delete ' . $sin, 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id), 'confirm'=>'Are you sure you want to delete this '  . $sin . '?'));
			if (self::Can($can, 'admin')) $items[] = array('label'=>'Manage ' . $plu, 'url'=>array('admin'));
			if ($c->user_is('superadmin') && ($cname == 'distributors' || $cname == 'buyers')) {
				$items[] = User::createFor($model, 1);
				$items[] = User::createFor($model, 2);
				$items[] = User::createFor($model, 3);
			}
			if ($cname == 'buyers')
			{
				$what = $c->user_is('buyer') ? 'me' : $model->id;
				$items[] = array('label'=>'Show Map with Addresses / Distributors', 'url'=>array('/buyers/mapof/' . $what));
			}
		}

		return $items;
	}
}
?>
