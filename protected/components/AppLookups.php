<?php
/**
 * @author Imran Ali Namazi <imran@cselian.com>
 * @link http://tg.cselian.com/showcase/e-vend
 * @copyright Copyright &copy; 2012-2013 cselian.com
 * @license http://tg.cselian.com/licenses/e-vend
 * The Enums, Lookups and cached stuff of the Application
 */

class AppLookups
{
	private static $distributors;
	
	public static function clearCache()
	{
		Yii::app()->cache->flush();
	}
	
	public static function selectUserTypes()
	{
		$can = self::$userTypes;
		if (!UserIdentity::context('buyer')) unset($can[self::$userTypeBuyer]);
		if (!UserIdentity::context('distributor')) unset($can[self::$userTypeDistributor]);
		$type = self::currentUserType();
		echo CHtml::form();
		echo CHtml::dropDownList('changeuser', $type, $can, array('onchange' => 'this.form.submit()'));
		echo CHtml::endForm();
	}
	
	public static function currentUserType($name = 0)
	{
		if (isset($_POST['changeuser']))
			$id = Yii::app()->session['userType'] = $_POST['changeuser'];
		else if (isset(Yii::app()->session['userType']))
			$id = Yii::app()->session['userType'];
		else if (Yii::app()->user->isGuest)
			$id = -2;
		
		return $name ? self::UserType($id): $id;
	}
	
	public static function canUserSee($what)
	{
		//1 = SuperAdmin, 2 =Distributor, 3 = Buyer (Dealer / ETrader / Corporate), 4 = Salesman
		if ($what == 'priceCalculation')
			return self::currentUserType() == 1;
		else if ($what == 'paymentDetails')
			return ($ut = self::currentUserType()) == 1 || $ut == 3;
		throw new Exception('User Visiblity Rule not configured for ' . $what);
	}
	
	public static function UserType($id, $buyer = null)
	{
		if ($buyer != null) return	self::$buyerTypes[$buyer->type];
		return self::$userTypes[$id];
	}

	/**
	 * Gets the user types from the allowed list
	 * @param string $allowed. The accesible_to property of the Help / Reports etc
	 * @param mixed $vals. If 0, returns keys (use in edit), if 1, returns vals (use in view)
	*/
	public static function UserTypes($allowed, $vals = 0)
	{
		$allowed = explode(',', $allowed);
		$list = array();
		foreach ($allowed as $i)
		{
			$i = intval($i);
			if (!isset(self::$userTypes[$i])) continue;
			$list[] = $vals ? self::$userTypes[$i] : $i;
		}
		if ($vals == 1) $list = implode(', ', $list);
		return $list;
	}
	
	public static $userTypeDistributor = 2;
	public static $userTypeBuyer = 3;
	
	public static $userTypes = array(
		1 => 'Super Admin',
		2 => 'Distributor', //Admin / Salesman
		3 => 'Buyer', // Admin / Employee (for B2B)
		-2 => 'Guest',
	);
	
	public static $userTypeNames = array(
		'superadmin',
		'distributor',
		'buyeradmin', // for report
		'buyer'
	);
	
	public static function BuyerType($type)
	{
		if ($type == '' || !isset(self::$buyerTypes[$type])) return '';
		return self::$buyerTypes[$type];
	}

	public static $buyerTypes = array(
		1 => 'ETrader',
		2 => 'Corporate',
		3 => 'B2B',
		4 => 'Customer',
		5 => 'Franchisee',
		6 => 'Global',
	);
	
	public static function OrderStatus($id)
	{
		return self::$orderStatuses[$id];
	}

	public static $orderStatuses = array(
		1 => 'Created',
		2 => 'Under Processing',
		3 => 'Despatched',
		4 => 'Delivered',
		9 => 'Cancelled',
	);
	
	public static function PaymentType($type)
	{
		return	self::$paymentTypes[$type];
	}

	/**
	 * Gets the payment types from the allowed list
	 * @param string $allowed. The payment_types property of the Buyer
	 * @param mixed $vals. If 0, returns keys (use in edit), if 1, returns vals (use in view), if 'array', used as datasource
	*/
	public static function PaymentTypes($allowed, $vals = 0)
	{
		$allowed = explode(',', $allowed);
		$list = array();
		foreach ($allowed as $i)
		{
			$i = intval($i);
			if (!isset(self::$paymentTypes[$i])) continue;
			if ($vals == 'array')
				$list[$i] = self::$paymentTypes[$i];
			else
				$list[] = $vals ? self::$paymentTypes[$i] : $i;
		}
		if ($vals == 1) $list = implode(', ', $list);
		return $list;
	}

	public static $paymentTypeGateway = 1;

	public static $paymentTypes = array(
		1 => 'Gateway',
		2 => 'COD',
		3 => 'NEFT',
	);
	
	public static $approvalTypes = array(
		0 => 'None',
		1 => 'Before Payment',
		2 => 'After Payment (rejection / pyt reversal)',
	);
	
	private static $cart; // since session returns from request
	
	public static function Cart($data = null)
	{
		$cart = self::$cart != null ? self::$cart : Yii::app()->session['cart'];
		if (is_array($data))
		{
			$cart = $data;
			Yii::app()->session['cart'] = $data;
			return;
		}
		
		if ($data == 'count')
			return $cart == null ? false : count($cart);
		return $cart !== false && $cart != null ? $cart : array();
	}

	public static function Distributors($id = null)
	{
		if (self::$distributors != null) return self::$distributors;
		$distributors = Yii::app()->cache->get('distributors');
		if (!$distributors)
		{
			$model = new Distributor;
			$items = Distributor::model()->findAllBySql('select id, name from distributors order by id');
			$distributors = array();
			foreach ($items as $itm)
				$distributors[$itm->id] = $itm->name;
			Yii::app()->cache->set('distributors', $distributors);
		}
		self::$distributors = $distributors;
		return $id != null ? $distributors[$id] : $distributors;
	}
}
?>
