<?php
/**
 * @author Imran Ali Namazi <imran@cselian.com>
 * @link http://tg.cselian.com/showcase/e-vend
 * @copyright Copyright &copy; 2012-2013 cselian.com
 * @license http://tg.cselian.com/licenses/e-vend
 * Checks the condition and decides to log an error / message and tells whether any fields have been dirtied or not
 */

class FieldSetter
{
	public $dirty = 0;
	public $errors = array();
	public $messages = array();
	
	public function getValue($old, $new, $condition, $msg, $error)
	{
		if ($condition)
		{
			$this->dirty = 1;
			$this->messages[] = $msg;
			return $new;
		}
		else
		{
			$this->errors[] = $error;
			return $old;
		}
	}
	
	public function addErrorIfOneOf($msg, $val, $disallowed, $errorFmt, $action, $values)
	{
		if (array_search($val, $disallowed) === false)
		{
			$this->messages[] = $msg;
			$this->dirty = 1;
			return false;
		}

		$error = sprintf($errorFmt, $action, $values[$val]);
		$this->errors[] = $error;
		return true;
	}
}
?>
