<?php
/**
 * @author Imran Ali Namazi <imran@cselian.com>
 * @link http://tg.cselian.com/showcase/e-vend
 * @copyright Copyright &copy; 2012-2013 cselian.com
 * @license http://tg.cselian.com/licenses/e-vend
 */

class AppMails
{
	private static $what;
	private static $buyer;
	private static $template;
	private static $templateIsHtml;
	
	public static function names()
	{
		return array(
			'order-created', 'order-approved', 'order-rejected', 'order-paid'
			, 'order-distrib', 'order-approval'
			, 'usercreation', 'user-deactivated', 'user-reactivated', 'userreset', 'usersignup'
		);
	}
	
	public static function getDir($env = null)
	{
		if ($env == null) $env = AppEnv::get('code', 'Site Code');
		$dir = Controller::site_file('protected/data/' . $env . '/');
		if (!is_dir($dir)) mkdir($dir);
		return $dir;
	}
	
	public static function send($what, $model, $data = array())
	{
		self::$template = null;
		self::$templateIsHtml = false;
		self::$what = $what;
		$modelType = get_class($model);
		if ($modelType == 'Order' || $modelType == 'User') self::$buyer = $model->buyer_id;
		
		if ($what == 'contact')
		{
			$us = Yii::app()->params['adminEmail'];
			$subject = sprintf('%s - %s - from %s', Yii::app()->name, $model->subject, $model->name);
			$body = self::getContent($what, array('msg' =>$model->body, 'name' => $model->name));
			self::sendSmtp($model->email, $subject, $body , $us);
			return;
		}
		else if ($what == 'order-created' || $what == 'order-approved' || $what == 'order-rejected' || $what == 'order-paid')
		{
			$what2 = substr($what, 6);
			$to = $model->user->email; // Must send to order owner, not UserIdentity::context('userEmail');
			$subject = sprintf('Order #%s %s', $model->id, $what2);
			
			if ($what2 == 'rejected')
				$contact = self::link('site/contact', 'contact us');
			else if ($what2 == 'paid')
				$payment = 'not set';
			else if ($model->payment_type == 3) // NEFT
				$payment = $model->despatcher != null ? 'Please do an NEFT to ' . $model->despatcher->payment_info . ' before we can process your order' : 'We will inform you how to make the payment';
			else if ($model->payment_type == 2) // COD
				$payment = 'You have chosen to pay Cash on Delivery';
			else if ($model->payment_type == 1) // Gateway
				$payment = 'Make sure to make the payment online ' . self::link('orders/pay/'. $model->id) . ' / view the details if already paid.'
				 . (AppEnv::valOrDefault($data, 'approval_required', 0) ? 'You will have to wait for approval before being able to pay' : '');
			
			$emlData = array(
				'name' => $model->user->name,
				'id' => $model->id,
				'despatcher' => $model->despatcher != null ? $model->despatcher->name : '*Not Set*',
				'url' => self::link('orders/'. $model->id),
				'verification' => '<large><b>' . $model->verification . '</b></large>',
				'value' => Formatter::amount($model->amount),
				'details' => $model->getDetails(0),
				'details-inline' => $model->getDetailsInline(),
				'note' => Formatter::Concat('',
					self::optional(AppBuyers::isSponsored($model->buyer_id), AppEnv::getArrayVal('sponsored', 'note', 'Sponsored Config')),
					self::optional(AppBuyers::canPurge($model->buyer_id), AppHelper::replaceAdminVals(AppEnv::get('purge-note', 'Purge Mail Mesage')))
				),
				'approval' => self::optional(AppEnv::valOrDefault($data, 'approval_required', 0), AppEnv::get('mail-approval','Approval Message', 'This order is Pending Approval with your company')),
			);
			
			if ($what2 == 'paid') {
				$emlData['address'] = Address::format($model->address, $model->buyer, 1);
				$emlData['adminmail'] = Yii::app()->params['adminEmail'];
				$emlData['adminphone'] = Yii::app()->params['adminPhone'];
			} else if ($what2 == 'rejected') {
				$emlData['contact'] = $contact;
			} else {
				$emlData['payment'] = $payment;
			}
			
			$body = self::getContent($what, $emlData);
		}
		else if ($what == 'order-distrib')
		{
			$to = $model->despatcher->office_email;
			$subject = sprintf('Order #%s created by %s of %s', $model->id, $model->user->email, Buyer::format($model->buyer, 1));
			$body = self::getContent($what, array(
				'buyer' => Buyer::format($model->buyer, 1),
				'id' => $model->id,
				'name' =>  $model->despatcher->name,
				'url' => self::link('orders/'. $model->id),
				'taxinfo' => $model->buyer->tax_info != '' ? $model->buyer->tax_info : '[*Despatcher not set]',
				'number' => $model->address->phone,
				'address' => Address::format($model->address, $model->buyer, 1),
				'value' => Formatter::amount($model->amount),
				'details' => $model->getDetails(1),
			));
		}
		else if ($what == 'order-approval')
		{
			if ($model->buyer->notify == '') return;
			$to = $model->buyer->notify;
			$subject = sprintf('Order #%s created by %s of %s needs approval', $model->id, $model->user->email, Buyer::format($model->buyer, 1));
			$body = self::getContent($what, array(
				'buyer' => Buyer::format($model->buyer, 1),
				'id' => $model->id,
				'user' =>  UserIdentity::userName(),
				'url' => self::link('orders/update/'. $model->id),
				'value' => Formatter::amount($model->amount),
				'details' => $model->getDetails(0),
			));
		}
		else if ($what == 'usercreation')
		{
			$to = $model->email;
			$subject = sprintf('Account created for ' . User::forWhom($model));
			$body = self::getContent($what, array(
				'name' => $model->name,
				'email' => $model->email,
				'password' => $data['pwd'],
				'sitename' => Yii::app()->name,
				'role' => AppLookups::UserType($model->type) . ($for != null ? ' (' . $for . ')' : ''),
				'url' => self::link('site/login'),
			));
		}
		else if ($what == 'user-deactivated' || $what == 'user-reactivated')
		{
			$what2 = substr($what, 5);
			$to = $model->email;
			$subject = sprintf('Account '.$what2.' for ' . User::forWhom($model));
			$body = self::getContent('useractivation', array(
				'name' => $model->name,
				'sitename' => Yii::app()->name,
				'action' => $what2,
				'reason' => isset($data['reason']) && $data['reason'] != '' ? $data['reason'] : 'None given',
				'additionally' => $what2 == 'reactivated'
					? 'You can once again login ' . self::link('site/login') . '.'
					: 'If this was a mistake, you can complain ' . self::link('site/contact') . '.'
			));
		}
		else if ($what == 'userreset')
		{
			$to = $model->email;
			$subject = sprintf('Reset password at %s for %s', Yii::app()->name, User::forWhom($model));
			$body = self::getContent($what, array(
				'name' => $model->name,
				'email' => $model->email,
				'password' => $data['pwd'],
				'sitename' => Yii::app()->name,
				'role' => AppLookups::UserType($model->type) . ($for != null ? ' (' . $for . ')' : ''),
				'url' => self::link('site/reset/?k=' .$model->reset_code . '&e=' . $model->email, 'this link'),
			));
		}
		else if ($what == 'usersignup')
		{
			$to = $model->email;
			$subject = sprintf('Activate your account at %s for %s (%s)', Yii::app()->name, User::forWhom($model), $model->email);
			$body = self::getContent($what, array(
				'name' => $model->name,
				'email' => $model->email,
				'password' => $data['pwd'],
				'sitename' => Yii::app()->name,
				'role' => AppLookups::UserType($model->type) . ($for != null ? ' (' . $for . ')' : ''),
				'url' => self::link('site/reset/?activate=1&k='.$model->reset_code . '&e=' . $model->email, 'this link'),
			));
		}
		else
		{
			throw new Exception('No Mail defined for type ' . $what);
		}
		
		//print_r($body); die();
		self::sendMail($to, $subject, $body);
	}
	
	private static function sendMail($email, $subject, $message)
	{
		$test = Yii::app()->params['testEmail'];
		if ($test)
		{
			if ($test == 'SELF') $test = UserIdentity::context('userEmail');
			$message .= sprintf('*<b>NB</b>: To test, sending mail to "%s" instead of intended recipient %s', $test, $email);
			if (self::$template != null) $message .= ' and using template: ' . self::$template;
			$email = $test;
		}
		else if (self::$template != null)
		{
			$message .= '<!-- CS/DIM/Mail Template:' . self::$template . '-->';
		}
		
		self::sendSmtp($email, $subject, $message);
	}
	
	private static function sendSmtp($email, $subject, $message, $cc = 0)
	{
		$message = wordwrap($message, 120);
		$message = self::adjustBody($message, self::$templateIsHtml);
		
		if (isset($_GET['previewemail'])) {
			$fmt = '<b>%s</b>: %s<br>' . PHP_EOL;
			echo '<div class="email">'
				. sprintf($fmt, 'Template', ucwords(str_replace('-', ' ', self::$what)))
				. sprintf($fmt, 'From', Yii::app()->name . ' (' . Yii::app()->mail->transportOptions['username'] . ')')
				. sprintf($fmt, 'Subject', $subject)
				. $message . '</div>';
			//return;
		}

		// http://www.yiiframework.com/extension/mail/
		Yii::import('application.extensions.yii-mail.YiiMailMessage');
		$mail = new YiiMailMessage($subject, $message, 'text/html');
		$emails = self::parseEmails($email);
		foreach ($emails as $eml) $mail->addTo($eml);
		$bcc = AppEnv::get('bcc', 'Mail Blind Carbon Copy');
		if ($cc) { $mail->setCc($cc); $mail->setReplyTo($cc); } // contact
		else if ($bcc) { $mail->setBcc($bcc); $mail->setReplyTo($bcc); }
		$mail->from = array(Yii::app()->mail->transportOptions['username'] => Yii::app()->name);
		Yii::app()->mail->send($mail);
	}
	
	public static function adjustBody($body, $html = false)
	{
		if ($html)
			return str_replace('src="', 'src="' . Yii::app()->createAbsoluteUrl('/img/mails') . '/', $body);
		return str_replace("
", "<br />
", $body);
	}
	
	public static function parseEmails($email)
	{
		$list = explode(';', $email);
		foreach ($list as $k=>$v)
			$list[$k] = trim($v);
		return $list;
	}
	
	private static function link($where, $txt = 'here')
	{
		return CHtml::link($txt, Yii::app()->createAbsoluteUrl($where));
	}
	
	// if not double eol and not putting the line break in the template, will get double lines when optional is not there
	private static function optional($cond, $msg)
	{
		return $cond ? $msg . PHP_EOL . PHP_EOL : '';
	}
	
	private static function getContent($what, $data)
	{
		$data['signature'] = 'The ' . Yii::app()->name . ' Team<br/>'
			. CHtml::image(Yii::app()->createAbsoluteUrl('/img/email-logo.png'), 'intermart logo', array('height'=> 40));

		$tpl = self::getTemplate($what);
		foreach ($data as $key=>$val)
		{
			$key = sprintf('{%s}', $key);
			$tpl = str_replace($key, $val, $tpl);
		}
		
		return $tpl;
	}
	
	private static function getTemplate($what)
	{
		$dir = self::getDir();
		$tpl = Mail::findMatch($dir, $what, self::$buyer); // no way to avoid cyclic reference
		if ($tpl)
		{
			self::$template = sprintf('%s/%s (%s)', AppEnv::get('code', 'Site Code'),
				$tpl['name'], $tpl['html'] ? 'html' : 'plain');
			self::$templateIsHtml = $tpl['html'];
			return file_get_contents($tpl['file']);
		}

		return Yii::app()->controller->site_file('protected/data/' . strtolower($what) . '.txt', 1);
	}
}
?>
