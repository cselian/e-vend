<?php
/**
 * @author Imran Ali Namazi <imran@cselian.com>
 * @link http://tg.cselian.com/showcase/e-vend
 * @copyright Copyright &copy; 2012-2013 cselian.com
 * @license http://tg.cselian.com/licenses/e-vend
 */

class AppStock
{
	private static $lastDis;
	private static $outOfStock;
	
	public static function clearCache()
	{
		$dis = Controller::user_is('distributor') ? UserIdentity::context('distributor') : Distributor::Deflt('id');
		Yii::app()->cache->get(self::cacheKey($dis));
		FlashMessages::AddCurrent('Cleared Stock cache for Distributor ' . AppLookups::Distributors($dis));
	}
	
	public static function displayStock($product)
	{
		$dis = Controller::user_is('distributor') ? UserIdentity::context('distributor') : Distributor::Deflt('id');
		$list = explode(',', $product->variations);
		$tbl = AppHelper::buildTable(array('Variation', 'Balance'), 'tblsmall');
		foreach ($list as $v)
		{
			$where = sprintf("distributor_id = %s AND product_id = %s AND variation = '%s'", $dis, $product->id, $v);
			$s = Stock::model()->find($where);
			if ($s == null) $s = 'Not Set';
			else $s = $s->balance;
			$tbl->add_row(Product::variation_r($v), $s);
		}
		return $tbl->generate();
	}
	
	public static function AvailableVariations($product)
	{
		$list = explode(',', $product->variations);
		$res = array('mixed' => 'Colours');
		self::SetOutOfStock();

		foreach ($list as $v)
		{
			$key = Stock::variationKey($product->id, $v);
			if (isset(self::$outOfStock[$key]) == false)
				$res[$v] = Product::variation_r($v);
		}
		
		return $res;
	}
	
	public static function UpdateStock($order, $dis, $increase = 0)
	{
		if ($order->gatewayAndUnpaid() || $dis == '')
			return false;
		
		self::SetOutOfStock($dis);
		foreach ($order->orderItems as $itm)
			self::ChangeStock($itm->product_id, $itm->variation, $itm->quantity, $increase);
		
		return true;
	}
	
	private static function SetOutOfStock($dis = null)
	{
		if ($dis == null) $dis = Distributor::deflt('id');
		if (self::$lastDis != $dis) { self::$lastDis = $dis; self::$outOfStock == null; }
		if (self::$outOfStock != null || self::$outOfStock = Yii::app()->cache->get(self::cacheKey($dis)) != null)
			return;
		
		self::$outOfStock = array();
		$items = Stock::model()->findAllBySql('select product_id, variation from stock where distributor_id = '
			. $dis . ' AND balance <= 0');
		
		foreach ($items as $i)
			self::$outOfStock[Stock::variationKey($i['product_id'], $i['variation'])] = 1;
		
		Yii::app()->cache->set(self::cacheKey($dis), self::$outOfStock);
	}
	
	// Changes stock for current distributor
	private static function ChangeStock($product, $variation, $by, $increase)
	{
		$where = sprintf("distributor_id = %s AND product_id = %s AND variation = '%s'", self::$lastDis, $product, $variation);
		$s = Stock::model()->find($where);
		if ($s == null) return;
		if ($increase && $s->balance <= 0)
			$out = false;
		else if ($decrease && $s->balance - $by <= 0)
			$out = true;
		
		if (isset($out))
		{
			$key = Stock::variationKey($product, $variation);
			if ($out)
				self::$outOfStock[$key] = 1;
			else
				unset(self::$outOfStock[$key]);
			
			Yii::app()->cache->set(self::cacheKey($dis), self::$outOfStock);
		}
		
		$s->balance += $by * ($increase ? 1 : -1);
		$s->save();
	}
	
	private static function cacheKey($dis)
	{
		return 'outOfStock-' . $dis;
	}
}
?>
