<?php
/**
 * @author Imran Ali Namazi <imran@cselian.com>
 * @link http://tg.cselian.com/showcase/e-vend
 * @copyright Copyright &copy; 2012-2013 cselian.com
 * @license http://tg.cselian.com/licenses/e-vend
 * Add FlashMessages to session for display on next page / current one
 */

class FlashMessages
{
	private static $msg;
	
	static function AddCurrent($message, $type = 'notice')
	{
		self::$msg = array('text' => $message, 'type' => $type);
	}
	
	static function HasCurrent()
	{
		return self::$msg != null;
	}
	
	// type can be notice, error or success (see main.css)
	static function Add($message, $type = 'notice')
	{
		$id = self::Id();
		Yii::app()->session['message'] = array('source' => $id, 'text' => $message, 'type' => $type);
	}
	
	static function Display($outer = 1)
	{
		if (self::$msg != null && $outer) {
			$msg = self::$msg;
			self::Display(0);
		} else {
			$id = self::Id();
			if (!isset(Yii::app()->session['message'])) return;
			$msg = Yii::app()->session['message'];
			if ($msg['source'] == $id) return;
			unset(Yii::app()->session['message']);
		}
		echo sprintf('<div class="flash-%s">%s</div>', $msg['type'], $msg['text']);
	}
	
	function displayActions($url, $actions, $action = null, $postHtml = null)
	{
		echo '<div id="actions">';
		$url = Yii::app()->request->baseUrl . $url;
		foreach ($actions as $k=>$v)
		{
			$u = strpos($k, "/") ? $k : $url . $k;
			echo '<a ' . ($k == $action ? 'class="current" ' : '') .
				'href="' . $u . '"> ' . $v . '</a>';
			}
		if ($postHtml != null) echo $postHtml;
		echo '</div>';
	}
	
	private static function Id()
	{
		$c = Yii::app()->controller;
		return $c->getId() . '_' . $c->getAction()->getId();
	}
}
?>
