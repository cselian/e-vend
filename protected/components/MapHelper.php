<?php
/**
 * @author Imran Ali Namazi <imran@cselian.com>
 * @link http://tg.cselian.com/showcase/e-vend
 * @copyright Copyright &copy; 2012-2013 cselian.com
 * @license http://tg.cselian.com/licenses/e-vend
 */

class MapHelper
{
	public static $linkLabel = 'Map Location';
	private static $do;
	private static $script;
	private static $center;
	
	private static $points = array();
	private static $count = 1;
	
	private static function nextLetter()
	{
		$int = 65 + self::$count++ % 26;
		return  chr($int);
	}
	
	public static function link($data, $text = null)
	{
		if (is_array($data)) {
			$lat = $data['lat'];
			$lon = $data['lon'];
		}else  {
			$lat = $data->lat;
			$lon = $data->lon;
		}
		if ($lat == 0 && $lon == 0) return '';
		if ($text == null) $text = $lat . ' / ' . $lon;
		$mapLink = '<a href="http://maps.google.com/?q=%s,%s">%s</a>';
		return sprintf($mapLink, $lat, $lon, $text);
	}
	
	public static function getDistance($fm, $to)
	{
		// http://snipplr.com/view/2531/
		$pi80 = M_PI / 180;
		$lat1 = $fm->lat * $pi80;
		$lng1 = $fm->lon * $pi80;
		$lat2 = $to->lat * $pi80;
		$lng2 = $to->lon * $pi80;
	 
		$r = 6372.797; // mean radius of Earth in km
		$dlat = $lat2 - $lat1;
		$dlng = $lng2 - $lng1;
		$a = sin($dlat / 2) * sin($dlat / 2) + cos($lat1) * cos($lat2) * sin($dlng / 2) * sin($dlng / 2);
		$c = 2 * atan2(sqrt($a), sqrt(1 - $a));
		$km = $r * $c;
		
		$miles = 0;
		return $miles ? ($km * 0.621371192) : $km;
	}
	
	public static function getDistance_r($dist)
	{
		return sprintf('%s km', round($dist, 3));
	}
	
	public static function register()
	{
		self::$do = 1;
	}
	
	public static function hasMap()
	{
		return self::$do != null;
	}
	
	public static function addHome()
	{
		//http://universimmedia.pagesperso-orange.fr/geo/loc.htm
		self::$center =  array_merge(
			Yii::app()->params['mapCenter'],
			array('id' => 'Home', 'name' => Yii::app()->name . ' home')
		);

		self::$script = '<script type="text/javascript">
  var locations = [
';
		self::$script .= self::addLocation(self::$center, $fmt, null);
	}
	
	public static function addLocation($data, $fmt, $name, $color = 'paleblue')
	{
		$mkFmt = "    ['%s', %s, %s, '%s'],
";
		$id = $data['id'] != 'Home' ? $data['id'] : -1;
		$link = $data['id'] != 'Home' ? sprintf($fmt, $data['id'], $name . ' ') : $data['id'];
		$caption = sprintf('<b>%s</b><br />%s<br />%s, %s', $link, $data['name'], $data['address'], $data['pin']);
		$letter = $data['id'] == 'Home' ? '*' : self::nextLetter();
		$icon = $data['id'] == 'Home' ? 'red_MarkerA.png' : sprintf('%s_Marker%s.png', $color, $letter);
		$scr = sprintf($mkFmt, $caption, self::coords($data), $id, $icon);
		self::$points[] = array('id' => $id, 'link' => $link, 'caption' => $caption, 'icon' => $icon, 'letter' => $letter);
		self::$script .= $scr;
	}
	
	public static function printMap()
	{
		$fol = Yii::app()->request->baseUrl . '/img/markers/';
		global $sidebarBot;
		$sidebarBot = Yii::app()->controller->renderPartial('/mapkey', 
			array('fol' => $fol, 'points' => self::$points), 1);

		self::$script .= "  ];

  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 11,
    center: new google.maps.LatLng(" . self::coords(self::$center) . "),
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });

  var infowindow = new google.maps.InfoWindow();

  var marker, i;
  var markers = new Array();
  function showMarker(id)
  {
    var m = markers[id];
    map.setCenter(m.getPosition());
  }

  for (i = 0; i < locations.length; i++) {
    marker = new google.maps.Marker({
      position: new google.maps.LatLng(locations[i][1], locations[i][2]),
      map: map,
      icon: '" . $fol ."' + locations[i][4]
    });
    markers[i] = marker;

    google.maps.event.addListener(marker, 'click', (function(marker, i) {
      return function() {
        infowindow.setContent(locations[i][0]);
        infowindow.open(map, marker);
      }
    })(marker, i));
  }
</script>";

		echo self::$script;
	}
	
	function coords($where)
	{
		return $where['lat'] . ', ' . $where['lon'];
	}
}
?>
