<?php
/**
 * @author Imran Ali Namazi <imran@cselian.com>
 * @link http://tg.cselian.com/showcase/e-vend
 * @copyright Copyright &copy; 2012-2013 cselian.com
 * @license http://tg.cselian.com/licenses/e-vend
 * Holds Buyer Info (Which ones should only be shown priced products, what payment types etc)
 */

class AppBuyers
{
	private static $buyerInfo;

	// used in Pricing Report, Mails
	public static function getList()
	{
		return self::getInfo('names');
	}
	
	public static function getName($buyer)
	{
		$names = self::getInfo('names', $buyer);
		return isset($names[$buyer]) ? $names[$buyer] : false;
	}

	public static function productCriteria()
	{
		if (self::getInfo('productCri') == 0) return 0;
		
		$buyerProducts = AppPricing::getBuyerProducts();
		if (count($buyerProducts) == 0)
			throw new Exception("Must Configure Pricing for products of Buyer when priced_only is set to true");
		
		$ids = array();
		foreach ($buyerProducts as $id => $p)
			$ids[] = $id;
		$ids = implode(', ', $ids);
		return 'id in (' . $ids . ')';
	}

	public static function paymentTypes()
	{
		$res = self::getInfo('pytTypes');
		if (count($res) == 0) throw new Exception("This buyer has no payment_types configured. Please contact the administrator");
		return $res;
	}

	public static function getBool($key, $buyer = null)
	{
		if ($buyer == null && !Controller::user_is('buyer')) return false;
		$cfg = AppEnv::get('buyerConfig', 'Temporary Buyer Config');
		if ($buyer == null) $buyer = UserIdentity::context('buyer');
		
		return isset($cfg[$buyer]) && isset($cfg[$buyer][$key]) ? $cfg[$buyer][$key] : false;
	}

	public static function isApprovalRequired()
	{
		return self::getInfo('approval');
	}

	public static function isSponsoredWithRadio()
	{
		return self::isSponsored()
			&& self::sponsoredConfig('restrictItems') === 1
			&& self::sponsoredConfig('restrictQuantity') === 1;
	}

	public static function isSponsored($buyer = null)
	{
		return self::getInfo('sponsored', $buyer);
	}

	public static function sponsoredConfig($what)
	{
		return AppEnv::getArrayVal('sponsored', $what, 'Sponsoring Buyer Cfg');
	}

	public static function canPurge($buyer = null)
	{
		return self::getInfo('purge', $buyer);
	}

	public static function ifConfiguredPGNames($names)
	{
		$use = self::isSponsored() ? self::sponsoredConfig('vpcnames') : false;
		return $use ? $use : $names;
	}

	public static function ifSponsoredMessage()
	{
		return self::isSponsored() ? self::sponsoredConfig('message') : '';
	}

	public static function signupTo($email)
	{
		$signups = self::getInfo('signups');
		$dom = explode('@', $email);
		if (count($dom) == 1) return 0;
		$dom = $dom[1];
		return isset($signups[$dom]) ? $signups[$dom] : sprintf('No buyer has been configured to signup from email domain "%s"', $dom);
	}

	private static function getInfo($name, $buyer = null)
	{
		if (self::$buyerInfo == null)
			self::$buyerInfo = self::getBuyerInfo('pcBuyerInfo');
		if ($name == 'signups' || $name == 'names') return self::$buyerInfo[$name];

		if ($buyer == null) $buyer = UserIdentity::context('buyer');
		if (!isset(self::$buyerInfo[$buyer])) throw New Exception('No Buyer Info is configured for Buyer #' . $buyer);
		return self::$buyerInfo[$buyer][$name];
	}

	private static function getBuyerInfo($cacheAs)
	{
		$val = Yii::app()->cache->get($cacheAs);
		if ($val) return $val;
		
		$sql = 'select id, name, priced_only, payment_types, signup_from, approval_required, sponsored, `purge` from buyers';
		$model = new Buyer();
		$rows = $model->findAllBySql($sql);
		$val = array();
		$signups = array();
		$names = array();
		foreach ($rows as $itm)
		{
			$names[$itm->id] = $itm->name;
			$val[$itm->id] = array(
				'productCri' => $itm->priced_only,
				'pytTypes' => AppLookups::PaymentTypes($itm->payment_types, 'array'),
				'approval' => intval($itm->approval_required), // dunno why its always string? or isnt it?
				'sponsored' => $itm->sponsored,
				'purge' => $itm->purge,
			);
			if ($itm->signup_from != '')
			{
				$doms = explode(',', $itm->signup_from);
				foreach ($doms as $dom)
				{
					$dom = trim($dom);
					if (isset($signups[$dom])) throw new Exception (
						sprintf('Domain %s cannot be assigned to Buyers %s and %s', $dom, $signups[$dom], $itm->id));
					
					$signups[$dom] = $itm->id;
				}
			}
		}
		
		$val['signups'] = $signups;
		$val['names'] = $names;
		Yii::app()->cache->set($cacheAs, $val);
		return $val;
	}
}
