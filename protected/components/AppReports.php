<?php
/**
 * @author Imran Ali Namazi <imran@cselian.com>
 * @link http://tg.cselian.com/showcase/e-vend
 * @copyright Copyright &copy; 2012-2013 cselian.com
 * @license http://tg.cselian.com/licenses/e-vend
 */

class AppReports
{
	private static $reports;
	private static $inReport = false;
	
	public static function clearNamesCache()
	{
		Yii::app()->cache->delete('reportNames');
	}
	
	public static function getReport($slug)
	{
		if (self::$reports == null) self::getByType();
		return isset(self::$reports[$slug]) ? self::$reports[$slug] : null;
	}
	
	public static function isInReport($val = null)
	{
		if ($val == null) return self::$inReport;
		self::$inReport = $val;
	}
	
	public static function isAndAssumingDev()
	{
		$c = Yii::app()->controller;
		return $c->user_is('dev') && $c->user_is('superadmin');
	}
	
	public static function getMenu()
	{
		$byType = self::getByType();
		//print_r($byType); print_r(self::$reports);
		$who = 0;
		foreach (AppLookups::$userTypeNames as $w)
		{
			if (Yii::app()->controller->user_is($w))
			{
				$who = $w;
				break;
			}
		}
		$nonAdmin = AppLookups::currentUserType() != 1 && UserIdentity::context('admin') != 1;
		if (!$who || count($byType[$who]) == 0 || $nonAdmin) return array('visible' => 0);
		
		$items = array(
			array('label'=>'Index', 'url'=>array('/reports/'))
		);
		foreach ($byType[$who] as $slug)
		{
			$item = self::$reports[$slug];
			$items[] = array('label'=>$item->name, 'url'=>array('/reports/run/?name=' . $item->slug)
				, 'linkOptions' => array('title' => $item->description));
		}
		
		return array('label'=>'Reports', 'url'=>array('#'), 'items'=> $items);
	}

	private static function getByType()
	{
		self::$reports = Yii::app()->cache->get('reportList');
		$list = Yii::app()->cache->get('reportNames');
		if ($list) return $list;
		
		$items = Report::model()->findAll();
		self::$reports = array();

		foreach ($items as $item)
		{
			$data = new stdClass();
			foreach ($item->attributes as $key=>$value)
				$data->$key = $value;
			self::$reports[$data->slug] = $data;
		}
		
		foreach (AppLookups::$userTypeNames as $who)
		{
			$can = array();
			foreach ($items as $item)
			{
				if (Report::canAccess($item, $who))
					$can[] = $item->slug;
			}
			
			$list[$who] = $can;
		}

		Yii::app()->cache->set('reportNames', $list);
		Yii::app()->cache->set('reportList', self::$reports);
		return $list;
	}
}
?>
