<?php
/**
 * @author Imran Ali Namazi <imran@cselian.com>
 * @link http://tg.cselian.com/showcase/e-vend
 * @copyright Copyright &copy; 2012-2013 cselian.com
 * @license http://tg.cselian.com/licenses/e-vend
 */

class AppImport
{
	public static function hasImport($c, $model, $data = array())
	{
		// http://www.yiiframework.com/wiki/336/importing-csv-file-to-mysql-table-using-load-data-command/
		$csv = CUploadedFile::getInstance($model, 'file');
		return $csv != null;
	}
	
	public static function import($c, $model)
	{
		$csv = CUploadedFile::getInstance($model, 'file');
		if ($csv->getExtensionName() != 'csv')
		{
			FlashMessages::AddCurrent('Only csv files allowed. Try uploading "' . $csv->getName() . '" again.');
			return false;
		}
		
		$file = self::getPath($csv);
		$csv->saveAs($file);
		include_once 'csvr_helper.php';
		$rdr = new Csv($file);
		$rdr->load_file();
		$data = $rdr->get_contents(TRUE);
		if (count($data) == 0)
		{
			FlashMessages::AddCurrent('No rows found in "' . $csv->getName() . '".');
			return false;
		}
		
		$what = sprintf('%s-%s', $c->id, $c->getAction()->getId());
		if ($what == 'pricing-create')
			$ok = self::pricing($model, $data);
		else if ($what == 'products-create')
			$ok = self::products($model, $data);
		else if ($what == 'stock-create')
			$ok = self::stock($model, $data);
		else if ($what == 'users-create')
			$ok = self::users($model, $data);
		else
			throw new exception('Unknown import action: ' . $what);
		
		if ($ok) FlashMessages::Add('Imported ' . count($data) .  ' rows from "' . $csv->getName() . '".');
		return $ok;
	}

	private static function users($model, $data)
	{
		if (self::columnsInvalid($data, array('name', 'email', 'admin'))) return false;

		$nosave = array();
		$row = 1;
		foreach ($data as $itm)
		{
			$m = new User();
			$m->date_added = Formatter::dateForSql();
			$pwd = AppHelper::generatePassword(8, 1);
			$m->password = md5($pwd);
			
			$m->name = $itm['name'];
			$m->email = $itm['email'];
			$m->admin = $itm['admin'];
			
			$m->type = $model->type;
			$m->buyer_id = $model->buyer_id;
			$m->distributor_id = $model->distributor_id;
			
			if($m->save())
				AppMails::send('usercreation', $m, array('pwd' => $pwd));
			else
				$nosave[] = sprintf(' - row: %s, model: %s, error: %s', $row, $m->email, Formatter::errors($m));
			$row++;
		}

		if (count($nosave) > 0)
		{
			FlashMessages::AddCurrent('These products couldnt be saved: <br/>' . implode('<br/> ', $nosave));
			return false;
		}

		return true;
	}

	private static function products($model, $data)
	{
		if (!isset($data[0]['variations']))
			return self::productPrice($model, $data);
		
		if (self::columnsInvalid($data, array('brand', 'model', 'mrp', 'price', 'variations', 'specifications', 'promote', 'category'))) return false;
		$nosave = array();
		$row = 1;
		foreach ($data as $itm)
		{
			$m = new Product();
			$m = $m->find("model = '".$itm['model']."'");
			if ($m == null) $m = new Product();
			$m->brand = $itm['brand'];
			$m->model = $itm['model'];
			$m->category = $itm['category'];
			$m->mrp = $itm['mrp'];
			$m->price = $itm['price'];
			$m->variations = $itm['variations'];
			$m->specifications = $itm['specifications'];
			$m->promote = $itm['promote'];
			if (!$m->save())
				$nosave[] = sprintf(' - row: %s, model: %s, error: %s', $row, $m->model, Formatter::errors($m));
			$row++;
		}

		if (count($nosave) > 0)
		{
			FlashMessages::AddCurrent('These products couldnt be saved: <br/>' . implode('<br/> ', $nosave));
			return false;
		}

		return true;
	}
	
	private static function productPrice($model, $data)
	{
		if (self::columnsInvalid($data, array('model', 'mrp', 'price'))) return false;
		$noamt = array();
		$nomodel = array();
		$nosave = array();
		foreach ($data as $itm)
		{
			if ($itm['price'] == '' || $itm['mrp'] == '')
			{
				$noamt[] = $itm['model'];
				continue;
			}
			
			$m = $m->find("model = '".$itm['model']."'");
			if ($m == null)
			{
				$nomodel[] = $itm['model'];
				continue;
			}
			
			$m->price = $itm['price'];
			$m->mrp = $itm['mrp'];
			if (!$m->save())
				$nosave[] = sprintf(' - id: %s, model: %s, error: %s', $m->id, $m->model, Formatter::errors($m));
		}
		
		if (count($noamt) > 0 || count($nomodel) > 0 || count($nosave) > 0)
		{
			if (count($noamt) > 0) $msg = 'These models had invalid amounts: ' . implode(', ', $noamt);
			if (count($nomodel) > 0) $msg .= ($msg != null ? ' and<br/> ' : '') . 'These models could not be found: ' . implode(', ', $nomodel);
			if (count($nosave) > 0) $msg .= ($msg != null ? ' and<br/> ' : '') . 'These products couldnt be saved: <br/>' . implode('<br/> ', $nosave);
			FlashMessages::AddCurrent($msg);
			return false;
		}

		return true;
	}
	
	private static function pricing($model, $data)
	{
		if (self::columnsInvalid($data, array('model', 'discount', 'price'))) return false;
		if ($model->product_id != '' || $model->discount != '' || $model->price != '')
		{
			FlashMessages::AddCurrent('Cannot fill product, discount or price when Importing csv');
			return false;
		}
		else if ($model->buyer_type == '' && $model->buyer_id == '')
		{
			FlashMessages::AddCurrent('Must fill buyer / buyer type when Importing csv');
			return false;
		}
		
		// print_r($data);
		$m = new Pricing;
		$where = array();
		if ($model->buyer_id != '') $where[] = 'buyer_id = ' . $model->buyer_id;
		if ($model->buyer_type != '') $where[] = 'buyer_type = ' . $model->buyer_type;
		$where = implode(' and ', $where);
		$m->deleteAll($where);
		
		foreach ($data as $itm)
		{
			$m = new Pricing;
			if ($itm['discount'] != '') $m->discount = $itm['discount'];
			if ($itm['price'] != '') $m->price = $itm['price'];
			$m->buyer_id = $model->buyer_id;
			$m->buyer_type = $model->buyer_type;
			$prods = Product::model()->findAll("model = '" . $itm['model'] . "'");
			foreach ($prods as $p)
			{
				$m->product_id = $p->id;
				$m->setIsNewRecord(1);
				$m->setPrimaryKey(null);
				$m->save();
			}
		}
		
		return true;
	}
	
	private static function stock($model, $data)
	{
		if (self::columnsInvalid($data, array('model', 'variation', 'balance'))) return false;
		if ($model->distributor_id == '' && !Controller::user_is('distributor'))
		{
			FlashMessages::AddCurrent('Must enter distributor when Importing csv');
			return false;
		}
		
		$dis = Controller::user_is('distributor') ?  : $model->distributor_id;
		$noprod = array();
		
		foreach ($data as $itm)
		{
			if (isset($itm['pid']) && $itm['pid'] != '') {
				$pid = $itm['pid'];
				$p = Product::model()->count("id = '" . $itm['pid'] . "'");
				if ($p == 0) { $noprod[] = 'Product Id: ' . $itm['pid']; continue; }
			} else {
				$vcri = $itm['variation'] == '' ? "variations = ''" : "find_in_set('" . $itm['variation'] . "', variations)";
				$p = Product::model()->find(sprintf("model = '%s' AND %s", $itm['model'], $vcri));
				if ($p == null) { $noprod[] = sprintf('Model: %s, variation: "%s"', $itm['model'], $itm['variation']); continue; }
				$pid = $p->id;
			}
			
			$m = new Stock;
			$where = sprintf("distributor_id = %s AND product_id = %s AND variation = '%s'", $dis, $pid, $itm['variation']);
			$s = Stock::model()->find($where);
			if ($s == null) {
				$s = new Stock();
				$s->attributes = array('distributor_id' => $dis, 'product_id' => $pid, 'variation' => $itm['variation']);
			}
			
			$s->balance = $itm['balance'];
			$s->save();
		}

		if (count($noprod) > 0)
		{
			if (count($noprod) > 0) $msg = 'These products could not be found: <br/>' . implode('<br /> ', $noprod);
			FlashMessages::AddCurrent($msg);
			return false;
		}
		
		return true;
	}
	
	private static function columnsInvalid($data, $cols)
	{
		$missing = array();
		$row = $data[0];
		foreach ($cols as $c)
		{
			if (!isset($row[$c])) $missing[] = $c;
		}
		
		if (count($missing) == 0) return false;
		
		FlashMessages::AddCurrent('These columns are missing: ' . implode(', ', $missing));
		return true;
	}

	private static function getPath($csv)
	{
		$dir = Yii::app()->getRuntimePath().DIRECTORY_SEPARATOR.'uploads';
		if (!is_dir($dir)) mkdir($dir);
		$file = $dir . DIRECTORY_SEPARATOR. $csv->getName();
		if (file_exists($file)) unlink($file);
		return $file;
	}
	
}
