<?php
/**
 * @author Imran Ali Namazi <imran@cselian.com>
 * @link http://tg.cselian.com/showcase/e-vend
 * @copyright Copyright &copy; 2012-2013 cselian.com
 * @license http://tg.cselian.com/licenses/e-vend
 * 
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */

class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

	/* Whether to use * instead of the attribute name. Requires changes to Number / Reqd validator */
	public $useValidationAsterisk = false;
	
	public function getvalnHideAttr() { return $this->useValidationAsterisk; }
	
	// Set to application event in config
	public function onBeginApplicationRequest()
	{
		$cache = get_class(Yii::app()->cache);
		if ($cache == 'CFileCache') {
			$dir = Yii::app()->cache->cachePath . '-' . AppEnv::get('code', 'Env Code');
			if(!is_dir($dir)) mkdir($dir,0777,true);
			Yii::app()->cache->cachePath = $dir;
		} else {
			throw new Exception ('Make sure of environment cache separation for: ' . $cache);
		}
	}
	
	function user_is($what)
	{
		$user = Yii::app()->user;
		$type = AppLookups::currentUserType();
		if ($what == 'notloggedin') return $user->isGuest;
		if ($what == 'autologin') return self::user_is('buyer') && AppBuyers::canPurge();
		if ($what == 'loggedin') return !$user->isGuest;
		if ($what == 'superadmin') return $type == 1;
		if ($what == 'distributor') return $type == 2;
		if ($what == 'buyer') return $type == 3;
		if ($what == 'superorbuyeradmin') return (self::user_is('superadmin') ||
			(self::user_is('buyer')  && UserIdentity::context('admin') == 1) );
		if ($what == 'buyeradmin') return self::user_is('buyer') && UserIdentity::context('admin') == 1;
		if ($what == 'buyernonadmin') return self::user_is('buyer') && UserIdentity::context('admin') == 0;
		if ($what == 'salesman') return $type == 4;
		if ($what == 'admin') return UserIdentity::isAdministrator($user->name);
		if ($what == 'dev') return $user->name == 'imran';
		if ($what == 'nowdev') return $user->name == 'imran' && $type == 1;
		return false;
	}
	
	function getAccessRules($returnList = 0)
	{
		Buyer::isEnteringGlobalBuyerSite($this);
		$name = $this->getId();
		
		//1 = SuperAdmin, 2 =Distributor, 3 = Buyer (Dealer / ETrader / Corporate), 4 = Salesman
		$rules = array(
			'distributors' => 'superadmin',
			'pricing' => 'superadmin',
			'users' => 'superadmin',
			'mails' => 'superadmin',
			'stock' => 'distributor',
			'buyers' => array('index' => array(1, 2, 4), 'admin' => array(1), 'view' => array(1, 2, 4)
				, 'update' => array(1), 'create' => array(1), 'delete' => array(1)
				, 'map' => array(1, 2), 'mapof' => array(1, 2, 3, 4)),
			'products' => array('index' => array(1, 2, 3, 4), 'admin' => array(1), 'view' => array(1, 2, 3, 4)
				, 'update' => array(1), 'create' => array(1), 'delete' => array(1), 'import' => array(1)),
			'orders' => array('index' => array(1, 2, 3, 4), 'admin' => array(1), 'view' => array(1, 2, 3, 4)
				, 'update' => array(1, 2, 3), 'add' => array(3), 'delete' => array(1), 'purge' => array(1)
				, 'map' => array(1, 2), 'import' => array(2), 'pay' => array(1, 3)),
			'cart' => 'buyer',
			'addresses' => array('index' => array(1, 3), 'admin' => array(1), 'view' => array(1, 2, 3, 4)
				, 'update' => array(1, 3), 'create' => array(1, 3), 'delete' => array(1), 'distances' => array(1)),
			'help' => array('index' => array(-2, 0, 1, 2, 3, 4), 'admin' => array(0), 'view' => array(-2, 1, 2, 3, 4)
				, 'update' => array(0), 'create' => array(0), 'delete' => array(0) ),
		);
		
		if (!isset($rules[$name])) throw new Exception(sprintf("Access Rules not set for %s controller", $name));

		$action = $this->getAction()->getId();
		$what = $rules[$name];
		
		if ($action == 'me' || $action == 'updateme') // TODO: UpdateMe url
		{
			$me = array('buyers' => 'buyer', 'distributors' => 'distributor', 'users' => '*');
			if (!isset($me[$name])) throw new Exception(sprintf("%s not valid on controller %s", $action, $name));
			$who = $me[$name];
			$can = $who == '*' || $this->user_is($who);
		}
		else if (is_array($what))
		{
			$type = AppLookups::currentUserType();
			if ($type == 0) $type = -1;
			$dev = $this->user_is('dev') && $type == 1;
			if ($returnList)
			{
				$allowed = array();
				foreach ($what as $name=>$arr)
				{
					if (array_search($type, $arr) !== false) $allowed[] = $name;
					else if ($dev) $allowed[] = $name;
				}
				return $allowed;
			}
			
			if (!isset($what[$action])) throw new Exception(sprintf("Access Rules not set for action %s of %s controller", $action, $name));
			$can = $dev || array_search($type, $what[$action]) !== false;
		}
		else
		{
			if ($returnList) return '*';
			$can = $this->user_is($what);
			if ($name == 'users' && $action == 'signup' && $can == false) $can = 1;
		}
		
		return array(array( $can ? 'allow' : 'deny', 'users'=>array('*')));
	}
	
	function getGotoUrl()
	{
		$cart = $this->user_is('buyer') ? AppLookups::Cart('count') : 0;
		
		if ($cart) $goto = array('/cart/');
		else $goto = $this->user_is('buyeradmin') ? array('/orders/?approval=1') : Yii::app()->user->returnUrl;
		
		if ($goto == null || $goto == $this->site_url('index.php', 1))
			$goto = array('/products');
		
		return $goto;
	}

	function add_script($what, $css, $where = CClientScript::POS_READY)
	{
		$cs=Yii::app()->clientScript;
		$js = $this->site_file('protected/scripts/' .$what . '.js', 1);
		$cs->registerScript($what, $js , $where);
		if ($css) $cs->registerCss($what, $this->site_file('protected/scripts/' .$what . '.css', 1));
	}

	function site_file($rel, $contents = false)
	{
		global $fileRoot;
		if (!isset($fileRoot))
		{
			$fileRoot = $_SERVER['SCRIPT_FILENAME'];
			$pos = strripos($fileRoot, "/");
			$fileRoot = substr($fileRoot, 0, $pos + 1);
		}
		$rel = $fileRoot . $rel;
		return $contents ? file_get_contents($rel) : $rel;
	}
	
	function site_url($rel, $return = 0)
	{
		if (!strncmp($rel, 'http://', strlen('http://'))) return $rel;
		global $urlRoot;
		if (!isset($urlRoot))
		{
			$urlRoot = $_SERVER['SCRIPT_NAME'];
			$urlRoot = str_ireplace("index.php", "", $urlRoot);
		}
		$rel = $urlRoot . $rel;
		if ($return) return $rel;
		echo $rel;
	}
	
	protected function menuOnTop()
	{
		$this->layout = '//layouts/column2-vert';
	}
}
