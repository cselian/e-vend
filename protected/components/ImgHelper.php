<?php
/**
 * @author Imran Ali Namazi <imran@cselian.com>
 * @link http://tg.cselian.com/showcase/e-vend
 * @copyright Copyright &copy; 2012-2013 cselian.com
 * @license http://tg.cselian.com/licenses/e-vend
 */

class ImgHelper
{
	public static $newImg;
	public static $baseFol;
	public static $missing = array();
	public static $shown = array();
	
	public static $toggleBtn = '<span class="toggle">&nbsp;%s</span>';
	
	function buyer_logo($model = null)
	{
		$id = $model != null ? $model->id : UserIdentity::context('buyer');
		$name = $model != null ? $model->name : UserIdentity::context('buyerName');
		if ($model == null && !user_is('buyer')) return;
		$fol = AppEnv::get('buyerIconsFol', 'Buyer Icons Folder', '');
		$img = sprintf("img/logos%s/buyer-%s.jpg", $fol, $id);
		if (!file_exists(Controller::site_file($img))) return;
		echo ' ' . CHtml::image(Yii::app()->baseurl . '/'. $img, $name . '-logo', array('height' => 30));
	}

	function site_logo($model = null)
	{
		$img = AppEnv::get('siteLogo', 'Site Logo', false);
		if (!$img) return;
		$img = sprintf("img/logos/%s", $img);
		echo ' ' . CHtml::image(Yii::app()->baseurl . '/'. $img, $img, array('height' => 30));
	}
	
	function product_img($item, $url = 0)
	{
		$b = $item->brand; $m = $item->model;
		$ifmt = "%s-%s.jpg";
		$img = 'img/p/' . ($file = sprintf($ifmt, strtolower($b), $m));
		//echo $img;
		if (file_exists(Controller::site_file($img)))
		{
			$img = self::$baseFol . $img;
			self::$shown[] = $file;
		} else  {
			self::$missing[] = $file;
			$img = self::$baseFol . "img/p/default.jpg";
		}
		
		if ($url) return $img;
		
		$fmt = '<a href="#" data-src="%s" class="pvwlink">%s</a>';
		return sprintf($fmt, $img, $m);
	}
	
	function report($what)
	{
		$list = $what == 'missing' ? self::$missing : self::$shown;
		$nl = '
';
		sort($list);
		echo $nl . '<b>' . (count($list) == 0 ? 'No ' . $what . ' images'
			: ucfirst($what) . ' ' . count($list) . ' images</b>:') . $nl . implode($nl, $list) . $nl;
	}
	
	function product_imgs($model, $before = '', $after = '')
	{
		$fol = "img/p/" . $model;
		if (!is_dir($fol)) return;
		
		echo $before;
		$dh  = opendir($fol);
		while (false !== ($item = readdir($dh)))
		{
			if ($item == "." || $item == "..") continue;
			echo CHtml::image(Yii::app()->baseurl . '/'. $fol . '/' . $item, $item, array('height' => 225, 'title' => $item));
		}
		echo $after;
	}
}
ImgHelper::$newImg = sprintf('<img src="%s" class="pnew" title="new" height="24">', Yii::app()->request->baseUrl . '/img/new.gif');
ImgHelper::$baseFol = Yii::app()->request->baseUrl . '/';
?>
