<?php
/**
 * @author Imran Ali Namazi <imran@cselian.com>
 * @link http://tg.cselian.com/showcase/e-vend
 * @copyright Copyright &copy; 2012-2013 cselian.com
 * @license http://tg.cselian.com/licenses/e-vend
 * Common DRY stuff
 */

class AppHelper
{
	public static function EmptyDB($nonConfig = 0)
	{
		$tables = array('order_items', 'orders', 'addresses', 'users', 'pricing', 'buyers', 'distributors');
		if ($nonConfig) $tables = array('order_items', 'orders', 'addresses', 'users');
		foreach ($tables as $tbl)
			Yii::app()->db->createCommand()->truncateTable($tbl);
		return 'Emptied Database of tables: ' . implode(', ', $tables);
	}

	public static function DatePicker($view, $model, $field)
	{
		Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
		return $view->widget('CJuiDateTimePicker', array(
			'model'=>$model, 
			'attribute'=>$field,
			'language'=>'en',
			'htmlOptions' => array('readonly' => 'readonly')
			),
			1
		);
	}
	
	public static function replaceAdminVals($txt)
	{
		$keys = array('name', 'email', 'phone');
		foreach ($keys as $key)
			$txt = str_replace('{' . $key . '}', Yii::app()->params['admin' . ucfirst($key)], $txt);
		return $txt;
	}
	
	public static function deleteModel($model, $log)
	{
		$id = sprintf('%s #%s', get_class($model), $model->id);
		self::log($log, 'Deleting ' . $id . '...', 1, 0);
		// uncomment to test error logging
		// if (get_class($model) != 'User') $err = 0; else
		$err = $model->delete();
		self::log($log, ' done', 0, 1);
		return $id . ($err !== true ? '(Error - ' . $err . ')' : '');
	}
	
	public static function log($file, $msg, $timestamp = 1, $newline = 1)
	{
		if ($timestamp) $msg = date('d M Y h:m:s') . ' ' . $msg;
		if ($newline) $msg .= "\r\n";
		if (file_exists($file)) $msg = file_get_contents($file) . $msg;
		file_put_contents($file, $msg);
	}
	
	public static function buildTable($cols = null, $css = null)
	{
		include_once 'ci_table.php';
		$table = new CI_Table();
		$table->set_template(array(
			'table_open' => '<table class="grid'.($css != null ? ' ' : '') . $css.'">',
			'heading_row_start' => '<tr class="head">',
			'row_start' => '<tr class="odd">',
			'row_alt_start' => '<tr class="even">'
		));
		
		if ($cols != null) $table->set_heading($cols);
		
		return $table;
	}
	
	// http://www.webtoolkit.info/php-random-password-generator.html
	public static function generatePassword($length=9, $strength=0)
	{
		$vowels = 'aeiou'; $consonants = 'bcdfghjklmnpqrstvwxzy';
		if ($strength & 1) $consonants .= '123456789';
		if ($strength & 2)  $consonants .= '@#$%';
		
		$password = '';
		$alt = time() % 2;
		for ($i = 0; $i < $length; $i++)
		{
			if ($alt == 1) {
				$password .= $consonants[(rand() % strlen($consonants))];
				$alt = 0;
			} else {
				$password .= $vowels[(rand() % strlen($vowels))];
				$alt = 1;
			}
		}
		return $password;
	}
	
	public static function formGetVal($name, $default = false)
	{
		return isset($_GET[$name]) ? $_GET[$name] : $default;
	}
	
	public static function formPostVal($name, $default = false)
	{
		return isset($_POST[$name]) ? $_POST[$name] : $default;
	}
}
