<?php
/**
 * @author Imran Ali Namazi <imran@cselian.com>
 * @link http://tg.cselian.com/showcase/e-vend
 * @copyright Copyright &copy; 2012-2013 cselian.com
 * @license http://tg.cselian.com/licenses/e-vend
 * Applies the Pricing rules to the given product
 */

class AppPricing
{
	private static $virtuals = array();
	
	// using prefix F to not have to deal with an array for the discount/Flat value settings
	private static $byType;
	private static $byBuyer;
	private static $byProductOfBuyer;
	private static $byProductOfType;
	private static $spoofedBuyer;

	public static function getBuyerProducts()
	{
		self::checkLoaded();
		return self::$byProductOfBuyer;
	}

	public static function getComputed($model, $what = 'detailed')
	{
		if (AppLookups::currentUserType() != 3 && self::$spoofedBuyer == null) return $model->price;
		$v = isset($virtuals[$model->id]) ? self::$virtuals[$model->id] : self::computeVirtual($model);
		if ($what == 'array') return $v;
		if ($what == 'final') return $v['final'];
		if ($what == 'calc') return $v['calc'];
		$title = sprintf('Saving: Rs %s @ %s%s on MRP of Rs %s', $v['saving'], $v['disc'], '%', $model->mrp);
		return sprintf('<span title="%s">Rs %s</span>', $title, $v['final']);
	}
	
	public static function spoofBuyer($id, $type)
	{
		self::$spoofedBuyer = array('' =>  $id, '_type' => $type);
	}
	
	private static function buyer($what = '')
	{
		return self::$spoofedBuyer != null ? self::$spoofedBuyer[$what] : UserIdentity::context('buyer' . $what);
	}

	private static function computeVirtual($model)
	{
		self::checkLoaded();
		$r = self::getRule($model);
		$final = $r['flat'] == null ? round($model->price / ((100 + $r['pct']) / 100) ) : $r['flat'];
		$calc2 = $r['flat'] == null ? sprintf('%s / ((100 + %s) / 100)', $model->price, $r['pct']) : $r['flat'];
		
		$disc = round(($model->mrp - $final) / $model->mrp * 100, 2);
		$calc2 .= sprintf(', discount = (%s - %s) / %s * 100', $model->mrp, $final, $model->mrp);
		
		$how = $r['flat'] != null ? sprintf('Flat: %s (%s)', $r['flat'], $disc) : 'Discount: ' . $r['pct']; 
		$calc = sprintf('MRP: %s, DP: %s, %s for %s', $model->mrp, $model->price, $how, $r['type']);
		
		$vat = false; // Yii::app()->params['vatRate']; // TODO: vat from AppEnv
		if ($vat)
		{
			$final += $final * $vat / 100;
			$calc .= ' + Vat: ' . $vat . '%';
		}
		
		$saving = $model->mrp - $final;
		$v = array('final' => $final, 'calc' => $calc, 'calc2' => $calc2, 'disc' => $disc, 'saving' => $saving);
		self::$virtuals[$model->id] = $v;
		//echo $calc; //print_r($v);
		return $v;
	}
	
	private static function getRule($model)
	{
		$buyer = self::buyer();
		$type = self::buyer('_type');
		
		if (isset(self::$byProductOfBuyer[$model->id])) { $ruleType = 'Product/Buyer'; $val = self::$byProductOfBuyer[$model->id]; }
		else if (isset(self::$byProductOfType[$model->id])) { $ruleType = 'Product/Buyer Type'; $val = self::$byProductOfType[$model->id]; }
		else if (isset(self::$byBuyer[$buyer])) { $ruleType = 'Buyer'; $val = self::$byBuyer[$buyer]; }
		else if (isset(self::$byType[$type])) { $ruleType = 'BuyerType'; $val = self::$byType[$type]; }
		else throw new Exception(sprintf("No Pricing Rule for Buyer: %s of BuyerType: %s (%s) for Product: %s", $buyer, AppLookups::BuyerType($type), $type, $model->id));
		
		$flat = substr($val, 0, 1) == 'F';
		return array('type' => $ruleType, 'pct' => $flat ? null : $val, 'flat' => $flat ? substr($val, 1) : null);
	}
	
	private static function checkLoaded()
	{
		if (self::$byType != null) return;

		$buyer = self::buyer();
		$type = self::buyer('_type');
		self::$byType = self::getPricing('pcType', 'buyer_type', 'product_id is null and buyer_id is null and buyer_type is not null');
		self::$byBuyer = self::getPricing('pcBuyer' . $buyer, 'buyer_id', 'product_id is null and buyer_id = ' . $buyer);
		self::$byProductOfBuyer = self::getPricing('pcProductOfBuyer' . $buyer, 'product_id', 'product_id is not null and buyer_id = ' . $buyer);
		self::$byProductOfType = self::getPricing('pcProductOfType'. $type, 'product_id', 'product_id is not null and buyer_id is null and buyer_type is not null');
	}

	private static function getPricing($cacheAs, $column, $where)
	{
		$val = Yii::app()->cache->get($cacheAs);
		if ($val) return $val;
		
		$sql = sprintf('select %s, discount, price from pricing where %s', $column, $where);
		$model = new Pricing();
		$rows = $model->findAllBySql($sql);
		$val = array();
		foreach ($rows as $itm)
			$val[$itm->$column] = $itm->price != null ? 'F' . $itm->price : $itm->discount;
		
		//echo '<br/>' . $cacheAs . ' ' . $sql . ' '; print_r($val);
		Yii::app()->cache->set($cacheAs, $val);
		return $val;
	}
}
