<?php
/**
 * @author Imran Ali Namazi <imran@cselian.com>
 * @link http://tg.cselian.com/showcase/e-vend
 * @copyright Copyright &copy; 2012-2013 cselian.com
 * @license http://tg.cselian.com/licenses/e-vend
 * 
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */

class UserIdentity extends CUserIdentity
{
	public static $superadminEmail = 'imran@cselian.com'; // needed in AppMails to echo instead of sending

	private static $administrators;
	
	public static function isAdministrator($name)
	{
		if (self::$administrators == null) 
			self::$administrators = array_merge(array('imran' => 'aiko12', 'ramesh'=>'aiko12'),
				array('aditya' => 'password', 'mani'=>'password'),
				AppEnv::get('superadmins', 'Super Administrators', array()) );

		return isset(self::$administrators[$name]);
	}
	
	public static function currentUserLink($retlbl = 0)
	{
		$retFmt = '<span id="user-link">%s</span>';
		$type = AppLookups::currentUserType();
		if ($type <= 1 && !$retlbl) return array('visible' => 0);
		if ($type > 1)
		{
			$buy = Yii::app()->controller->user_is('buyer');
			$what = $buy ? 'Buyer' : 'Distributor';
			if (self::context('admin')) $what .= ' Admin';
			$name = self::context($buy ? 'buyerName' : 'distributorName');
			$slug = $buy ? 'buyers' : 'distributors';
			$label = sprintf('%s: %s', $what, $name);
		}
		else
		{
			$label = AppLookups::userType($type);
		}
		
		if ($retlbl) return sprintf($retFmt, $label);
		return array('label'=> $label, 'url'=>array('/'.$slug.'/me'));
	}

	public static function userName()
	{
		return self::context('userEmail');
	}
	
	public static function context($key = 'distributor')
	{
		if (!isset(Yii::app()->session['context']) || !isset(Yii::app()->session['context'][$key])) return false;
		return Yii::app()->session['context'][$key];
	}
	
	public static function setUserType($type)
	{
		Yii::app()->session['userType'] = $type;
	}
	
	public static function setContext($key = 'distributor', $val)
	{
		$ctx = Yii::app()->session['context'];
		$ctx[$key] = $val;
		Yii::app()->session['context'] = $ctx;
	}
	
	public static function clear()
	{
		unset(Yii::app()->session['context']);
		unset(Yii::app()->session['userType']);
	}

	public static function signupOrLogin()
	{
		if (Controller::user_is('autologin'))
			return CHtml::link('enter your name', array('/users/signup'));
		else
			return CHtml::link('signup', array('/users/signup')) . ' or ' . CHtml::link('login', array('/site/login'));
	}
	
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		if(self::isAdministrator($this->username))
		{
			$ok = self::$administrators[$this->username] == $this->password;
			self::clear();
			if ($ok)
			{
				self::setUserType(1);
				Yii::app()->session['context'] = array('user' => 1, 'admin' => 1, 'userEmail' => self::$superadminEmail);
			}
			else $this->errorCode = self::ERROR_PASSWORD_INVALID;
		}
		else
		{
			$usr = new User; $usr = $usr->find('email = "'.$this->username.'"');
			$ok = $usr && $usr->password == md5($this->password);
			if ($ok && !$usr->deactive) {
				self::setUserType($usr->type);
				Yii::app()->session['context'] = array(
					'user' => $usr->id,
					'userEmail' => $usr->email,
					'admin' => $usr->admin,
					'buyer' => $usr->buyer_id,
					'buyer_type' => $usr->buyer->type,
					'distributor' => $usr->distributor_id, 
					'buyerName' => $usr->buyer != null ? $usr->buyer->name : null,
					'distributorName' => $usr->distributor != null ? $usr->distributor->name : null,
				);
			} else {
				if ($ok && $usr->deactive) {
					$ok = false;
					$this->errorCode= 1;
					$lnk = CHtml::link('contact us', array('/site/contact?email=' . $this->username));
					FlashMessages::AddCurrent(sprintf('You account has been deactivated. Please %s to know why.', $lnk), 'error');
				} else {
					$this->errorCode = $usr == null ? self::ERROR_USERNAME_INVALID : self::ERROR_PASSWORD_INVALID;
				}
			}
		}
		
			
		if ($ok) $this->errorCode=self::ERROR_NONE;
		
		return !$this->errorCode;
	}
}
