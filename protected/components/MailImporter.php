<?php
/**
 * @author Imran Ali Namazi <imran@cselian.com>
 * @link http://tg.cselian.com/showcase/e-vend
 * @copyright Copyright &copy; 2012-2013 cselian.com
 * @license http://tg.cselian.com/licenses/e-vend
 * Helper class for Managing Samples and importing data via email.
 * Used to fill admin data on a per record basis.
 */

class MailImporter
{
	public static function getSample($what)
	{
		return Yii::app()->controller->site_file('protected/data/' . strtolower($what) . '.txt', 1);
	}
	
	public static function getHints($what)
	{
		$hints = Yii::app()->controller->site_file('protected/data/' . strtolower($what) . '-hints.txt', 1);
		if (!$hints) return false;
		$res = '<b style="font-size: 120%">' . ucfirst($what) . '</b><br/>';
		$hints = self::getData($hints);
		foreach ($hints as $n=>$h)
			$res .= sprintf('<b>%s</b> - %s<br/>
', $n, $h);
		return $res;
	}
	
	//
	public static function getModel($what, $val)
	{
		$data = self::getData($val);
		$model = new $what;
		$model->attributes = $data;
		
		if ($what == 'Distributor' || $what == 'Buyer') $model->date_added = Formatter::dateForSql();
		
		return $model;
	}
	
	private static function getData($val)
	{
		$lines = explode('
', $val);
		$started = 0;
		$cur = null;
		$data = array();
		foreach ($lines as $line)
		{
			$line = trim($line);
			if (self::startsWith($line, '=='))
			{
				if ($started) { $data[$cur[0]] = $cur[1]; break; }
				$started = 1;
			}
			else if (self::contains($line, ':'))
			{
				if ($cur != null) $data[$cur[0]] = $cur[1];
				$bits = explode(':', $line);
				$cur = array($bits[0], trim($bits[1]));
			}
			else
			{
				$cur[1] .= '
' . $line; 
			}
		}
		return $data;
	}
	
	private static function startsWith($haystack, $needle)
	{
		return !strncmp($haystack, $needle, strlen($needle));
	}

	private static function contains($haystack, $needle)
	{
		return gettype(strpos($haystack, $needle)) == "integer";
	}
}
?>
