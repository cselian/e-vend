<?php
/**
 * @author Imran Ali Namazi <imran@cselian.com>
 * @link http://tg.cselian.com/showcase/e-vend
 * @copyright Copyright &copy; 2012-2013 cselian.com
 * @license http://tg.cselian.com/licenses/e-vend
 * Manages the domains and databases for a Multisite Install
 */

class AppMultisite
{
  private static $model = 'Distributor';
  private static $rootHost = 'localhost';
  private static $dbName = 'yiims';
  private static $hosts = null;

  public static function qualifyWithDB($table)
  {
    return $table; // removing multisite idea for now. all in one db. cant have fkeys from buyer/address to distrib otherwise
    self::checkLoaded();
    $h = $_SERVER['HTTP_HOST'];
    // TODO: Proper Code, use rootHost properly to build a login link
    if (!isset(self::$hosts[$h])) throw new CHttpException(201, 'Invalid Distributor, please login at ' . self::$rootHost, E_USER_NOTICE);

    $id = self::$hosts[$h];
    return sprintf('%s_%s.%s', self::$dbName, $id, $table);
  }
  
  public static function clear()
  {
    Yii::app()->cache->delete('msDBName');
    Yii::app()->cache->delete('msHosts');
  }
  
  private static function checkLoaded()
  {
    if (self::$hosts != null) return;
    
    self::$dbName = self::getDBName();
    self::$hosts = self::getHosts();
  }

  private static function getHosts()
  {
    $val = Yii::app()->cache->get('msHosts');
    if ($val) return $val;

    $model = new self::$model;
    $sql = 'select id, host from ' . $model->tableName();
    $items = $model->findAllBySql($sql);
    $val = array();
    foreach ($items as $v)
      $val[$v->host] = $v->id;
    
    Yii::app()->cache->set('msHosts', $val);
    return $val;
  }

  private static function getDBName()
  {
    $val = Yii::app()->cache->get('msDBName');
    if ($val) return $val;
    
    $val = Yii::app()->db->connectionString;
    $val = explode(';', $val);
    foreach ($val as $v) {
      if (strstr($v, 'dbname') != '')
      {
        $val = strstr($v, 'dbname');
        $val = explode('=', $val);
        $val = $val[1];
        break;
      }
    }
    if (is_array($val)) throw new Exception('Expected dbname in Connectionstring' . Yii::app()->db->connectionString);
    Yii::app()->cache->set('msDBName', $val);
    return $val;
  }
}
