<tr<?php Formatter::altRow($index); ?>>
<?php if (!isset($data)) {
$data = new Help; ?>
	<th><?php echo CHtml::encode($data->getAttributeLabel('topic')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('description')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('keywords')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('slug')); ?></th><?php if ($isDev) { ?>
	<th><?php echo CHtml::encode($data->getAttributeLabel('accessible_to')); ?></th></tr>
<tr<?php Formatter::altRow($index); ?>>
	<th colspan="5"><?php echo CHtml::encode($data->getAttributeLabel('content')); ?></th>
<?php } } else {
$data->adjustAll(); ?>
	<td<?php echo $isDev ? ' class="edit"' : ''; ?>><?php echo Help::link($data); ?></td>
	<td><?php echo CHtml::encode($data->description); ?></td>
	<td><?php echo $data->keywords; ?></td>
	<td><?php echo $data->slug; ?></td><?php if ($isDev) { ?>
	<td><?php echo CHtml::encode($data->accessible_to); ?></td></tr>
<tr<?php Formatter::altRow($index); ?>>
	<td colspan="5"><?php echo $data->content; ?></td>
<?php } } ?></tr>
