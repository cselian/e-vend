<?php
$this->breadcrumbs=array(
	'Help Topic'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu= MenuHelper::Menu($model);
?>

<h1>Update Help Topic <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
