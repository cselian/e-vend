<?php
$this->breadcrumbs=array(
	'Help Topic',
);

$this->menu= MenuHelper::Menu();
?>

<h1>Help Topics</h1>

<table class="grid">
<?php
$vd = array('isDev' => Yii::app()->controller->user_is('nowdev'));
$this->renderPartial('_view', array_merge($vd, array('index' => false)));
$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
	'viewData'=> $vd
)); ?>
</table>
