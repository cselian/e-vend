<?php
$this->breadcrumbs=array(
	'Help Topic'=>array('index'),
	$model->id,
);

$this->menu= MenuHelper::Menu($model);
?>

<h1><?php echo sprintf('Topic: %s (#%s)', $model->topic, $model->id); ?></h1>

<?php
$atts = array(
		array('label'=>$model->getAttributeLabel('keywords'), 'type'=>'raw', 'value'=>$model->keywords),
		'description',
		array('label'=>$model->getAttributeLabel('content'), 'type'=>'raw', 'value'=>$model->content, 'cssClass' => 'helpcontent'),
);
if (Yii::app()->controller->user_is('nowdev'))
	$atts = array_merge(array(
		'id',
		array('label'=>$model->getAttributeLabel('slug'), 'type'=>'raw', 'value'=>$model->slug),
		'accessible_to',
		), $atts);

$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=> $atts
)); ?>
