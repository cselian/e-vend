<?php
$this->breadcrumbs=array(
	'Help Topic'=>array('index'),
	'Create',
);

$this->menu= MenuHelper::Menu();
?>

<h1>Create Help Topic</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
