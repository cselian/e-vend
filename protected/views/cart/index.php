<?php
Yii::app()->controller->add_script('submit-right', 0);
$this->breadcrumbs=array(
	'Cart',
);

global $sidebarTop;
$sidebarTop = $this->renderPartial('/addresses/_cart', $addressSidebarInfo, 1);
?>
<?php if (count($cart) > 0) { ?>
<h3>Cart Items</h3>
<form method="POST" action="<?php echo Yii::app()->request->baseUrl . '/cart/'; ?>">
<input type="hidden" name="edit" />
<?php

function cell_amount($data) { return array('class' => 'amount', 'data' => $data); }

$table = AppHelper::buildTable(array('Model', 'Colour', 'Price', 'Quantity', 'Amount', 'Delete'));
$amt = 0; $qty = 0;
foreach ($cart as $i)
{
  $a = $i['final'] * $i['qty'];
  $q = sprintf('<input type="hidden" name="pid[]" value="%s" /><input type="text" name="qty[]" value="%s" %s/>', $i['id'] . '-' . $i['varn'], $i['qty'], $radios ? 'readonly="readonly" ' : '');
  $d = sprintf('<input type="checkbox" name="del[]" value="%s" />', $i['id'] . '-' . $i['varn']);
  $table->add_row(array($i['model'], Product::variation_r($i['varn']), cell_amount($i['price']), $q, cell_amount($a), $d));
  $amt += $a;
  $qty += $i['qty'];
}

$table->set_template(array(
  'table_open' => '<table border="1" class="qty grid">',
  'row_alt_start' => '<tr class="alt">',
  'table_close' => sprintf('<tr class="tot head"><td colspan="%s" class="cap">Tot:</td><td>%s</td><td>%s</td><td></td></tr></table>', 3, $qty, $amt)
));

//if ($isCustomer)   echo 'Order for: ' . '<input type="text" name="orderFor" value="' . $orderFor . '" /> <br />';

echo $table->generate();
?>
<div class="submitrgt">
  <input type="submit" value="Update Cart" />
</div>
</form>
<?php if ($isGuest) {
echo 'Please ' . UserIdentity::signupOrLogin() . 'before you can place the order.';
} else {
if (!$canBill) echo '<div class="flash-error flash-right">' . $cantBillReason . '</div>';

echo sprintf('<form method="POST" action="%s/orders/add/">', Yii::app()->request->baseUrl);
$tbl = AppHelper::buildTable(null, 'tblsmall');
$tbl->add_row('Delivery Address', CHtml::dropDownList('address', null, $addresses)); // TODO: Consider , array('size' => count($addressList) - 1)

$tbl->add_row('Billing Address ' . $billExcl, CHtml::dropDownList('billingAddress', null, $userAddresses));
$tbl->add_row('Payment Type', CHtml::dropDownList('payment_type', null, $paymentTypes));
$confirm = $canBill ? '<input type="submit" value="Confirm Order" title="This will not include unsaved changes to the cart" />' : 'Cannot place order yet!';
$tbl->add_row('', $confirm);
echo $tbl->generate();
?>
<div class="submitrgt">
  
</div>
</form>
<?php } ?>

<?php } else {
  echo '<div class="flash-notice">No Items in cart</div>';
  FlashMessages::displayActions(null, array('/products/' => 'Back to Phones'), null);
} ?>
