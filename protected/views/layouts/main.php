<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/extra.css" />
	<link rel="stylesheet" href="http://code.jquery.com/mobile/1.1.1/jquery.mobile-1.1.1.min.css" />
	<!--script src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
	<script src="http://code.jquery.com/mobile/1.1.1/jquery.mobile-1.1.1.min.js"></script-->
	<?php if (MapHelper::hasMap()) { ?>
	<script src="http://maps.google.com/maps/api/js?sensor=false"type="text/javascript"></script><?php } ?>
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container <?php echo AppEnv::get('siteBodyCss', 'Site Logo Css Class', ''); ?>" id="page">

<?php
// returns true if the user >= $what
function user_is($what)
{
	return Yii::app()->controller->user_is($what);
}
?>
	<div id="mainMbMenu" class="nav popout">
		<div id="logo"><?php echo UserIdentity::currentUserLink(1); ImgHelper::buyer_logo(); ImgHelper::site_logo(); ?></div>
		<?php
		$cart = user_is('buyer') ? AppLookups::Cart('count') : 0;
		$this->widget('application.extensions.mbmenu.MbMenu',array(
			//'htmlOptions' => array('class' => 'nav'),
			'firstItemCssClass' => 'first',
			'items'=>array(
				array('label'=>'Home', 'url'=>array('/site'), 'items'=> array(
					array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
					array('label'=>'Contact', 'url'=>array('/site/contact')),
					array('label'=>'Terms', 'url'=>array('/site/page', 'view'=>'terms')),
					array('label'=>'Policies', 'url'=>array('/site/page', 'view'=>'policies')),
					)),
				array('label'=>'Administer', 'visible'=>user_is('superadmin'), 'items'=> array(
					array('label'=>'Distributors', 'url'=>array('/distributors/')),
					array('label'=>'Products', 'url'=>array('/products/admin') ,'items'=> array(
						array('label'=>'Create', 'url'=>array('/products/create')))),
					array('label'=>'Pricing', 'url'=>array('/pricing/') ,'items'=> array(
						array('label'=>'Create', 'url'=>array('/pricing/create')))),
					array('label'=>'Buyers', 'url'=>array('/buyers/')),
					array('label'=>'Users', 'url'=>array('/users/admin')),
					array('label'=>'Mail Templates', 'url'=>array('/mails')),
					array('label'=>'Generator', 'url'=>array('/gii'), 'visible' => user_is('dev') && AppEnv::get('gii', 'Enable Code Generator', 0), 'linkOptions' => array('target' => 'new')),
					array('label'=>'Site', 'url'=>array('/site/admin')),
					)),
				array('label'=>'View', 'visible'=>user_is('superadmin'), 'items'=> array(
					array('label'=>'Orders', 'url'=>array('/orders/')),
					array('label'=>'Addresses', 'url'=>array('/addresses/')),
					)),
				array('label'=>'View', 'visible'=>user_is('distributor'), 'items'=> array(
					array('label'=>'Orders', 'url'=>array('/orders/')),
					array('label'=>'Buyers', 'url'=>array('/buyers/')),
					array('label'=>'Products', 'url'=>array('/products/')),
					array('label'=>'Stock', 'url'=>array('/stock/')),
					// array('label'=>'Salesmen', 'url'=>array('/orders/')),
					)),
				array('label'=>'Products', 'url'=>array('/products/'), 'visible'=>user_is('buyer')),
				array('label'=>'Cart (' . $cart . ')', 'url'=>array('/cart'), 'visible'=>$cart, 'items'=> array(
					array('label'=>'Empty Cart', 'url'=>array('/cart/clear')),
					)),
				array('label'=>'Orders', 'url'=>array('/orders/'), 'visible'=>user_is('buyer') && user_is('loggedin')),
				array('label'=>'View', 'visible'=>user_is('salesman'), 'items'=> array(
					array('label'=>'Buyers', 'url'=>array('/buyers/')),
					array('label'=>'Orders', 'url'=>array('/orders/')),
					)),
				AppReports::getMenu(),
				array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>user_is('notloggedin') && !user_is('autologin')),
				array('label'=>'Signup', 'url'=>array('/users/signup'), 'visible'=>user_is('notloggedin') && !user_is('autologin')),
				array('label'=> 'User' , 'visible'=>user_is('loggedin'), 'items'=> array(
					array('label' => Yii::app()->user->name, 'url'=>array('/users/me'), 'linkOptions' => array('title' => UserIdentity::context('userEmail'))),
					UserIdentity::currentUserLink(),
					user_is('buyer') ? array('label'=>'Addresses', 'url'=>array('/addresses/')) : array('visible' => 0),
					array('label'=>'Logout', 'url'=>array('/site/logout/')),
					)),
				array('label'=>'Help', 'url'=>array('/help'), 'items' => MenuHelper::HelpMenu()),
			),
		)); ?>
	<?php if (user_is('admin')) { echo '<div class="nav-item">'; AppLookups::selectUserTypes(); echo '</div>'; } ?>
	</div><!-- mainmenu -->
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php FlashMessages::Display(); ?>
	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
		Copyright <?php echo sprintf(Yii::app()->params['copyright'], date('Y')); ?>
		<p class="credits">Developed by <a href="http://tg.cselian.com">cselian</a> with <a href="http://www.yiiframework.com/" rel="external">Yii</a>.</p>
		<p class="links"><?php echo CHtml::link('Terms', array('/site/page', 'view'=>'terms'))
		 . ' ' . CHtml::link('Policies', array('/site/page', 'view'=>'policies')); ?></p>
		<?php echo Yii::app()->params['address']; ?><br />
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
