<?php $this->beginContent('//layouts/main'); ?>
<div id="top-operations">
<?php
$this->widget('zii.widgets.CMenu', array(
		'items'=>$this->menu,
		'htmlOptions'=>array('class'=>'operations'),
	));

?>
</div>
<div id="content" style="padding-top: 10px; clear: both;">
	<?php echo $content; ?>
</div><!-- content -->
<?php $this->endContent(); ?>
