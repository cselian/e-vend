<?php
$this->breadcrumbs=array(
	'Buyers',
);

$this->menu= MenuHelper::Menu();
?>

<h1>Buyers</h1>

<table class="grid" style="margin-top: 20px;">
<?php
$this->renderPartial('_view', array('index' => false));
$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
</table>
