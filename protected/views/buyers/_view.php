<tr<?php Formatter::altRow($index); ?>>
<?php
global $superAdmin;
if (!isset($data)) {
$superAdmin = Controller::user_is('superadmin');
$data = new Buyer; ?>
	<th><?php echo CHtml::encode($data->getAttributeLabel('id')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('name')); ?> /
	<?php echo CHtml::encode($data->getAttributeLabel('contact')); ?></th>
	<th>Logo</th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('type')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('date_added')); ?></th><?php if ($superAdmin) { ?>
	<th><?php echo CHtml::encode($data->getAttributeLabel('outstanding')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('signup_from')); ?></th>
	<th rowspan="2">Admin</th><?php } ?>
</tr>
<tr<?php Formatter::altRow($index); ?>>
	<th></th>
	<th colspan="3"><?php echo CHtml::encode($data->getAttributeLabel('address')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('phone')); ?> /
	<?php echo CHtml::encode($data->getAttributeLabel('mobile')); ?></th><?php if ($superAdmin) { ?>
	<th>Priced Only / Appvl <br /> Reqd / Sponsored / Purge</th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('payment_types')); ?></th>
<?php } } else { ?>
	<td><?php echo CHtml::link(CHtml::encode($data->id), array('/buyers/view', 'id'=>$data->id)); ?></td>
	<td><?php echo sprintf('%s (%s)', Formatter::Capitalize($data->name), Formatter::Capitalize($data->contact)); ?></td>
	<td><?php ImgHelper::buyer_logo($data); ?></td>
	<td><?php echo AppLookups::BuyerType($data->type); ?></td>
	<td><?php echo CHtml::encode($data->date_added); ?></td><?php if ($superAdmin) { ?>
	<td><?php echo CHtml::encode($data->outstanding); ?></td>
	<td><?php echo CHtml::encode($data->signup_from); ?></td>
	<td rowspan="2"><?php echo $data->siteEntryLink() . ' ' . User::createFor($data, 0, ''); ?></td><?php } ?>
</tr>
<tr<?php Formatter::altRow($index); ?>>
	<td><?php if ($superAdmin || AppReports::isInReport()) echo ' ' . CHtml::link('&hellip;', array('/buyers/update', 'id'=>$data->id));?></td>
	<td colspan="3"><?php echo Formatter::concat(', ', $data->address, $data->pin)
		. ' ' . MapHelper::link($data, 'map'); ?></td>
	<td><?php echo Formatter::concat(' / ', $data->phone, $data->mobile); ?></td>
	<?php if ($superAdmin) { ?>
	<td><?php echo Formatter::bool($data->priced_only) . ' / ' 
		. $data->formatApproval() . ' / ' . Formatter::bool($data->sponsored) . ' / ' . Formatter::bool($data->purge); ?></td>
	<td><?php $data->adjustPaymentTypes(); echo $data->payment_types; ?></td><?php } ?>
<?php } ?>
</tr>
