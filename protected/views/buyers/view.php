<?php
$this->breadcrumbs=array(
	'Buyers'=>array('index'),
	$model->name,
);

$this->menu= MenuHelper::Menu($model);
?>

<h1>View Buyer #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		array('label'=>$model->getAttributeLabel('type'), 'type'=>'raw', 'value'=>AppLookups::BuyerType($model->type)),
		'contact',
		'notify',
		array('label'=>$model->getAttributeLabel('address'), 'type'=>'raw', 'value'=>Address::formatEx($model)
			. Address::linkToDistances($model)),
		'phone',
		'mobile',
		array('label'=>$model->getAttributeLabel('date_added'), 'type'=>'raw', 'value'=>Formatter::date($model->date_added)),
		array('label'=>MapHelper::$linkLabel, 'type'=>'raw', 'value'=>MapHelper::link($model)),
		array('label'=>$model->getAttributeLabel('assigned_to_id'), 'type'=>'raw', 'value'=>Formatter::displayObject($model->assignedTo)),
		array('label'=>$model->getAttributeLabel('outstanding'), 'type'=>'raw', 'value'=>Formatter::amount($model->outstanding)),
		array('label'=>$model->getAttributeLabel('tax_info'), 'type'=>'raw', 'value'=>Formatter::multiline($model->tax_info)),
		array('label'=>$model->getAttributeLabel('priced_only'), 'type'=>'raw', 'value'=>Formatter::bool($model->priced_only)),
		array('label'=>$model->getAttributeLabel('approval_required'), 'type'=>'raw', 'value'=>$model->formatApproval()),
		'payment_types',
		'signup_from',
		array('label'=>$model->getAttributeLabel('sponsored'), 'type'=>'raw', 'value'=>Formatter::bool($model->sponsored)),
		array('label'=>$model->getAttributeLabel('purge'), 'type'=>'raw', 'value'=>Formatter::bool($model->purge)),
	),
)); ?>

<br /><br />
<h2>Addresses</h2>
<?php if(count($model->addresses)> 0) { ?>
<table class="grid">
<?php
$this->renderPartial('/addresses/_view', array('index' => false));
$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>new CArrayDataProvider($model->addresses),
	'itemView'=>'/addresses/_view',
	'template'=>'{items}',
)); ?>
</table>
<?php }
if (!Yii::app()->controller->user_is('distributor'))
{
	$params = '';
	if (Yii::app()->controller->user_is('superadmin')) $params = sprintf('?for=%s&name=%s', $model->id, $model->name);
	echo CHtml::link('Add Address', array('/addresses/create/' . $params));
}
?>
