<?php
$this->breadcrumbs=array(
	'Buyers',
);

$this->menu= MenuHelper::Menu();

echo isset($buyer)
	? sprintf('<h1>Buyer #%s - %s, with Addresses and Distributors</h1>', $buyer->id, $buyer->name)
	: '<h1>Buyers on the Map</h1>';
?>

<div id="map" style="height: 450px; border: double 2px #ccc">Loading Map...</div>
<?php
MapHelper::addHome();
$pts = $dataProvider->getData();

foreach ($pts as $itm)
{
	$slug = $itm->tableName();
	$name = ucfirst(Formatter::Singular($slug));
	$fmt = '<a href="' . Yii::app()->request->baseUrl . '/' . $slug . '/%s">%s</a>';

	if ($slug == 'distributors' && !Yii::app()->controller->user_is('superadmin'))
		$fmt = 'Distributor #%s ' . $itm->name;
	
	if ($slug == 'distributors')
		MapHelper::addLocation($itm, $fmt, $name, 'green');
	else
		MapHelper::addLocation($itm, $fmt, $name);
}
MapHelper::printMap();
?>
