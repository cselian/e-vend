<?php
$this->breadcrumbs=array(
	'Buyers'=>array('index'),
	'Create',
);

$this->menu= MenuHelper::Menu();
?>

<h1>Create Buyer</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>