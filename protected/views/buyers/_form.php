<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'buyer-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'type'); ?>
		<?php echo $form->dropDownList($model,'type', Formatter::includeBlank(AppLookups::$buyerTypes)); ?>
		<?php echo $form->error($model,'type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'contact'); ?>
		<?php echo $form->textField($model,'contact',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'contact'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'notify'); ?>
		<?php echo $form->textField($model,'notify',array('size'=>60,'maxlength'=>512)); ?>
		<?php echo $form->error($model,'notify'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'address'); ?>
		<?php echo $form->textArea($model,'address',array('rows'=>3, 'cols'=>60, 'maxlength'=>255)); ?>
		<?php echo $form->error($model,'address'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pin'); ?>
		<?php echo $form->textField($model,'pin',array('size'=>6,'maxlength'=>6)); ?>
		<?php echo $form->error($model,'pin'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'phone'); ?>
		<?php echo $form->textField($model,'phone',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'phone'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'mobile'); ?>
		<?php echo $form->textField($model,'mobile',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'mobile'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lat'); ?>
		<?php echo $form->textField($model,'lat',array('size'=>9,'maxlength'=>9)); ?>
		<?php echo $form->error($model,'lat'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lon'); ?>
		<?php echo $form->textField($model,'lon',array('size'=>9,'maxlength'=>9)); ?>
		<?php echo $form->error($model,'lon'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'assigned_to_id'); ?>
		<?php echo $form->dropDownList($model,'assigned_to_id', Formatter::includeBlank(AppLookups::Distributors())); ?>
		<?php echo $form->error($model,'assigned_to_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'outstanding'); ?>
		<?php echo $form->textField($model,'outstanding',array('size'=>9,'maxlength'=>9)); ?>
		<?php echo $form->error($model,'outstanding'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tax_info'); ?>
		<?php echo $form->textArea($model,'tax_info',array('rows'=>3, 'cols'=>60, 'maxlength'=>512)); ?>
		<?php echo $form->error($model,'tax_info'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'priced_only'); ?>
		<?php echo $form->checkBox($model,'priced_only'); ?>
		<?php echo $form->error($model,'priced_only'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'approval_required'); ?>
		<?php echo $form->radioButtonList($model,'approval_required', AppLookups::$approvalTypes,
			array('separator' => '', 'template' => '<div class="radio-row">{input} {label}</div>')); ?>
		<?php echo $form->error($model,'approval_required'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'payment_types'); ?>
		<?php echo $form->checkBoxList($model,'payment_types', AppLookups::$paymentTypes,
			array('separator' => '', 'template' => '<div class="radio-row">{input} {label}</div>')); ?>
		<?php echo $form->error($model,'payment_types'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'signup_from'); ?>
		<?php echo $form->textArea($model,'signup_from',array('rows'=>3, 'cols'=>60,'maxlength'=>1024)); ?>
		<?php echo $form->error($model,'signup_from'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sponsored'); ?>
		<?php echo $form->checkBox($model,'sponsored'); ?>
		<?php echo $form->error($model,'sponsored'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'purge'); ?>
		<?php echo $form->checkBox($model,'purge'); ?>
		<?php echo $form->error($model,'purge'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->