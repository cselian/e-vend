<?php
$this->pageTitle=Yii::app()->name . ' - Password Reset';
$this->breadcrumbs=array(
	'Reset Password',
);
?>

<?php if ($resetstep == 'enter') { ?>
<h1>Confirm New Password</h1>
<p>Please enter your new password to proceed.</p>
<?php } else { ?>
<h1>Reset Password</h1>
<p>Please enter your email and we will email you a new password.</p>
<?php } ?>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

<?php if ($resetstep == 'enter') { ?>
	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password'); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>
<?php } else { ?>
	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email'); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>
<?php } ?>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Reset Password'); ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
