<?php $this->pageTitle=Yii::app()->name; ?>

<h1>Welcome to <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1>

<?php
if (Yii::app()->user->isGuest) echo '<p>Head over to the login page to begin. ' . AppEnv::get('demoLoginMsg', 'Demo Login Message', '') . '</p>'; 
?>
Enabling Mobile Manufacturers like Nokia to
<ul style="margin: 5px 0px 0px 15px;">
	<li>Manage Buyer (ETrader / Corporate) accounts</li>
	<li>Tailor Pricing based on the buyer</li>
	<li>Route orders to the nearest Distributor</li>
</ul>
<br><br>
And then lets the buyer
<ul style="margin: 5px 0px 0px 15px;">
	<li>See products with his special price</li>
	<li>Place orders</li>
</ul>
<br><br>

<?php /* 
<br><br>
<h2>Roadmap (Phase II)</h2>
The complete solution for Mobile Distributors to manage their Catalogue and Sales.
<div>You can sign up as a Distributor and
	<ul style="margin: 5px 0px 0px 15px;">
		<li>Manage your Dealers and Representatives (Salesmen)</li>
		<li>Maintain your Catalogue of Products</li>
		<li>Maintain Closing Stock for Dealers</li>
		<li>Let them place orders / place orders in the system on behalf on them</li>
		<li>See where dealers are located on a map and plan your delivery</li>
		<li>Export / Import data so you can use with other systems</li>
	</ul>
</div>
*/ ?>
