<?php
$this->pageTitle=Yii::app()->name . ' - Contact Us';
$this->breadcrumbs=array(
	'Contact',
);

Yii::app()->clientScript->registerScript('expand', "
$('div.form').hide();
$('#expand-form').click(function(){
	$('div.form').slideToggle(500);
});
");

$ourMail = Yii::app()->params['adminEmail'];
$ourMail = sprintf('<a href="mailto:%s">%s</a>', $ourMail, $ourMail);
?>

<h1>Contact Us</h1>

<?php $siteHtml = AppEnv::get('contact', 'Contact Page Html', false);
if ($siteHtml) {
	echo $siteHtml;
	//return; //TODO: Consider return
} else { ?>
Person: <?php echo Yii::app()->params['adminName']; ?><br />
Address : <?php echo Yii::app()->params['address']; ?><br />
Phone: <?php echo Yii::app()->params['adminPhone']; ?><br />
Email: <?php echo $ourMail ?>
<?php } ?>
<br /><br />

<?php if(Yii::app()->user->hasFlash('contact')) { ?>

<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('contact'); ?>
</div>

<?php } else { ?>

<p>
If you have business inquiries or other questions, please <a id="expand-form" href="#">fill out the following form</a> to contact us. Thank you.
</p>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'contact-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name'); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email'); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'subject'); ?>
		<?php echo $form->textField($model,'subject',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'subject'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'body'); ?>
		<?php echo $form->textArea($model,'body',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'body'); ?>
	</div>

	<?php if(CCaptcha::checkRequirements()): ?>
	<div class="row">
		<?php echo $form->labelEx($model,'verifyCode'); ?>
		<div>
		<?php $this->widget('CCaptcha'); ?>
		<?php echo $form->textField($model,'verifyCode'); ?>
		</div>
		<div class="hint">Please enter the letters as they are shown in the image above.
		<br/>Letters are not case-sensitive.</div>
		<?php echo $form->error($model,'verifyCode'); ?>
	</div>
	<?php endif; ?>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Submit'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<?php } ?>
