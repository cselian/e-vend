<?php
if (!Controller::user_is('buyer')) return;

$byr = strtolower(str_replace(' ', '-', UserIdentity::context('buyerName')));
$swf = '/img/banners/login-' . $byr . '.swf';
if (!file_exists(Controller::site_file($swf))) return;

//print_r(getimagesize(Controller::site_file($swf)));
$swf = Yii::app()->baseUrl . $swf;

$ht = '240'; $wd = '320';
$type = 'object'; // html5 / object
?>

<?php if ($page == 'login') { ?>
<div class="flash-right flash-banner" style="width: <?php echo $wd; ?>px; height: <?php echo $ht; ?>px;">
<?php if ($type == 'html5') {
// TODO: Show img properly / check if exists
$img = Yii::app()->baseUrl . '/img/banners/login-' . $byr . '.jpg';
?>
<video width="<?php echo $wd; ?>" height="<?php echo $ht; ?>" controls preload="none">
  <object width="<?php echo $wd; ?>" height="<?php echo $ht; ?>" type="application/x-shockwave-flash" data="<?php echo $swf; ?>">
  <param name="movie" value="<?php echo $swf; ?>" />
  <param name="flashvars" value="image=<?php echo $img; ?>&amp;file=<?php echo $swf; ?>" />
  <img src="<?php echo $img; ?>" width="<?php echo $wd; ?>" height="<?php echo $ht; ?>" alt="Nokia, Connecting People"
    title="No video playback capabilities, please update / configure your browser." />
  </object>
</video>
<?php } elseif ($type == 'object') { ?>
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0" width="<?php echo $wd; ?>" height="<?php echo $ht; ?>">
 <param name="movie" value="<?php echo $swf; ?>">
 <param name="quality" value="high">
 <param name="bgcolor" value="#fff">
 <param name="wmode" value="transparent">
 <embed src="<?php echo $swf; ?>" wmode="transparent" quality="high" bgcolor="#fff"  width="<?php echo $wd; ?>" height="<?php echo $ht; ?>" name="menu" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer">
</object>
<?php } ?>
</div>
<?php } ?>
