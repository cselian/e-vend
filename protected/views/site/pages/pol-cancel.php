<p>
  You can cancel your order by sending a mail to <?php echo $ourMail; ?> with order details, before the product has
  been shipped. Your entire order amount will be refunded.
</p>
<p>
  In case your product has been shipped but has not yet been delivered, to cancel it please Contact Us. Unfortunately, an
  order cannot be cancelled in case a delivery has already been made.
</p>
