<?php
$this->pageTitle=Yii::app()->name . ' - About';
$this->breadcrumbs=array(
	'About',
);
?>
<h1>About</h1>

<p>
  Intermart.in is a part of Aikon cellular which is a private limited company distributing quality mobile phones
  and accessories. Established on Sankey Road the company services hundreds of users across Bangalore.
</p>
<p>
  This website allows the user(buyer) to place an order online. Any registered user who has a user name and password
  can log in, use the shopping cart and place an order. After logging in, the user can activate the PHONE button
  that will take him to the product page. The list provided allows him to view the list and see the model by
  passing the mouse over the name. A small box alongside lets him order the number of pieces required. Once the
  list is complete he can view and edit his shopping cart and place the order. The user can also view past orders
  and outstanding amounts.
</p>
