<?php
$this->pageTitle=Yii::app()->name . ' - Policies';
$this->breadcrumbs=array(
	'Policies',
);

$ourMail = Yii::app()->params['adminEmail'];
$ourMail = sprintf('<a href="mailto:%s">%s</a>', $ourMail, $ourMail);
$ourName = 'intermart.in';
?>

<h1>Privacy Policy</h2>
<?php include "pol-privacy.php"; ?>

<h1>Booking Policy</h2>
<p>
	// TODO: Booking Policy
</p>

<h1>Cancellation Policy</h2>
<?php include "pol-cancel.php"; ?>

<h1>Refund Policy</h2>
<?php include "pol-refund.php"; ?>
