<form method="get">
<?php
echo CHtml::dropDownList('action', $action, $actions, array('size' => count($actions) + 1, 'style'=>'width: 300px;')) . '<br />';
echo CHtml::submitButton('Perform Action', array('name'=> ''));
?>
<br /><br />
</form>

<?php if (isset($textArea)) { ?>
<hr />
<form method="post">
<?php

if ($textArea['hints']) echo '<div class="flash-notice" style="float: right; width: 400px">' . $textArea['hints'] . '</div>';

echo CHtml::hiddenField('action', $action);
if (isset($error)) echo $error; 
echo $textArea['label'] . '<br/>';
echo CHtml::textArea($textArea['name'], $textArea['val'], $textArea['atts']) . '<br/>';
echo CHtml::submitButton($textArea['submit']);
?>
</form>
<?php } else if (isset($config)) {
	echo '<hr />' . CHtml::tag('h2', array(), 'These are the configurations for the site');
	$tbl = AppHelper::buildTable(array('What', 'Value', 'Notes'));
	$tbl->template['table_open'] = '<table class="grid" style="width: auto;">';
	foreach ($config as $label=>$value)
	{
		$v2 = is_array($value) ? $value[1] : null;
		$v = $v2 == null ? $value : $value[0];
		$tbl->add_row(array($label, $v, $v2));
	}
	echo $tbl->generate();
} else if (isset($listData)) {
	echo $listPreData;
	echo '<table class="grid">';
	$this->renderPartial($listData['itemView']);
	$this->widget('zii.widgets.CListView', $listData);
	echo '</table>';
	echo $listPostData;
}
?>
