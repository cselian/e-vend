<?php
$atts = array(
	'id',
	array('label'=>$model->getAttributeLabel('product'), 'type'=>'raw', 'value'=>$model->product->imageAndLink()),
	'quantity',
	array('label'=>$model->getAttributeLabel('variation'), 'type'=>'raw', 'value'=>Product::variation_r($model->variation)),
	array('label'=>$model->getAttributeLabel('price'), 'type'=>'raw', 'value'=>Formatter::amount($model->price)),
	'calculation',
	'stock_nos',
	//array('label'=>$model->getAttributeLabel(''), 'type'=>'raw', 'value'=>$model->),
	);
if ($attsOnly) return $atts;
else throw new exception('not supported view for orderItems. Only meant to return attributes for use in purge');
?>
