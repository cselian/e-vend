<tr<?php Formatter::altRow($index); ?>>
<?php if (!isset($data)) {
$data = new OrderItem; ?>
	<th><?php echo CHtml::encode($data->getAttributeLabel('id')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('product')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('quantity')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('variation')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('price')); ?></th><?php if (AppLookups::canUserSee('priceCalculation')) { ?>
	<th><?php echo CHtml::encode($data->getAttributeLabel('calculation')); ?></th><?php } ?>
	<th><?php echo CHtml::encode($data->getAttributeLabel('stock_nos')); ?></th>
<?php } else { ?>
	<td><?php echo $data->id; ?></td>
	<td><?php echo $data->product->imageAndLink(); ?></td>
	<td><?php echo $data->quantity; ?></td>
	<td><?php echo CHtml::encode($data->variation); ?></td>
	<td><?php echo Formatter::amount($data->price); ?></td><?php if (AppLookups::canUserSee('priceCalculation')) { ?>
	<td><?php echo $data->calculation; ?></td><?php } ?>
	<td><?php echo $form->hiddenField($data, 'id',array('name' => 'itmid[]'));
		echo $form->textArea($data, 'stock_nos',array('name' => 'stock_nos[]', 'cols'=>30, 'rows' => 2, 'maxlength'=>4096));
		echo $form->error($data, 'stock_nos'); ?></td>
<?php } ?>
</tr>
