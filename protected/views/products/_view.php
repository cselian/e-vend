<tr<?php Formatter::altRow($index); ?>>
<?php if (!isset($data)) {
$data = new Product; ?>
	<th><?php echo Controller::user_is('buyer') ? 'Quantity' : 'id'; ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('model')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('price')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('variations')); ?></th>
	<th>Info</th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('category')); ?></th>
<?php } else { if (Controller::user_is('buyer')) { ?>
	<td class="qty">
		<input type="hidden" name="pid[]" value="<?php echo $data->id;?>" />
		<input type="text" class="numedit" name="qty[]" value="" />
	</td><?php } else { ?>
	<td><?php echo CHtml::link(CHtml::encode($data->id), array('/products/view', 'id'=>$data->id));
	if (AppReports::isInReport()) echo ' ' . CHtml::link('&hellip;', array('/products/update', 'id'=>$data->id)); ?></td><?php } ?>
	<td><?php echo ImgHelper::product_img($data); ?></td>
	<td><?php echo AppPricing::getComputed($data); ?></td>
	<td><?php $data->adjustVariations(); echo $data->variations; ?></td>
	<td><?php if ($data->promote) echo ImgHelper::$newImg; ?></td>
	<td><?php echo CHtml::encode($data->category); ?></td>
<?php } ?>
</tr>
