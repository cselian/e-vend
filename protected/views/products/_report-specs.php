<?php
if (!isset($data)) return;
global $specs;
if ($index == 0)
{
	$extra = Product::getAllSpecifications();
	$specs = array();
	$missing = array();
	foreach ($items as $data)
	{
		$ok = isset($extra[$data->model]);
		if ($ok)
		{
			$specs[$data->model] = 1;
			unset($extra[$data->model]);
		}
		else
		{
			$missing[$data->model] = 1;
		}
	}

	echo sprintf('<tr class="head"><td colspan="6"><b>Missing Specs (%s):</b> %s<br/><br/><b>Extra Specs (%s):</b> %s</td></tr>' 
		, count($missing), count($missing) > 0 ? implode(' / ', array_keys($missing)) : 'None'
		, count($extra)  , count($extra) > 0   ? implode(' / ', array_keys($extra))   : 'None'
	);
}

$alt = ($index % 6 == 0) ? ($index % 12 == 0 ? 2 : 1) : 0;
if ($alt) echo ($index != 0 ? '</tr>' : '') . '<tr' . Formatter::altRow($index / 6, 1) . '>';

$ok = isset($specs[$data->model]);

$img = '<img src="' . ImgHelper::product_img($data, 1) . '" height="120" />';
echo sprintf('<td style="text-align: center;">%s<br>%s<br>%s</td>'
	, CHtml::link($img, array('/products/view', 'id'=>$data->id))
	, CHtml::link(sprintf('[edit %s]', $data->model), array('/products/update', 'id'=>$data->id))
	, $ok ? 'Specs Available' : 'No Specs');
if ($index == $total - 1)
{
	echo '</tr>';
	ImgHelper::report('missing');
	echo '<br/><br/>';
	ImgHelper::report('shown');
}
?>
