<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'product-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=> $model->isNewRecord ? array('enctype'=>'multipart/form-data') : array(),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php if ($model->isNewRecord) { ?>
	<div class="row">
		<?php echo $form->labelEx($model,'file to import'); ?>
		<?php echo $form->fileField($model,'file'); ?>
		<?php echo $form->error($model,'file'); ?>
		<?php echo CHtml::submitButton('Import'); ?>
		<?php echo '<br />' . CHtml::link('Download Sample', Yii::app()->baseUrl . '/data/import product.csv') . ' or '; ?>
		<?php echo CHtml::link('Price Import', Yii::app()->baseUrl . '/data/import product price.csv'). '<br /><br /><hr />'; ?>
	</div>
	<?php } ?>

	<div class="row">
		<?php echo $form->labelEx($model,'brand'); ?>
		<?php echo $form->textField($model,'brand',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'brand'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'category'); ?>
		<?php echo $form->textField($model,'category',array('size'=>32,'maxlength'=>32)); ?>
		<?php echo $form->error($model,'category'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'model'); ?>
		<?php echo $form->textField($model,'model',array('size'=>60,'maxlength'=>64)); ?>
		<?php echo $form->error($model,'model'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'mrp'); ?>
		<?php echo $form->textField($model,'mrp',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'mrp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'price'); ?>
		<?php echo $form->textField($model,'price',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'price'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'variations'); ?>
		<?php echo $form->textField($model,'variations',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'variations'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'specifications'); ?>
		<?php //echo $form->textArea($model,'specifications',array('size'=>60,'maxlength'=>2048)); ?>
<?php
$this->widget('ext.niceditor.nicEditorWidget',array(
	"model"=>$model,
	"attribute"=>'specifications',
	"defaultValue"=>'defaultValue text here',
	"config"=>array("maxHeight"=>"200px"),
	"width"=>"400px",       // Optional default to 100%
	"height"=>"200px",      // Optional default to 150px
));
?>
		<?php echo $form->error($model,'specifications'); ?>
		<p class="hint">
			Must be empty for the excel to be used (data/specs.tsv)
		</p>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'promote'); ?>
		<?php echo $form->checkBox($model,'promote'); ?>
		<?php echo $form->error($model,'promote'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
