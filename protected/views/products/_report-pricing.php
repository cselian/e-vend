<?php
global $dontShow;
if (!isset($data))
{
	$dontShow = is_null($buyer = AppHelper::formPostVal('buyer', NULL)) || empty($buyer);
	$buyers = Formatter::includeBlank(AppBuyers::getList());
	if (!$dontShow)
	{
		$br = Buyer::model()->findByPK($buyer);
		// TODO: Link to pricing rules of buyer
		AppPricing::spoofBuyer($br->id, $br->type);
	}
	
	echo CHtml::form(); // TODO: Consider making is GET not POST
	echo 'Buyer: ' . CHtml::dropDownList('buyer', $buyer, $buyers, array('onchange' => 'this.form.submit()'));
	echo CHtml::endForm();
}
else if ($dontShow)
{
	if ($dontShow === true) echo '<tr><td colspan="8">Please select a buyer first</td></tr>';
	$dontShow = 1;
	return;
}
?>

<tr<?php Formatter::altRow($index); ?>>
<?php if (!isset($data)) {
$data = new Product; ?>
	<th><?php echo Controller::user_is('buyer') ? 'Quantity' : 'id'; ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('model')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('dealer_price')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('mrp')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('calculation')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('final')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('discount')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('saving')); ?></th>
<?php } else {
$pc = AppPricing::getComputed($data, 'array'); ?>
	<td><?php echo CHtml::link(CHtml::encode($data->id), array('/products/update', 'id'=>$data->id)); ?></td>
	<td><?php echo CHtml::link($data->model, array('/products/view', 'id'=>$data->id)); ?></td>
	<td><?php echo Formatter::amount($data->price); ?></td>
	<td><?php echo Formatter::amount($data->mrp); ?></td>
	<td><?php echo $pc['calc']; ?></td>
	<td><?php echo Formatter::amount($pc['final']); ?></td>
	<td><?php echo $pc['disc'] . ' / rd: ' . round($pc['disc']); ?></td>
	<td><?php echo Formatter::amount($pc['saving']); ?></td>
<?php } ?>
</tr>
