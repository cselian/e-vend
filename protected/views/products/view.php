<?php
$this->breadcrumbs=array(
	'Products'=>array('index'),
	$model->id,
);

global $sidebarTop; $sidebarTop = sprintf('<div class="img-side"><img src="%s" /></div>', ImgHelper::product_img($model, 1));

if ($isBuyer)
{
	$vd = array('addtocart' => 1, 'data' => $model, 'isBuyer' => $isBuyer, 'radios' => 'radios');
	$sidebarTop .= sprintf('<div class="product"><form method="POST" onsubmit="return canSubmitProducts();" action="%s">', 
		Yii::app()->request->baseUrl . '/cart/add');
	$sidebarTop .= $this->renderPartial('_view-tile', $vd, 1);
	if (count(AppStock::AvailableVariations($model)) > 0)
		$sidebarTop .= '<br /><input type="submit" value="Add to Cart" />'
		 . '<div class="delivery"><b>NB: </b>' . AppEnv::get('deliveryNotice', 'Delivery Notice') . '</div>';
	$sidebarTop .= '</form></div>';
}

$this->menu= MenuHelper::Menu($model);
?>

<h1><?php echo ucfirst($model->brand) . ' ' . $model->model; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'brand',
		'category',
		'model',
		array('label'=>$model->getAttributeLabel('mrp'), 'type'=>'raw', 'value'=>Formatter::amount($model->mrp)),
		array('label'=>$model->getAttributeLabel('price'), 'type'=>'raw', 'value'=>AppPricing::getComputed($model)),
		'variations',
		array('label'=> 'Stock', 'type'=>'raw', 'value' => $stock , 'visible' => $stock),
		array('label'=>$model->getAttributeLabel('specifications'), 'type'=>'raw', 'value'=>$model->getSpecifications()),
		//array('label'=>$model->getAttributeLabel('promote'), 'type'=>'raw', 'value'=>Formatter::bool($model->promote)),
	),
));

ImgHelper::product_imgs($model->model, '<div id="altprod"><h2>Alternate Images</h2>', '</div>');
?>
