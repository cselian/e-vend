<?php if (!isset($addtocart)) { ?>
<div class="product">
<?php if ($data->promote) echo ImgHelper::$newImg; ?>
<div class="img"><?php
$url =  array('/products/view', 'id'=>$data->id);
echo CHtml::link(sprintf('<img src="%s" height="130" />', ImgHelper::product_img($data, 1)), $url); ?></div>
<h3><?php echo CHtml::link(/*ucfirst($data->brand) . ' ' .*/ $data->model, $url); ?></h3>
<?php
}

$price = AppPricing::getComputed($data, 'array');
if (AppBuyers::getBool('simplepricing'))
	echo sprintf('<div class="amt">
  <span class="price">Rs %s</span></div>
' , round($price['final']));
else
	echo sprintf('<div class="amt">
  <span class="off">%s<br/>OFF</span>
  <span class="mrp">Rs %s</span><br/>
  <span class="price">Rs %s</span></div>
  <span class="save">Save Rs %s</span><br/>
'
	, round($price['disc']) . '%' , $data->mrp, round($price['final']), round($price['saving']));

$varns = AppStock::AvailableVariations($data);
if (count($varns) > 1 && $radios)
{
	echo sprintf('<input type="radio" name="singlepid" value="%s" id="prod-%s" /><label for="prod-%s">%s</label>', 
		$data->id, $data->id, $data->id, 'BUY');
}
else if (count($varns) > 1) 
{
echo sprintf('<input type="hidden" name="pid[]" value="%s" />
<input type="text" name="qty[]" class="qty" autocomplete="off" readonly="readonly" maxlength="1" />', $data->id);

echo CHtml::dropDownList('varn[]', null, $varns) //, array('style'=>'display:none;')
	. '	<input type="hidden" name="mixed[]" class="varn" />';
	echo '<span class="qtylbl">QTY</span>';
}
else
{
	echo '<span class="out-of-stock">Out of stock</span>';
}

if (!isset($addtocart)) { ?>
</div>
<?php } ?>