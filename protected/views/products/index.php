<?php
$this->breadcrumbs=array(
	'Products',
);

//Yii::import('application.extensions.CKeypad.CKeypad');
//$this->widget('CKeypad');
Yii::app()->controller->add_script('products', 0);
Yii::app()->controller->add_script('products-fn', 0, CClientScript::POS_END);
if ($radios) Yii::app()->clientScript->registerScript('products-rad', '$prdRadios = true;' , CClientScript::POS_END);

$this->menu= MenuHelper::Menu();
?>

<h1>Products</h1>
<div id="pvwdiv" style="display:none;"></div>
<div id="varndiv" style="display:none;"></div>

<?php if ($isBuyer) {
$sVal = "model..."; $sCls = 'class="txtdeflt" ';
if (isset($_GET['s'])) { $sVal = $_GET['s']; $sCls = ''; } ?>
<form method="GET" onsubmit="return canSubmitSearch();" action="<?php echo Yii::app()->request->baseUrl . '/products/index/search'; ?>">
<?php FlashMessages::displayActions('/products/index/', $actions, $action, sprintf('<input type="text" id="searchprod" name="s" value="%s" %s/>
  <input type="submit" value="Search" title="Search by Model" />', $sVal, $sCls) ); ?>
</form>

<form method="POST" onsubmit="return canSubmitProducts();" action="<?php echo Yii::app()->request->baseUrl . '/cart/add'; ?>">
<?php } ?>

<?php
if (!$isTile)
{
	echo '<table id="products" class="grid" style="margin-top: 20px;">';
	$this->renderPartial('_view', array('index' => false));
}
$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view' . ($isTile ? '-tile' : ''),
	'viewData'=>array('radios' => $radios),
));
echo $isTile ? '<div class="clear"></div>' : '</table>';

if (Controller::user_is('buyer') && count($dataProvider->getData()) >0) { ?>
<input type="submit" value="Add to Cart" />
</form>
<?php } ?>
