<?php
global $dontShow, $stock, $csv, $distributor, $prods, $balance;
if (!isset($data))
{
	$dontShow = false;
	if (Controller::user_is('distributor'))
	{
		$distributor = UserIdentity::context('distributor');

		echo CHtml::form(); // TODO: Consider making is GET not POST
		echo 'Balance Under: ' . CHtml::textField('balance', AppHelper::formPostVal('balance', ''));
		echo CHtml::endForm();
	}
	else
	{
		if (!is_null($distributor = AppHelper::formPostVal('distributor', NULL)))
			$dontShow = true;

		$distributors = Formatter::includeBlank(AppLookups::Distributors());
		echo CHtml::form(); // TODO: Consider making is GET not POST
		echo 'Distributor: ' . CHtml::dropDownList('distributor', $distributor, $distributors, array('onchange' => 'this.form.submit()'));
		echo '<br />Balance Under: ' . CHtml::textField('balance', AppHelper::formPostVal('balance', ''));
		echo CHtml::endForm();
	}
	
	if (!$dontShow)
	{
		$balance = AppHelper::formPostVal('balance');
		if (empty($balance)) $balance = false;
		$stock = Stock::getList($distributor, $balance);
		$prods = $stock['products'];
		$stock = $stock['stock'];
		if ($balance !== false) echo '<br />All Products csv and orphaned entries will not be shown unless balance under criterion is removed';
	}
	
	$csv = 'pid,model,variation,balance';
}
else if ($dontShow)
{
	if ($dontShow === true) echo '<tr><td colspan="8">Please select a Distributor first</td></tr>';
	$dontShow = 1;
	return;
}
else if ($balance != false && !isset($prods[$data->id]))
{
	return;
}
?>

<tr<?php Formatter::altRow($index); ?>>
<?php if (!isset($data)) {
$data = new Product; ?>
	<th><?php echo Controller::user_is('buyer') ? 'Quantity' : 'id'; ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('model')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('variation')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('stock')); ?></th>
<?php } else {

?>
	<td><?php echo CHtml::link(CHtml::encode($data->id), array('/products/update', 'id'=>$data->id)); ?></td>
	<td><?php echo CHtml::link($data->model, array('/products/view', 'id'=>$data->id)); ?></td>
	<?php
$varns = $data->adjustVariations('array');
if ($balance !== false)
	$varns = Stock::reduceVariationsToInStock($data->id, $varns, $stock);

foreach ($varns as $ix=>$v)
{
	$vk = Stock::variationKey($data->id, $v);
	if (isset($stock[$vk])) {
		$bal = $stock[$vk];
		unset($stock[$vk]);
	} else {
		$bal = CHtml::link('add', array(sprintf('/stock/add/?d=%s&p=%s&v=%s', $distributor, $data->id, $v)));
	}
	
	echo sprintf('<td>%s</td><td>%s</td>', Product::variation_r($v), $bal, $vk);
	if ($ix != count($varns) - 1) echo '</tr><tr' . Formatter::altRow($index, 1) . '><td colspan="2"></td>
';
	$csv .= sprintf('
%s,%s,%s,%s', $data->id, $data->model, $v, '0');
}
?>
<?php } ?>
</tr>

<?php
if ($index == $total - 1)
{
	echo 'Saved ' . CHtml::link('All Products Stock Import file', Stock::genCsv($csv));
	echo '<br/>Orphaned Stock entries: ';
	echo count($stock) == 0 ? 'None' : '<br/>' . implode('<br/>
', array_keys($stock));
}
?>
