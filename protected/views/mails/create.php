<?php
$this->breadcrumbs=array(
	'Mail Templates'=>array('/mails'),
	'Create',
);
$this->menu = array(array ('label' => 'List Mails', 'url' => array ('index')));
?>
<h1>Create Mail Template</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>