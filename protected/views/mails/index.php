<?php
$this->breadcrumbs=array(
	'Mails',
);
$this->menu = array(
	array('label' => 'Refresh List', 'url' => array ('index')),
	array ('label' => 'Create Mail', 'url' => array ('create'))
);
?>
<h1>Mail Templates</h1>
<?php
$names = Mail::names(); $list = array();
foreach ($names as $k=>$v) $list[] = CHtml::link($v, array('/mails/create?name=' . $k));
echo 'Create for: ' . implode(', ', $list) . '.';
?>

<table class="grid" style="margin-top: 20px;">
<?php
$this->renderPartial('_view', array('index' => false));
$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
</table>
