<tr<?php Formatter::altRow($index); ?>>
<?php if (!isset($data)) {
$data = new Mail; ?>
	<th><?php echo CHtml::encode($data->getAttributeLabel('name')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('buyer')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('html')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('trial')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('date')); ?></th>
	<th>Actions</th>
<?php } else { ?>
	<td><?php echo Mail::name_r($data->name); ?></td>
	<td><?php echo $data->formatBuyer(); ?></td>
	<td><?php echo Formatter::bool($data->html); ?></td>
	<td><?php echo Formatter::bool($data->trial); ?></td>
	<td><?php echo Formatter::date($data->date, 1); ?></td>
	<td><?php echo CHtml::link('view', array('/mails/view/?id=' . $data->id)) . ' '
	 . CHtml::link('update', array('/mails/update/?id=' . $data->id)); ?></td>
<?php } ?>
</tr>
