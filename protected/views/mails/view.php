<?php
$this->breadcrumbs=array(
	'Mail Templates'=>array('index'),
	$model->name . $model->formatBuyer(' / %s'),
);
$this->menu = array(
	array('label' => 'List Mails', 'url' => array ('index')),
	array('label' => 'Create Mail', 'url' => array ('create')),
	array('label' => 'Update Mail', 'url' => array ('/mails/update/?id=' . $model->id)),
);
//$this->menu = MenuHelper::Menu($model);
?>

<h1>Mail Template for <?php echo $model->name . $model->formatBuyer(' / %s'); ?></h1>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'id',
		array('label'=>$model->getAttributeLabel('name'), 'type'=>'raw', 'value'=>$model->name),
		array('label'=>$model->getAttributeLabel('buyer'), 'type'=>'raw', 'value'=> $model->formatBuyer()),
		array('label'=>$model->getAttributeLabel('html'), 'type'=>'raw', 'value'=>Formatter::bool($model->html)),
		array('label'=>$model->getAttributeLabel('trial'), 'type'=>'raw', 'value'=>Formatter::bool($model->trial)),
		array('label'=>$model->getAttributeLabel('body'), 'type'=>'raw', 'value'=>$model->body, 'cssClass' => 'white-bg'),
		array('label'=>$model->getAttributeLabel('original'), 'type'=>'raw', 'value'=>$model->original),
	)
)); ?>
