<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'mail-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php
		if ($model->isNewRecord)
			echo $form->dropDownList($model,'name', Formatter::includeBlank(Mail::names()));
		else
			echo $form->textField($model,'name', array('readonly' => 'readonly'));
		?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'buyer'); ?>
		<?php echo $form->dropDownList($model,'buyer', Formatter::includeBlank(AppBuyers::getList())); ?>
		<?php echo $form->error($model,'buyer'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'html'); ?>
		<?php echo $form->checkBox($model,'html'); ?>
		<?php echo $form->error($model,'html'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'body'); ?>
		<?php echo $form->textArea($model,'body', array('cols' => '80', 'rows' => '20') ); ?>
		<?php echo $form->error($model,'body'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'original'); ?>
		<?php echo CHtml::textArea('orig', $model->original, array('cols' => '80', 'rows' => '10', 'style' => 'background-color: #E3E3E3;', 'readonly' => 'readonly') ); ?>
		<?php echo $form->error($model,'original'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'trial'); ?>
		<?php echo $form->checkBox($model,'trial'); ?>
		<?php echo $form->error($model,'trial'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'remove'); ?>
		<?php echo $form->checkBox($model,'remove'); ?>
		<?php echo $form->error($model,'remove'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton('Submit'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->