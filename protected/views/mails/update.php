<?php
$this->breadcrumbs=array(
	'Buyers'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu = array(
	array('label' => 'List Mails', 'url' => array ('index')),
	array('label' => 'Create Mail', 'url' => array ('create')),
	array('label' => 'View Mail', 'url' => array ('/mails/view/?id=' . $model->id)),
);
?>

<h1>Update Mail Template for <?php echo $model->name . $model->formatBuyer(' / %s'); ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>