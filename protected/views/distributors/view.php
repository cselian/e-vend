<?php
$this->breadcrumbs=array(
	'Distributors'=>array('index'),
	$model->name,
);

$this->menu= MenuHelper::Menu($model);
?>

<h1>View Distributor #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		array('label'=>$model->getAttributeLabel('date_added'), 'type'=>'raw', 'value'=>Formatter::date($model->date_added)),
		'office_email',
		array('label'=>$model->getAttributeLabel('address'), 'type'=>'raw', 'value'=>Address::formatEx($model)),
		array('label'=>MapHelper::$linkLabel, 'type'=>'raw', 'value'=>MapHelper::link($model)),
		array('label'=>$model->getAttributeLabel('tax_info'), 'type'=>'raw', 'value'=>Formatter::multiline($model->tax_info)),
		array('label'=>$model->getAttributeLabel('payment_info'), 'type'=>'raw', 'value'=>Formatter::multiline($model->payment_info)),
	),
)); ?>
