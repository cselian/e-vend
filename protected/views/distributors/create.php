<?php
$this->breadcrumbs=array(
	'Distributors'=>array('index'),
	'Create',
);

$this->menu= MenuHelper::Menu();
?>

<h1>Create Distributor</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
