<?php
$this->breadcrumbs=array(
	'Distributors'=>array('index'),
	'Manage',
);

$this->menu= MenuHelper::Menu();

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('distributor-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Distributors</h1>

<?php
echo CHtml::link('Clear Pricing/Distributor Cache', array('/site/admin?action=clear'), array('target' => '_new', 'class' => 'button')) . '<br /><br />';
?>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'distributor-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'name',
		'date_added',
		'office_email',
		'address',
		'pin',
		'tax_info',
		'payment_info',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
