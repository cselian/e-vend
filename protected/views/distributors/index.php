<?php
$this->breadcrumbs=array(
	'Distributors',
);

$this->menu= MenuHelper::Menu();
?>

<h1>Distributors</h1>

<?php
if (Yii::app()->controller->user_is('superadmin'))
	echo CHtml::link('Clear Pricing/Distributor Cache', array('/site/admin?action=clear'), array('target' => '_new', 'class' => 'button'));
?>

<table class="grid">
<?php
$this->renderPartial('_view', array('index' => false));
$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
));
?>
</table>
