<tr<?php Formatter::altRow($index); ?>>
<?php if (!isset($data)) {
$data = new Distributor; ?>
	<th><?php echo CHtml::encode($data->getAttributeLabel('id')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('name')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('date_added')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('office_email')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('address')); ?></th>
	<th><?php echo MapHelper::$linkLabel; ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('tax_info')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('payment_info')); ?></th><?php if (Yii::app()->controller->user_is('superadmin')) { ?>
	<th>Admin</th><?php } ?>
<?php } else { ?>
	<td><?php echo CHtml::link(CHtml::encode($data->id), array('/distributors/view', 'id'=>$data->id));
	if (AppReports::isInReport()) echo ' ' . CHtml::link('&hellip;', array('/distributors/update', 'id'=>$data->id));?></td>
	<td><?php echo CHtml::encode($data->name); ?></td>
	<td><?php echo Formatter::date($data->date_added); ?></td>
	<td><?php echo $data->office_email; ?></td>
	<td><?php echo Address::formatEx($data); ?></td>
	<td><?php echo MapHelper::link($data); ?></td>
	<td><?php echo Formatter::multiline($data->tax_info); ?></td>
	<td><?php echo Formatter::multiline($data->payment_info); ?></td><?php if (Yii::app()->controller->user_is('superadmin')) { ?>
	<th><?php echo User::createFor($data, 0, ''); ?></th><?php } ?>
<?php } ?>
</tr>
