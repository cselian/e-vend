<?php
$this->breadcrumbs=array(
	'Distributors',
);

$this->menu= MenuHelper::Menu();
?>

<h1>Distributors on the Map</h1>

<div id="map" style="height: 450px; border: double 2px #ccc">Loading Map...</div>
<?php
MapHelper::addHome();
$pts = $dataProvider->getData();
$fmt = '<a href="' . Yii::app()->request->baseUrl . '/distributors/%s">%s</a>';
foreach ($pts as $itm)
	MapHelper::addLocation($itm, $fmt, 'Distributor', 'green');

MapHelper::printMap();
?>
