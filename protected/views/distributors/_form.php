<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'distributor-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date_added'); ?>
		<?php if($model->id != null) echo Formatter::date($model->date_added); else echo '<br/>'; ?>
		<?php echo $form->error($model,'date_added'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'office_email'); ?>
		<?php echo $form->textField($model,'office_email',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'office_email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'address'); ?>
		<?php echo $form->textArea($model,'address',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'address'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pin'); ?>
		<?php echo $form->textField($model,'pin',array('size'=>6,'maxlength'=>6)); ?>
		<?php echo $form->error($model,'pin'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lat'); ?>
		<?php echo $form->textField($model,'lat',array('size'=>9,'maxlength'=>9)); ?>
		<?php echo $form->error($model,'lat'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lon'); ?>
		<?php echo $form->textField($model,'lon',array('size'=>9,'maxlength'=>9)); ?>
		<?php echo $form->error($model,'lon'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tax_info'); ?>
		<?php echo $form->textArea($model,'tax_info',array('size'=>60,'maxlength'=>512)); ?>
		<?php echo $form->error($model,'tax_info'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'payment_info'); ?>
		<?php echo $form->textArea($model,'payment_info',array('size'=>60,'maxlength'=>512)); ?>
		<?php echo $form->error($model,'payment_info'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->