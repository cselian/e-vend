<?php
$forBuyer = get_class($model) == 'Buyer';
$this->breadcrumbs=array(
	'Addresses',
	'Distances to ' . ($forBuyer ? 'Buyer' : '') . '#' . $model->id
);

$this->menu= MenuHelper::Menu();

echo !$forBuyer
	? sprintf('<h2>Distances to Distributors from <br />Address #%s - %s of buyer %s</h2>',
			$model->id, $model->name, $model->buyer->name)
	: sprintf('<h2>Distances to Distributors from Buyer #%s - %s</h2>',
			$model->id, $model->name);
?>

<div id="map" style="height: 450px; border: double 2px #ccc">Loading Map...</div>
<?php
MapHelper::addHome();
$pts = $dataProvider->getData();

$fmt = '<a href="' . Yii::app()->request->baseUrl . '/' . $model->tableName() . '/%s">%s</a>';
MapHelper::addLocation($model, $fmt, $model->name, 'paleblue');

foreach ($pts as $itm)
{
	$fmt = '<a href="/distributors/%s">' . $itm->name . '</a>' . sprintf(' (%s)', $distances[$itm->id]);
	MapHelper::addLocation($itm, $fmt, $name, 'green');
}
MapHelper::printMap();
?>
