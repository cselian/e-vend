<div class="portlet" id="yw0">
	<div class="portlet-decoration">
	<div class="portlet-title">Addresses of <?php echo $buyer->name; ?> / you</div>
	</div>

	<div class="portlet-content">
<?php
function print_addy($link, $itm)
{
	echo sprintf('<br/><br/>%s<br/>
%s', $link, Address::formatEx($itm));
}

if (Controller::user_is('notloggedin'))
	echo sprintf('<div class="flash-error" style="margin: 0">Please %s then come back to the cart to checkout.</div>', UserIdentity::signupOrLogin());
else
	echo CHtml::link('Add Address', array('/addresses/create/'), array('id' =>'add-address'));

print_addy('<u>'.$caption.'</u>', $buyer);
foreach ($list as $itm)
	print_addy(CHtml::link($itm->name, array('/addresses/update', 'id'=>$itm->id)), $itm);
?>
	</div>
</div>
