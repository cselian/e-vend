<?php
$this->breadcrumbs=array(
	'Addresses'=>array('index'),
	$model->name,
);

$this->menu= MenuHelper::Menu($model);
?>

<h1>View Address #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		array('label'=>$model->getAttributeLabel('buyer_id'), 'type'=>'raw', 'value'=>Formatter::displayObject($model->buyer)),
		'name',
		'address',
		'pin',
		'phone',
		array('label'=>MapHelper::$linkLabel, 'type'=>'raw', 'value'=>MapHelper::link($model)),
		array('label'=>$model->getAttributeLabel('assigned_to_id'), 'type'=>'raw', 'value'=>Formatter::displayObject($model->assignedTo) . Address::linkToDistances($model)),
	),
)); ?>
