<?php
$this->breadcrumbs=array(
	'Addresses'=>array('index'),
	'Create',
);

$this->menu= MenuHelper::Menu();
?>

<h1>Create Address</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
