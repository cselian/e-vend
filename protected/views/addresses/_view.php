<tr<?php Formatter::altRow($index); ?>>
<?php if (!isset($data)) {
$data = new Address; ?>
	<th><?php echo CHtml::encode($data->getAttributeLabel('id')); ?></th>
	<?php if (!Yii::app()->controller->user_is('buyer')) echo '<th>' . CHtml::encode($data->getAttributeLabel('buyer_id')) . '</th>'; ?>
	<th><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('name')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('address')); ?></th>
	<th><?php echo MapHelper::$linkLabel; ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('phone')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('assigned_to_id')); ?></th>
<?php } else { ?>
	<td><?php echo CHtml::link(CHtml::encode($data->id), array('/addresses/view', 'id'=>$data->id));
		if (!Yii::app()->controller->user_is('distributor') || AppReports::isInReport()) echo ' ' . CHtml::link('&hellip;', array('/addresses/update', 'id'=>$data->id)); ?></td>
	<?php if (!Yii::app()->controller->user_is('buyer')) echo '<td>' . Formatter::displayObject($data->buyer) . '</td>'; ?>
	<td><?php echo Formatter::displayObject($data->user); ?></td>
	<td><?php echo CHtml::encode($data->name); ?></td>
	<td><?php echo Address::formatEx($data); ?></td>
	<td><?php echo MapHelper::link($data); ?></td>
	<td><?php echo $data->phone; ?></td>
	<td><?php echo Formatter::displayObject($data->assignedTo) . Address::linkToDistances($data); ?></td>
<?php } ?>
</tr>
