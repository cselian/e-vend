<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'address-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'buyer_id'); ?>
		<?php echo CHtml::textField('', $model->buyer->name, array('readonly' => 'readonly')); ?>
		<?php echo $form->error($model,'buyer_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_id'); ?>
		<?php $userName = $model->user != null ? sprintf('%s (%s)', $model->user->name, $model->user->email) : 'null';
		echo CHtml::textField('',  $userName, array('size'=>40, 'readonly' => 'readonly')); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'name'); ?>
		<p class="hint">
			Name used to refer to the address. If sending to someone, please enter his name in the address field.
		</p>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'address'); ?>
		<?php echo $form->textArea($model,'address',array('cols'=>40, 'rows' => 4,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'address'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pin'); ?>
		<?php echo $form->textField($model,'pin',array('size'=>6,'maxlength'=>6)); ?>
		<?php echo $form->error($model,'pin'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'phone'); ?>
		<?php echo $form->textField($model,'phone',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'phone'); ?>
	</div>

<?php if (!Controller::user_is('buyer')) { ?>
	<div class="row">
		<?php echo $form->labelEx($model,'lat'); ?>
		<?php echo $form->textField($model,'lat',array('size'=>9,'maxlength'=>9)); ?>
		<?php echo $form->error($model,'lat'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lon'); ?>
		<?php echo $form->textField($model,'lon',array('size'=>9,'maxlength'=>9)); ?>
		<?php echo $form->error($model,'lon'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'assigned_to_id'); ?>
		<?php echo Yii::app()->controller->user_is('superadmin')
			? $form->dropDownList($model,'assigned_to_id', Formatter::includeBlank(AppLookups::Distributors()))
					. Address::linkToDistances($model)
			: Formatter::displayObject($data->assignedTo); ?>
		<?php echo $form->error($model,'assigned_to_id'); ?>
	</div>
<?php } ?>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
