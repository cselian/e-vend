<?php
$this->breadcrumbs=array(
	'Pricing Rules',
);

$this->menu= MenuHelper::Menu();
?>

<h1>Pricing Rules</h1>

<?php
echo CHtml::link('Clear Pricing/Distributor Cache', array('/site/admin?action=clear'), array('target' => '_new', 'class' => 'button'));
?>

<table class="grid" style="margin-top: 20px;">
<?php
$this->renderPartial('_view', array('index' => false));
$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
</table>
