<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'pricing-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=> $model->isNewRecord ? array('enctype'=>'multipart/form-data') : array(),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
	
	<?php if ($model->isNewRecord) { ?>
	<div class="row">
		<?php echo $form->labelEx($model,'file to import'); ?>
		<?php echo $form->fileField($model,'file'); ?>
		<?php echo $form->error($model,'file'); ?>
		<?php echo CHtml::submitButton('Import'); ?>
		<?php echo '<br />' . CHtml::link('Download Sample', Yii::app()->baseUrl . '/data/import pricing.csv') . '<br /><br /><hr />'; ?>
	</div>
	<?php } ?>

	<div class="row">
		<?php echo $form->labelEx($model,'product_id'); ?>
		<?php echo $form->textField($model,'product_id'); ?>
		<?php echo CHtml::label($model->product != null ? Product::format($model->product) : 'Not Set', false, array('id'=>'productName')); ?>
		<?php echo $form->error($model,'product_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'buyer_id'); ?>
		<?php echo $form->textField($model,'buyer_id'); ?>
		<?php echo CHtml::label($model->buyer != null ? $model->buyer->name : 'Not Set', false, array('id'=>'buyerName')); ?>
		<?php echo $form->error($model,'buyer_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'buyer_type'); ?>
		<?php echo $form->dropDownList($model,'buyer_type', Formatter::includeBlank(AppLookups::$buyerTypes)); ?>
		<?php echo $form->error($model,'buyer_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'discount'); ?>
		<?php echo $form->textField($model,'discount',array('size'=>5,'maxlength'=>5)); ?>
		<?php echo $form->error($model,'discount'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'price'); ?>
		<?php echo $form->textField($model,'price',array('size'=>7,'maxlength'=>7)); ?>
		<?php echo $form->error($model,'price'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
