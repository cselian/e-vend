<?php
$this->breadcrumbs=array(
	'Pricings'=>array('index'),
	'Create',
);

$this->menu= MenuHelper::Menu();
?>

<h1>Create Pricing</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
