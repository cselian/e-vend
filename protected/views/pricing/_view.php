<tr<?php Formatter::altRow($index); ?>>
<?php if (!isset($data)) {
$data = new Buyer; ?>
	<th><?php echo CHtml::encode($data->getAttributeLabel('id')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('product_id')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('buyer_id')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('buyer_type')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('discount')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('price')); ?></th>
<?php } else { ?>
	<td><?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?></td>
	<td><?php if ($data->product != null) echo Product::format($data->product); ?></td>
	<td><?php echo Formatter::displayObject($data->buyer); ?></td>
	<td><?php echo AppLookups::BuyerType($data->buyer_type); ?></td>
	<td><?php echo $data->discount; ?></td>
	<td><?php echo Formatter::amount($data->price); ?></td>
<?php } ?>
</tr>
