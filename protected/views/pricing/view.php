<?php
$this->breadcrumbs=array(
	'Pricings'=>array('index'),
	$model->id,
);

$this->menu= MenuHelper::Menu($model);
?>

<h1>View Pricing #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		array('label'=>$model->getAttributeLabel('product_id'), 'type'=>'raw', 'value'=>Product::format($model->product)),
		array('label'=>$model->getAttributeLabel('buyer_id'), 'type'=>'raw', 'value'=>Formatter::displayObject($model->buyer)),
		array('label'=>$model->getAttributeLabel('buyer_type'), 'type'=>'raw', 'value'=>AppLookups::BuyerType($model->buyer_type)),
		'discount',
		array('label'=>$model->getAttributeLabel('price'), 'type'=>'raw', 'value'=>Formatter::amount($model->price)),
	),
)); ?>
