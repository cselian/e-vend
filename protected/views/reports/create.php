<?php
$this->breadcrumbs=array(
	'Reports'=>array('index'),
	'Create',
);

$this->menu= MenuHelper::Menu();
?>

<h1>Create Report</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>