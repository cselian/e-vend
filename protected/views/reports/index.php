<?php
$this->breadcrumbs=array(
	'Reports',
);

$this->menu= MenuHelper::Menu();
?>

<h1>Reports</h1>

<?php
if (AppReports::isAndAssumingDev())
	echo CHtml::link('Clear Report Cache', array('/site/admin?action=clearreport'), array('target' => '_new', 'class' => 'button'));
?>

<table class="grid" style="margin-top: 20px;">
<?php
$viewName = '_view' . (AppReports::isAndAssumingDev() ? '-dev' : '');
$this->renderPartial($viewName, array('index' => false));
$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>$viewName,
)); ?>
</table>
