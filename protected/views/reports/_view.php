<tr<?php Formatter::altRow($index); ?>>
<?php if (!isset($data)) {
$data = new Report; ?>
	<th><?php echo CHtml::encode($data->getAttributeLabel('name')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('group')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('description')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('last_count')); ?></th>
<?php } else { ?>
	<td><?php echo $data->runUrl(); ?></td>
	<td><?php echo CHtml::encode($data->group); ?></td>
	<td><?php echo CHtml::encode($data->description); ?></td>
	<td><?php echo CHtml::encode($data->last_count); ?></td>
<?php } ?></tr>
