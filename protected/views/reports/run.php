<?php
$this->breadcrumbs=array(
	'Reports',
);

$isCsv = Report::isUrlCsv();
if (!$isCsv) { // not if csv
?>

<h1>Report: <?php echo $report->name; ?></h1>

<table class="grid" style="margin-top: 20px;">
<?php
} // not csv ends

$dataProvider->setPagination(false);
$viewData = array(
	'total' => $dataProvider->getTotalItemCount(),
	'items' => $dataProvider->getData(),
	'report' => $report,
	'isCsv' => $isCsv,
);

$this->renderPartial($viewName, array_merge($viewData, array('index' => false)));
$lvwData = array(
	'dataProvider'=>$dataProvider,
	'itemView'=>$viewName,
	'viewData'=>$viewData,
);

if ($isCsv) {
	$lvwData['useTags'] = false; // changed CListView and CBaseListView to support this
	$lvwData['template'] = '{items}';
}
$this->widget('zii.widgets.CListView', $lvwData); ?>
</table>
