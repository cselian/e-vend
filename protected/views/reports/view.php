<?php
$this->breadcrumbs=array(
	'Reports'=>array('index'),
	$model->name,
);

$this->menu= MenuHelper::Menu($model);
?>

<h1>View Report #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'slug',
		'name',
		'group',
		'description',
		'returns',
		'condition',
		'accessible_to',
		'last_count',
	),
)); ?>
