<tr<?php Formatter::altRow($index); ?>>
<?php if (!isset($data)) {
$data = new Report; ?>
	<th><?php echo CHtml::encode($data->getAttributeLabel('id')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('slug')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('name')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('group')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('description')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('returns')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('condition')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('accessible_to')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('last_count')); ?></th>
<?php } else { ?>
	<td><?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id));
	echo ' ' . CHtml::link('&hellip;', array('update', 'id'=>$data->id))
	. ' ' . $data->runUrl(1); ?></td>
	<td><?php echo CHtml::encode($data->slug); ?></td>
	<td><?php echo CHtml::encode($data->name); ?></td>
	<td><?php echo CHtml::encode($data->group); ?></td>
	<td><?php echo CHtml::encode($data->description); ?></td>
	<td><?php echo CHtml::encode($data->returns); ?></td>
	<td><?php echo CHtml::encode($data->condition); ?></td>
	<td><?php echo CHtml::encode($data->accessible_to); ?></td>
	<td><?php echo CHtml::encode($data->last_count); ?></td>
<?php } ?>
</tr>
