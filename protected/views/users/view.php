<?php
$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->name,
);

$this->menu= MenuHelper::Menu($model);
?>

<h1>View User #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'email',
		'employee_id',
		array('label'=>$model->getAttributeLabel('date_added'), 'type'=>'raw', 'value'=>Formatter::date($model->date_added)),
		array('label'=>$model->getAttributeLabel('type'), 'type'=>'raw', 'value'=>AppLookups::UserType($model->type, $model->buyer)),
		array('label'=>$model->getAttributeLabel('admin'), 'type'=>'raw', 'value'=>Formatter::bool($model->admin)),
		array('label'=>$model->getAttributeLabel('distributor_id'), 'type'=>'raw', 'value'=>Formatter::displayObject($model->distributor)),
		array('label'=>$model->getAttributeLabel('buyer_id'), 'type'=>'raw', 'value'=>Formatter::displayObject($model->buyer)),
		array('label'=>$model->getAttributeLabel('deactive'), 'type'=>'raw', 'value'=>Formatter::bool($model->deactive)),
	),
)); ?>
