<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=> $model->isNewRecord ? array('enctype'=>'multipart/form-data') : array(),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php if ($model->isNewRecord) { ?>
	<div class="row">
		<?php echo $form->labelEx($model,'file to import'); ?>
		<?php echo $form->fileField($model,'file'); ?>
		<?php echo $form->error($model,'file'); ?>
		<?php echo CHtml::submitButton('Import'); ?>
		<?php echo '<br />' . CHtml::link('Download Sample', Yii::app()->baseUrl . '/data/import user.csv') . '<br /><br /><hr />'; ?>
		<p class="hint">
			Make sure the domains match buyer setting. Checking will not be done.
		</p>
	</div>
	<?php } ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<?php if ($model->type != 1) { ?>
	<div class="row">
		<?php echo $form->labelEx($model,'admin'); ?>
		<?php echo $form->checkBox($model,'admin'); ?>
		<?php echo $form->error($model,'admin'); ?>
	</div><?php } ?>

	<div class="row">
		<?php echo $form->hiddenField($model,'type'); ?>
		<?php echo CHtml::label(AppLookups::UserType($model->type), ''); ?>
		<?php echo $form->error($model,'type'); ?>

	<?php if ($model->type == 2) { ?>
		<?php echo $form->hiddenField($model,'distributor_id'); ?>
		<?php echo CHtml::textField('distributor_Name', Formatter::displayObject($model->distributor), array('readonly' => 'readonly')); ?>
		<?php echo $form->error($model,'distributor_id'); ?>
	<?php } else if ($model->type == 3) { ?>
		<?php echo $form->hiddenField($model,'buyer_id'); ?>
		<?php echo CHtml::textField('buyer_Name', Formatter::displayObject($model->buyer), array('readonly' => 'readonly')); ?>
		<?php echo $form->error($model,'buyer_id'); ?>
	<?php } ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'deactive'); ?>
		<?php echo $form->checkBox($model,'deactive'); ?>
		<?php echo '<br />' . CHtml::textField('reason','') . '*'; ?>
		<?php echo $form->error($model,'deactive'); ?>
		<p class="hint">
			* Give a reason if activating / deactivating the user.
		</p>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
