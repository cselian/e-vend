<?php
$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Manage',
);

$this->menu= MenuHelper::Menu();

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('user-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Users</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'user-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	//'nullDisplay'=>'Not Set',
	'columns'=>array(
		'id',
		'name',
		'email',
		'employee_id',
		array('name' => 'date_added', 'value' => 'Formatter::date($data->date_added)'),
		array('name' => 'type', 'value' => 'AppLookups::UserType($data->type, $data->buyer)'),
		array('name' => 'admin', 'value' => 'Formatter::bool($data->admin, null)'),
		array('name' => 'distributor_id', 'value' => 'Formatter::displayObject($data->distributor, null)'),
		array('name' => 'buyer_id', 'value' => 'Formatter::displayObject($data->buyer, null)'),
		array('name' => 'deactive', 'value' => 'Formatter::bool($data->deactive, null)'),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
