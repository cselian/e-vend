<tr<?php Formatter::altRow($index); ?>>
<?php if (!isset($data)) {
$data = new User; ?>
	<th><?php echo CHtml::encode($data->getAttributeLabel('id')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('name')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('email')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('employee_id')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('date_added')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('type')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('distributor_id')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('buyer_id')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('admin')) . ' / '
		. CHtml::encode($data->getAttributeLabel('deactive')); ?></th>
<?php } else { ?>
	<td><?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?></td>
	<td><?php echo CHtml::encode($data->name); ?></td>
	<td><?php echo CHtml::encode($data->email); ?></td>
	<td><?php echo CHtml::encode($data->employee_id); ?></td>
	<td><?php echo Formatter::date($data->date_added); ?></td>
	<td><?php echo AppLookups::UserType($data->type, $data->buyer); ?></td>
	<td><?php echo Formatter::displayObject($data->distributor); ?></td>
	<td><?php echo Formatter::displayObject($data->buyer, ''); ?></td>
	<td><?php echo Formatter::bool($data->admin, '') . ' / ' .
		Formatter::bool($data->deactive, ''); ?></td>
<?php } ?>
</tr>
