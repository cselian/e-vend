<?php
$this->breadcrumbs=array(
	'Users',
);

$this->menu= MenuHelper::Menu();
?>

<h1>Users</h1>

<table class="grid">
<?php
$this->renderPartial('_view', array('index' => false));
$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
</table>
