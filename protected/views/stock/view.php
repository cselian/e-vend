<?php
$this->breadcrumbs=array(
	'Stocks'=>array('index'),
	$model->id,
);

$this->menu= MenuHelper::Menu($model);
?>

<h1>View Stock #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'distributor_id',
		'product_id',
		'variation',
		'balance',
	),
)); ?>
