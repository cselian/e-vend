<tr<?php Formatter::altRow($index); ?>>
<?php if (!isset($data)) {
$data = new Stock; ?>
	<th><?php echo CHtml::encode($data->getAttributeLabel('id')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('distributor_id')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('product_id')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('variation')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('balance')); ?></th>
<?php } else { ?>
	<td><?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?></td>
	<td><?php echo CHtml::encode($data->distributor_id); ?></td>
	<td><?php echo CHtml::encode($data->product_id); ?></td>
	<td><?php echo CHtml::encode($data->variation); ?></td>
	<td><?php echo CHtml::encode($data->balance); ?></td>
<?php } ?></tr>
