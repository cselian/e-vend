<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'stock-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=> $model->isNewRecord ? array('enctype'=>'multipart/form-data') : array(),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php if ($model->isNewRecord) { ?>
	<div class="row">
		<?php echo $form->labelEx($model,'file to import'); ?>
		<?php echo $form->fileField($model,'file'); ?>
		<?php echo $form->error($model,'file'); ?>
		<?php echo CHtml::submitButton('Import'); ?>
		<?php echo '<br />' . CHtml::link('Download Sample', Yii::app()->baseUrl . '/data/import stock.csv') . ' or '; ?>
		<?php echo CHtml::link('All Products Stock (run stock Report to generate)', Stock::genCsv()). '<br /><br /><hr />'; ?>
	</div>
	<?php } ?>

	<div class="row">
		<?php echo $form->labelEx($model,'distributor_id'); ?>
		<?php echo $form->textField($model,'distributor_id'); ?>
		<?php echo $form->error($model,'distributor_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'product_id'); ?>
		<?php echo $form->textField($model,'product_id'); ?>
		<?php echo $form->error($model,'product_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'variation'); ?>
		<?php echo $form->textField($model,'variation',array('size'=>32,'maxlength'=>32)); ?>
		<?php echo $form->error($model,'variation'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'balance'); ?>
		<?php echo $form->textField($model,'balance'); ?>
		<?php echo $form->error($model,'balance'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
