<?php
$this->breadcrumbs=array(
	'Stocks'=>array('index'),
	'Create',
);

$this->menu= MenuHelper::Menu();
?>

<h1>Create Stock</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>