<?php
$this->breadcrumbs=array(
	'Stocks'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu= MenuHelper::Menu($model);
?>

<h1>Update Stock <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>