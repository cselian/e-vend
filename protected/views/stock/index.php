<?php
$this->breadcrumbs=array(
	'Stocks',
);

$this->menu= MenuHelper::Menu();
?>

<h1>Stocks</h1>

<?php FlashMessages::displayActions('/stock/index/', $actions); ?>

<table class="grid">
<?php
$this->renderPartial('_view', array('index' => false));
$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
</table>
