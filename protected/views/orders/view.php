<?php
$atts = array(
		'id',
		array('label'=>$model->getAttributeLabel('buyer_id'), 'type'=>'raw', 'value'=>Formatter::displayObject($model->buyer)),
		array('label'=>$model->getAttributeLabel('user_id'), 'type'=>'raw', 'value'=> User::link($model->user, $model->buyer)),
		array('label'=>$model->getAttributeLabel('address_id'), 'type'=>'raw', 'value'=>$model->formatAddress()),
		array('label'=>$model->getAttributeLabel('billing_address_id'), 'type'=>'raw', 'value'=>$model->formatAddress(1)),
		array('label'=>$model->getAttributeLabel('order_date'), 'type'=>'raw', 'value'=>Formatter::date($model->order_date, 1)),
		array('label'=>$model->getAttributeLabel('amount'), 'type'=>'raw', 'value'=>Formatter::amount($model->amount)),
		array('label'=>$model->getAttributeLabel('verification'), 'type'=>'raw', 'value'=>Order::hideVerification($model)),
		array('label'=>$model->getAttributeLabel('status'), 'type'=>'raw', 'value'=>AppLookups::OrderStatus($model->status)),
		array('label'=>$model->getAttributeLabel('approval'), 'type'=>'raw', 'value'=>$model->formatApproval()),
		array('label'=>$model->getAttributeLabel('despatched_date'), 'type'=>'raw', 'value'=>Formatter::date($model->despatched_date, 1)),
		'despatcher_reference',
		array('label'=>$model->getAttributeLabel('despatcher_id'), 'type'=>'raw', 'value'=>Formatter::displayObject($model->despatcher) . $addr),
		array('label'=>'Despatcher Payment Details', 'type'=>'raw', 'value'=>$model->despatcher != null ? $model->despatcher->payment_info : ''),
		array('label'=>$model->getAttributeLabel('payment_type'), 'type'=>'raw', 'value'=>AppLookups::PaymentType($model->payment_type)),
		'payment_ref',
		array('label'=>$model->getAttributeLabel('payment_date'), 'type'=>'raw', 'value'=>Formatter::date($model->payment_date, 0)),
		array('label'=>$model->getAttributeLabel('payment_confirmed'), 'type'=>'raw', 'value'=>Formatter::bool($model->payment_confirmed)),
);
if ($attsOnly) return $atts;

$this->breadcrumbs=array(
	'Orders'=>array('index'),
	$model->id,
);

$model->addMenu('pay');
$model->addMenu('purge');
$this->menu= MenuHelper::Menu($model);
$addr = $model->despatcher != null ? ' <br/>Address: ' . Address::formatEx($model->despatcher) : '';
Yii::app()->controller->add_script('products', 0);
?>

<h1>View Order #<?php echo $model->id; ?></h1>
<div id="pvwdiv" style="display:none;"></div>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>$atts
)); ?>

<br /><br />
<h2>Items</h2>

<table class="grid">
<?php
//print_r($model->orderItems);
$this->renderPartial('/orderItems/_view', array('index' => false));
$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>new CArrayDataProvider($model->orderItems),
	'itemView'=>'/orderItems/_view',
	'template'=>'{items}'
));
?>
</table>
