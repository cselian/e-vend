<?php
$this->breadcrumbs=array(
	'Orders'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu= MenuHelper::Menu($model);
Yii::app()->controller->add_script('products', 0);

if (isset($errors) && $errors != null) echo sprintf('<div class="flash-error">%s</div>', $errors);
if (isset($messages) && $messages != null) echo sprintf('<div class="flash-notice">%s</div>', $messages);
?>

<h1>Update Order <?php echo $model->id; ?></h1>
<div id="pvwdiv" style="display:none;"></div>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'addresses' => $addresses)); ?>
