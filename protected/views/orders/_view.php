<tr<?php Formatter::altRow($index); ?>>
<?php if (!isset($data)) {
$data = new Order; ?>
	<th><?php echo CHtml::encode($data->getAttributeLabel('id')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('buyer_id')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('address_id')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('order_date')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('status')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('approval')); ?></th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('amount')); ?></th>
	<th>Despatch Info</th>
	<th><?php echo CHtml::encode($data->getAttributeLabel('verification')); ?></th>
	<th><abbr title="Type, Date, Verification, Purge">Payment</abbr></th>
<?php } else { ?>
	<td><?php echo CHtml::link(CHtml::encode($data->id), array('/orders/view', 'id'=>$data->id));
	if ($this->user_is('superorbuyeradmin') || AppReports::isInReport()) echo ' ' . CHtml::link('&hellip;', array('/orders/update', 'id'=>$data->id)); ?></td>
	<td><?php echo Formatter::displayObject($data->buyer); ?></td>
	<td><?php echo User::link($data->user, $data->buyer); ?></td>
	<td><?php echo $data->formatAddress(); ?></td>
	<td><?php echo Formatter::date($data->order_date); ?></td>
	<td><?php echo AppLookups::OrderStatus($data->status); ?></td>
	<td><?php echo $data->formatApproval(); ?></td>
	<td><?php echo Formatter::amount($data->amount); ?></td>
	<td><?php echo Formatter::displayObject($data->despatcher, '') 
		. ($data->despatcher_reference != null ? ' / ' . $data->despatcher_reference : '')
		. ($data->despatched_date != null ? ' / ' . Formatter::date($data->despatched_date) : ''); ?></td>
	<td><?php echo Order::hideVerification($data); ?></td>
	<td><?php echo Formatter::Concat(' / ', // TODO: ConcatArray
		$data->formatPaymentType(),
		Formatter::date($data->payment_date, 0),
		$data->paymentLinkOrStatus(), $data->purgeLink() ); ?></td>
<?php } ?>
</tr>
