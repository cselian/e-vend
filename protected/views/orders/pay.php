<?php
$this->breadcrumbs=array(
	'Orders'=>array('index'),
	$model->id,
);

$this->menu= MenuHelper::Menu($model);
$addr = $model->despatcher != null ? ' <br/>Address: ' . Address::formatEx($model->despatcher) : '';
$pytData = array(
	'Type: %s' => AppLookups::PaymentType($model->payment_type),
	', Ref: %s' => $model->payment_ref,
	', Date: %s' => Formatter::date($model->payment_date, 0),
	', Confirmed: %s' => $model->payment_ref != '' ? Formatter::bool($model->payment_confirmed) : '',
);
if (isset($payLink))
{
	if ($model->needsApprovalOrRejection(1))
		$pytData['<strong>Actions</strong>: %s'] = 'Cannot Pay yet because Approval Pending';
	else if ($model->isRejected())
		$pytData['<strong>Actions</strong>: %s'] = 'Cannot Pay since order rejected.';
	else
	$pytData['<br /><b>Actions</b>: %s'] = $payLink;
}
//print_r($pytData);
?>

<h1>Online Payment Details for Order #<?php echo $model->id; ?></h1>

<?php
$atts = array(
		array('label'=>'Order', 'type'=>'raw', 'value'=>CHtml::link('#' . $model->id, array('/orders/view', 'id'=>$model->id))),
		array('label'=>$model->getAttributeLabel('buyer_id'), 'type'=>'raw', 'value'=>Formatter::displayObject($model->buyer)),
		array('label'=>$model->getAttributeLabel('address_id'), 'type'=>'raw', 'value'=>$model->formatAddress()),
		array('label'=>$model->getAttributeLabel('billing_address_id'), 'type'=>'raw', 'value'=>$model->formatAddress(1)),
		array('label'=>$model->getAttributeLabel('order_date'), 'type'=>'raw', 'value'=>Formatter::date($model->order_date, 1)),
		array('label'=>$model->getAttributeLabel('amount'), 'type'=>'raw', 'value'=>Formatter::amount($model->amount)),
		array('label'=>$model->getAttributeLabel('status'), 'type'=>'raw', 'value'=>AppLookups::OrderStatus($model->status)),
		array('label'=>$model->getAttributeLabel('approval'), 'type'=>'raw', 'value'=>$model->formatApproval()),
		array('label'=>'Payment Info', 'type'=>'raw', 'value'=>Formatter::ConcatArray(' ', $pytData), 
			'cssClass' => isset($payLink) ? 'pytinfo' : ''),
	);

if (isset($details))
{
	$tbl = AppHelper::buildTable(null, 'tblsmall');
	foreach ($details as $key=>$value) $tbl->add_row($key, $value);
	$atts[] = array('label'=> 'Payment Details', 'type'=>'raw', 'value' => $tbl->generate(), 'cssClass' => 'pytdetl');
}

if ($dummy)	$atts[] =
		array('label'=> 'Dummy PG Links', 'type'=>'raw', 'value' => $dummy);

$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>$atts,
));

if (isset($pgs['amex']))
{
	
	echo '<div class="right" style="margin-top: 20px">' .
		CHtml::image(Yii::app()->baseurl . '/img/pg/amex-logo.png', 'amex', array('height' => 70)) . ' ' .
		CHtml::image(Yii::app()->baseurl . '/img/pg/amex-safekey.jpg', 'amex safekey', array('height' => 70))
		. '</div>';
}
?>

