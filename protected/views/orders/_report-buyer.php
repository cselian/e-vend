<?php
$isHeader = !isset($data);
if ($isHeader) {
	// Link to csv
	if (!$isCsv) echo CHtml::link('Download as CSV', Report::getUrl($report, 1), array('target'=>'_blank'));
	
	function other_orders($m, $csv)
	{
		global $others;
		if ($others == null)
		{
			$others = array();
			foreach ($m as $o) {
				if (!isset($others[$o->user_id])) $others[$o->user_id] = array();
				$others[$o->user_id][$o->id] = $o->formatStatus();
			}
			return;
		}
		
		if (!isset($others[$m->user_id])) return '';
		$oth = $others[$m->user_id];
		$res = array();
		foreach ($oth as $id=>$status) {
			if ($m->id == $id) continue;
			$id = $csv ? $id : CHtml::link($id, array('/orders/' . $id));
			$res[] = sprintf('%s (%s)', $id, $status);
		}
		return implode(', ', $res);
	}
	other_orders($items, 0);

	$cols = explode('	', 'Id	Date	Name	Email	Emp Id	' .
		'Device	Model	IMEI Number	' . //Color
		'Despatcher	Despatcher Ref	' .
		'Contact number	Location	Status	Other(s)');
	if (!$isCsv) {
		$cols[array_search('Location', $cols)] = '<abbr title="(of Billing Address)">Location</abbr>';
		$cols[array_search('Other(s)', $cols)] = '<abbr title="(orders of Same User)">Other(s)</abbr>';
	}
} else {
	$itms = array(array(), array(), array(), array());
	foreach ($data->orderItems as $item)
	{
		$itms[0][] = $item->product->brand;
		$itms[1][] = $item->product->model;
		$itms[2][] = Product::variation_r($item->variation);
		$itms[3][] = $item->stock_nos; // no need to format
	}
	$itemSep = $isCsv ? PHP_EOL : '<br />';
	for ($i = 0; $i <= 3; $i++)
		$itms[$i] = implode($itmSep, $itms[$i]);
	
	$id = $isCsv ? $data->id : CHtml::link($data->id, array('/orders/' . $data->id));
	$add = $data->billingAddress; // since this cannot be the office address
	
	$cells = array($id, Formatter::dateCsv($data->order_date, $isCsv), $data->user->name, $data->user->email, $data->user->employee_id,
		$itms[0], $itms[1], $itms[3], //$itms[2], 
		Formatter::displayObject($data->despatcher, ''), $data->formatDespatcherRef(),
		$add->phone, $add->cityOrPincode(), $data->formatStatus(), other_orders($data, $isCsv)
	);
}

if ($isCsv)
{
	if ($isHeader) header(sprintf('Content-Disposition: attachment; filename=%s-orders-%s-%s.csv', 
		UserIdentity::Context('buyerName'), date('d-M-Y'), 
		str_replace(' ', '-', strtolower(AppEnv::name())) ));
	echo implode(',', $isHeader ? $cols : $cells) . PHP_EOL;

	if ($index == $total - 1) die('');
	return;
}
?>

<tr<?php Formatter::altRow($index); ?>>
<?php if ($isHeader) { ?>
	<th><?php echo implode('</th>' . PHP_EOL . '<th>', $cols); ?></th>
<?php } else { ?>
	<td><?php echo implode('</td>' . PHP_EOL . '<td>', $cells); ?></td>
<?php } ?>
</tr>
