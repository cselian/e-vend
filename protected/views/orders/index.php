<?php
$this->breadcrumbs=array(
	'Orders',
);

$this->menu = MenuHelper::Menu();
?>

<h1>Orders</h1>

<?php if ($actions) FlashMessages::displayActions('/orders/index/', $actions['items'], $actions['selected']); ?>

<table class="grid">
<?php
$this->renderPartial('_view', array('index' => false));
$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
));
?>
</table>
