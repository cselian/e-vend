<?php
$this->breadcrumbs=array(
	'orders',
);

$this->menu= MenuHelper::Menu();
?>

<h1>Orders on the Map</h1>

<div id="map" style="height: 450px; border: double 2px #ccc">Loading Map...</div>
<?php
MapHelper::addHome();
$pts = $dataProvider->getData();
$fmt = '<a href="' . Yii::app()->request->baseUrl . '/orders/%s">%s</a>';
foreach ($pts as $itm)
{
	$add = $itm->address != null ? $itm->address : $itm->buyer;
	$pt = array('id' => $itm->id, 'name' => $add['name'], 'lat' => $add['lat'], 'lon' => $add['lon'], 'address' => $add['address'], 'pin' => $add['pin']);
	if ($itm->address != null) $pt['name'] = sprintf("%s (Buyer: %s)", $pt['name'], $itm->buyer->name);
	MapHelper::addLocation($pt, $fmt, 'Order');
}

MapHelper::printMap();
?>
