<?php
$form=$this->beginWidget('CActiveForm', array(
	'id'=>'order-form',
	'enableAjaxValidation'=>false,
));

$address = $this->user_is('buyernonadmin') && $model->status == 1
	? CHtml::dropDownList('Order[address]', $model->address_id, $addresses)
	: $model->formatAddress();

function statusAction($status, $name, $disp = null)
{
	if ($disp == null) $disp = ucfirst($name) . ' Order';
	$cbname = 'Order[status_' . $name . ']';
	$status .= ' ' . CHtml::checkBox($cbname) . CHtml::label($disp, 'Order_status_' . $name);
	return $status;
}

$status = CHtml::label(AppLookups::OrderStatus($model->status), '', array('class' => 'first'));
$approval = $model->formatApproval();
$verification = $model->verification;
$despatcher = Formatter::displayObject($model->despatcher, '')
	. ($model->despatcher != null ? ' <br/>Address: ' . Address::formatEx($model->despatcher) : '');
$despatcherRef = $model->despatcher_reference;
$pytRef = $model->payment_ref;
$pytDate = Formatter::date($model->payment_date, 0);
$pytConf = Formatter::bool($model->payment_confirmed);

if ($this->user_is('buyernonadmin'))
{
	if ($model->status == 1)
	{
		$status = statusAction($status, 'cancel');
		if (!$model->isUnpaid())
			$status .= ' * <span class="small-note">' . AppEnv::get('cancellation-charge-note', 'cancellation-charge-note') . '</span>';
	}
	
	if ($model->payment_type == 3 && !$model->payment_confirmed) // NEFT
	{
		$pytRef = CHtml::textField('Order[payment_ref]', $model->payment_ref);
		$pytDate = AppHelper::DatePicker($this, $model, 'payment_date');
	}
}
else if ($this->user_is('distributor'))
{
	if ($model->status == 1)
		$status = statusAction($status, 'begin');
	
	if ($model->status == 1 || $model->status == 2)
		$status = statusAction($status, 'despatch');
	
	if ($model->status == 1 || $model->status == 2 || $model->status == 3)
	{
		$status = statusAction($status, 'deliver', 'Verify && Deliver');
		$verification = CHtml::textField('Order[verification]');
		$despatcherRef = CHtml::textField('Order[despatcher_reference]', $model->despatcher_reference);
	}
	
	if (!$model->payment_confirmed)
	{
		$pytConf = CHtml::checkBox('Order[payment_confirmed]');
		if ($model->payment_type == 2) // COD
		{
			$pytRef = CHtml::textField('Order[payment_ref]', $model->payment_ref);
			$pytDate = AppHelper::DatePicker($this, $model, 'payment_date');
		}
	}
}
else if ($this->user_is('superorbuyeradmin'))
{
	if ($this->user_is('superadmin') && $model->status == 1)
		$despatcher = CHtml::dropDownList('Order[despatcher_id]', $model->despatcher_id, Formatter::includeBlank(AppLookups::Distributors()));
	
	if ($model->needsApprovalOrRejection())
		$approval .= 
			 ' ' . CHtml::checkBox('Order[approved]') . CHtml::label('Approve Order', 'Order_approved')
			.' ' . CHtml::checkBox('Order[rejected]') . CHtml::label('Reject Order', 'Order_rejected');
}

$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'htmlOptions' => array('class' => 'detail-view viewandedit'),
	'attributes'=>array(
		'id',
		array('label'=>$model->getAttributeLabel('buyer_id'), 'type'=>'raw', 'value'=>Formatter::displayObject($model->buyer)),
		array('label'=>$model->getAttributeLabel('user_id'), 'type'=>'raw', 'value'=>User::link($model->user, $model->buyer)),
		array('label'=>$model->getAttributeLabel('address_id'), 'type'=>'raw', 'value'=>$address),
		array('label'=>$model->getAttributeLabel('order_date'), 'type'=>'raw', 'value'=>Formatter::date($model->order_date, 1)),
		array('label'=>$model->getAttributeLabel('amount'), 'type'=>'raw', 'value'=>Formatter::amount($model->amount)),
		array('label'=>$model->getAttributeLabel('verification'), 'type'=>'raw', 'value'=>$verification),
		array('label'=>$model->getAttributeLabel('status'), 'type'=>'raw', 'value'=>$status),
		array('label'=>$model->getAttributeLabel('approval'), 'type'=>'raw', 'value'=>$approval),
		array('label'=>$model->getAttributeLabel('despatched_date'), 'type'=>'raw', 'value'=>Formatter::date($model->despatched_date, 1)),
		array('label'=>$model->getAttributeLabel('despatcher_reference'), 'type'=>'raw', 'value'=> $despatcherRef),
		array('label'=>$model->getAttributeLabel('despatcher_id'), 'type'=>'raw', 'value'=>$despatcher),
		array('label'=>$model->getAttributeLabel('payment_type'), 'type'=>'raw', 'value'=>AppLookups::PaymentType($model->payment_type)),
		array('label'=>$model->getAttributeLabel('payment_ref'), 'type'=>'raw', 'value'=>$pytRef),
		array('label'=>$model->getAttributeLabel('payment_date'), 'type'=>'raw', 'value'=>$pytDate),
		array('label'=>$model->getAttributeLabel('payment_confirmed'), 'type'=>'raw', 'value'=>$pytConf),
	),
)); ?>

<?php if ($this->user_is('distributor')) { ?>
<br /><br />
<h2>Items</h2>

<table class="grid">
<?php
// dummy so that isset($_POST['Order']) passes even when there is only an updated stock number (usu onlu if entering after closing the order)
echo CHtml::hiddenField('Order[items]');
//print_r($model->orderItems);
$this->renderPartial('/orderItems/_edit');
$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>new CArrayDataProvider($model->orderItems),
	'itemView'=>'/orderItems/_edit',
	'viewData' => array('form' => $form),
	'template'=>'{items}'
));
?>
</table>
<?php } ?>

<div class="row buttons">
	<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
</div>

<?php $this->endWidget(); ?>
