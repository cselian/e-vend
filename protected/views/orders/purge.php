<?php
$this->breadcrumbs=array(
	'Orders'=>array('index'),
	$model->id,
);

if ($model != null) $this->menu= MenuHelper::Menu($model);

if ($model == null) echo 'The order doesnt exist. If it has already been purged, you can try to ';
echo CHtml::link('Download txt file', array('/orders/purge/' . $id . '?export=1')
	, array('target' => '_blank'));
if ($model != null) echo ' or ' . CHtml::link('Purge Order', array('/orders/purge/' . $id . '?delete=1'));
echo '<br /><br/>';

function buildAtts(&$op, $cap, $model, $atts)
{
	if ($op != '') $op .= PHP_EOL;
	if ($cap != '') $op .= $cap . PHP_EOL . str_pad('', strlen($cap), '=') . PHP_EOL;
	foreach ($atts as $a)
	{
		if (is_string($a)) $a = array('label'=>$model->getAttributeLabel($a), 'value'=> $model->$a);
		$op .= sprintf('%s: %s', $a['label'], $a['value']) . PHP_EOL;
	}
}

function buildItems(&$op, $items)
{
	$view = Controller::site_file('protected/views/orderItems/view.php');
	$attsOnly = 1;
	$ix = 0;
	foreach ($items as $model)
	{
		$ia = include $view;
		buildAtts($op, 'Order Item ' . ++$ix, $model, $ia);
	}
}

if ($canPurge)
{
	$op = '';
	$attsOnly = 1;
	$addr = '';
	$oa = include 'view.php';
	buildAtts($op, 'Order', $model, $oa);
	buildItems($op, $model->orderItems);
	buildAtts($op, 'User', $model->user, array('name', 'email', 
		array('label' => 'Date Added', 'value' => Formatter::date($model->user->date_added, 1)) ));
	echo str_replace("\r\n", "<br />\r\n" . PHP_EOL, $op);
	$this->purgeFile($model->id, $op);
}
?>
