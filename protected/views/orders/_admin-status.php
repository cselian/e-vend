<tr<?php Formatter::altRow($index); ?>>
<?php if (!isset($data)) { ?>
	<th>What*</th>
	<th>Virtual Status**</th>
	<th>Status</th>
	<th>Approval</th>
	<th>Payment</th>
<?php } else { ?>
	<td><?php echo $data->payment_details; ?></td>
	<td><?php echo $data->formatStatus(); ?></td>
	<td><?php echo $data->status . ' => ' . AppLookups::OrderStatus($data->status); ?></td>
	<td><?php echo $data->approved . ' => ' . $data->formatApproval(); ?></td>
	<td><?php echo Formatter::bool($data->payment_confirmed); ?></td>
<?php } ?>
</tr>
