Hi {name},

Order #{id} has been rejected by your administrator.
If you have already paid, you will be reimbursed within 48 hours.

In case of any problems with reimbursement, please {contact}.

Value: {value}
{details}

{signature}
