<?php
/**
 * @author Imran Ali Namazi <imran@cselian.com>
 * @link http://tg.cselian.com/showcase/e-vend
 * @copyright Copyright &copy; 2012-2013 cselian.com
 * @license http://tg.cselian.com/licenses/e-vend
 * Manages the variations by domains (environments) for a Multisite Install
 */

class AppEnv
{
	private static $initialized = false;
	
	private static function init($dom)
	{
		self::$initialized = true;
		$copy = array(
			'beta.intermart.in' => array('txn' => 'IMBTA-', 'from' => 'localhost', 'nonProduction' => 1),
			'demo.intermart.in' => array('txn' => 'IMDMO-', 'from' => 'localhost', 'nonProduction' => 1),
			// Production Dbs. Remove NP when going live
			'shop.intermart.in' => array('txn' => 'IMSHP-', 'from' => 'nokia.intermart.in', 'nonProduction' => 1),
		);
		if (isset($copy[$dom]))
		{
			$c = $copy[$dom];
			self::$data[$dom]['vpc'] = self::$data[$c['from']]['vpc'];
			self::$data[$dom]['vpc']['txnPrefix'] = $c['txn'];
			if (isset($c['nonProduction'])) self::$data[$dom]['nonProduction'] = 1;
		}
		
		// TODO: exclude these in demo site & shop
		//self::$data[$dom]['siteLogo'] = 'nokia.jpg'; // not suitable on dark menu
		if ($dom == 'beta.intermart.in' || $dom == 'nokia.intermart.in')
			self::$data[$dom]['siteBodyCss'] = 'body-nokia';
		
		self::$data[$dom]['deliveryNotice'] = 'Delivery will be made in 4-5 working days on payment receipt.';
		self::$data[$dom]['default_distributor'] = 1;
		self::$data[$dom]['vpc']['axis']['url'] = 'https://migs.mastercard.com.au/vpcpay';
		if (isset(self::$data[$dom]['vpc']['amex'])) {
		self::$data[$dom]['vpc']['amex']['url'] = 'https://vpos.amxvpos.com/vpcpay';
		self::$data[$dom]['vpc']['amex']['capture'] = array('url' => 'https://vpos.amxvpos.com/vpcdps',
			'username' => 'aW50ZXJtYXJ0YW1h', 'password' => 'MHBhc3N3b3Jk');
		}
		
		//self::$data[$dom]['smtp'] = array('host' => 'smtp.gmail.com', 'username' => 'intermart@cselian.com', 'password' => 'int3rm@rty123', 'port' => 465, 'encryption' => 'ssl');
		self::$data[$dom]['smtp'] = array('host' => 'mail.intermart.in', 'username' => 'noreply@intermart.in', 'password' => 'int3rm@rty123', 'port' => 26); // TODO: ssl?
		self::$data[$dom]['bcc'] = array('dhanapal.aikon@gmail.com');
		
		// What to show to sponsored buyers (flag is present)
		self::$data[$dom]['sponsored'] = array(
			'message' => '<div style="background-color: #c9e7f7; padding: 5px; margin-top: 8px;">This purchase is sponsored by your company. You can select only 1 product with a quantity of 1 (using the radio button)</div>',
			'restrictItems' => 1,
			'restrictQuantity' => 1,
			'vpcnames' => array('amex'),
			'note' => '<b>Note</b>: You are allowed to purchase only one device as per PDA Program. Please do not place additional orders. Terms & conditions apply for placing additional orders.'
		);
		
		self::$data[$dom]['purge-note'] = 'As per your company\'s policy, your account and history of this order <b>will be completely removed</b> as soon as processing begins, after payment is confirmed. Once your order is deleted, you will not be able to login and will have to call {name} at {phone} or email <a href="mailto:{email}">{email}</a> and mention your order number for any clarifications. Or contact Maqsood at 09901450004 / <a href="mailto:maqsooda@nokiaenterprise.co.in">maqsooda@nokiaenterprise.co.in</a> or Manjunath at 096200311225 / <a href="mailto:manjunathts@nokiaenterprise.co.in">manjunathts@nokiaenterprise.co.in</a>';
		self::$data[$dom]['no-activation-note'] = 'No email activation has been sent since your data will not be stored on our site after processing as per our agreement with your Employer. You are now automatically logged in.';
		self::$data[$dom]['cancellation-charge-note'] = '* Cancellation Charges will apply since payment is made';
		
		// Nokia-specific stuff (TODO: exclude in demo)
		self::$data[$dom]['contact'] = 'Please contact our Nokia MIS Team at <a href="mailto:info@nokiaenterprise.co.in">info@nokiaenterprise.co.in</a>.';
		self::$data[$dom]['mail-approval'] = 'You will receive the payment link once Your PDA team approves the order.';
		
		if ($dom == 'localhost') self::$data[$dom]['gii'] = 1;
		
		self::initDebug($dom);
	}

	private static function initDebug($dom)
	{
		if ($dom !== 'localhost') return;

		$machine = gethostname();
		if ($machine == 'TWGC01A433')
		{
			//self::$data[$dom]['testEmail'] = 'imran@cselian.com';
			self::$data[$dom]['testEmail'] = 'noreply@intermart.in';
			self::$data[$dom]['db'] = array('connectionString' => 'mysql:host=localhost;dbname=wp_ev', 'username' => 'root', 'password' => '');
		}
	}

	private static $data = array(
		'localhost' => array(
			'name' => 'Intermart Dev'
			, 'code' => 'dev'
			, 'testEmail' => 'aditya@digitalchakra.in'
			, 'db' => array('connectionString' => 'mysql:host=localhost;dbname=intermart', 'username' => 'root', 'password' => 'password')
			, 'vpc' => array(
					'axis' => array('secret' => '21FA48BDEAC79B070FCC525478623FC1', 'merchant' => 'TESTAIKONCL', 'access' => '014097A2', 'name'=> 'Axis Bank'),
					'amex' => array('secret' => '2D7201F18A09698FF0437D4A0AB59AB1', 'merchant' => 'TEST9820029775', 'access' => '49DFEED3', 'name'=> 'Amex'),
					'names' => array('axis', 'amex'), 'txnPrefix' => 'IMDEV-', 'factor' => 0.002, 'round' => 1,
					'testMsg' => 'This is a TEST Environment. Values sent should be under 1000, so multiplying by .002 so that orders upto 5 lakhs can be tested.<br/>Mastercard Card Number: 5123456789012346 - 05/13(Exp.)<br/>Amex Card Number: 345678000000007 - 05/13(Exp.) 4DBC: 0773',
				)
			, 'nonProduction'=> 1
			, 'default_distributor' => 1
			, 'default_buyer' => 1
			, 'buyerConfig' => array(1 => array('simplepricing' => 1, 'removeaccessories' => 1, 'employee_id' => 1))
		),
		'beta.intermart.in' => array(
			'name' => 'Intermart Beta'
			, 'code' => 'beta'
			, 'testEmail' => 'nokia-testers@cselian.com'
			, 'db' => array('connectionString' => 'mysql:host=localhost;dbname=consumax_imStaging', 'username' => 'consumax_user', 'password' => 'y0us33r')
			  // vpc is copied from local
			, 'buyerConfig' => array(4 => array('simplepricing' => 1, 'removeaccessories' => 1, 'employee_id' => 1)) // deloitte
		),
		'demo.intermart.in' => array(
			'name' => 'Intermart Demo'
			, 'code' => 'demo'
			, 'demoLoginMsg' => '<b class="demo">Use demo / demo for superadmin.</b>'
			, 'superadmins' => array('demo' => 'demo')
			, 'testEmail' => 'SELF'
			, 'db' => array('connectionString' => 'mysql:host=localhost;dbname=consumax_imDemo', 'username' => 'consumax_user', 'password' => 'y0us33r')
			  // vpc is copied from local
			, 'buyerIconsFol' => '/demo'
		),
		'nokia.intermart.in' => array(
			'name' => 'Nokia Intermart'
			, 'code' => 'nokia'
			, 'superadmins' => array('nokia' => 'nookie123')
			, 'db' => array('connectionString' => 'mysql:host=localhost;dbname=consumax_imNokia', 'username' => 'consumax_user', 'password' => 'y0us33r')
			, 'vpc' => array(
					'axis' => array('secret' => '52E17F68435CE6E9F8C7FDE2D055ED55', 'merchant' => 'AIKONCL', 'access' => '09F8BE77', 'name'=> 'Axis Bank'),
					'names' => array('axis', 'amex'), 'txnPrefix' => 'IMNOK-', 'round' => 1, //'factor' => 0.002, 'under' => 19,
					'amex' => array('secret' => 'E53A03541787B34FB24C65E3179629F3', 'merchant' => '9820029775', 'access' => '755949DD', 'name'=> 'Amex'),
					//'testMsg' => 'This is still a TEST Environment, but we are using the live codes of Axis. Real cards must be used. Am multiplying by .0002 to keep values low. Amex live codes are not yet available',
				)
			, 'buyerConfig' => array(2 => array('simplepricing' => 1, 'removeaccessories' => 1, 'employee_id' => 1)) // deloitte
			//, 'nonProduction'=> 1
		),
		'shop.intermart.in' => array(
			'name' => 'Intermart Shop'
			, 'code' => 'shop'
			,	'db' => array('connectionString' => 'mysql:host=localhost;dbname=consumax_imShop', 'username' => 'consumax_user', 'password' => 'y0us33r')
			, 'testEmail' => 'intermart-testers@cselian.com'
			  // vpc is copied from nokia
			, 'default_buyer' => 1
			, 'buyerIconsFol' => '/shop'
		),
	);
	
	// Helper function for Array
	public static function valOrDefault($array, $name, $default)
	{
		return isset($array[$name]) ? $array[$name] : $default;
	}

	public static function getArrayVal($var, $key, $name)
	{
		$arr = self::get($var, $name);
		return $arr ? $arr[$key] : false;
	}

	public static function get($var, $name, $default = null)
	{
		return self::getDomainVar($var, $name, $default);
	}

	public static function db()
	{
		return self::getDomainVar('db', 'Connection String');
	}
	
	public static function name()
	{
		return self::getDomainVar('name', 'Site Name', 'Intermart');
	}
	
	private static function getDomainVar($var, $friendly, $default = null)
	{
		$dom = $_SERVER['HTTP_HOST'];
		if ($dom == '72.29.75.99' || $dom == '127.0.0.1')
		{
			$scr = $_SERVER['SCRIPT_NAME'];
			if (self::contains($scr, 'staging'))
				$dom = 'beta.intermart.in';
			else
				$dom = 'nokia.intermart.in';
		}
		
		if (!isset(self::$data[$dom]))
		{
			if ($default !== null) return $default;
			throw new Exception(sprintf('Domain "%s" not configured!', $dom));
		}
		
		if (!self::$initialized) self::init($dom);
		$domData = self::$data[$dom];
		if (!isset($domData[$var]))
		{
			if ($default !== null) return $default;
			throw new Exception(sprintf('%s not configured for domain "%s"!', $friendly, $dom));
		}
		
		return $domData[$var];
	}
	
	private static function contains($haystack, $needle) { return gettype(strpos($haystack, $needle)) == "integer"; }
}
