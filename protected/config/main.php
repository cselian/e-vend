<?php
include_once 'AppEnv.php';
// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=> AppEnv::name(),

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		
		'gii'=> AppEnv::get('gii', 'Enable Code Generator', 0) ? array(
			'class'=>'system.gii.GiiModule',
			'password'=>'dim',
		 	// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		) : null,
		
	),
	
	'language' =>'en-US',

	// TODO: fix this problem when deploying
	//'onBeginRequest'=>array('Controller', 'onBeginApplicationRequest'), // To set the cache folder by environment -> is_callable(array("Controller", "onBeginApplicationRequest") calls include(46sp7nvdle.php)
	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
		'cache'=>array(
			'class'=>'system.caching.CFileCache'
		),
		// uncomment the following to enable URLs in path-format
		'urlManager'=>array(
			'urlFormat'=>'path',
			'showScriptName'=>false,
			'rules'=>array(
				//'etraders/<id:\d+>'=>'buyers/view',
				//'etraders/<action:\w+>'=>'buyers/<action>',
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
				'buyers/view/me' => 'buyers/view',
				'products/index/<type>' => 'products/index',
				//'api/buyers/<s:\d+>' => 'api/buyers',
			),
		),
		/*'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),*/
		// uncomment the following to use a MySQL database
		
		'db'=> array_merge(AppEnv::db(), array(
			'emulatePrepare' => true,
			'charset' => 'utf8',
		)),
		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
            'errorAction'=>'site/error',
        ),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
		'mail' => array(
			'class' => 'ext.yii-mail.YiiMail',
			'transportType' => 'smtp',
			'transportOptions' => AppEnv::get('smtp', 'Smtp Configuration', false),
			'viewPath' => 'application.views.mail',
			'logging' => true,
			'dryRun' => false
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=> 'dhanapal.aikon@gmail.com', //'mail@intermart.in',
		'testEmail' => AppEnv::get('testEmail', 'Test Email', false),
		'adminPhone' => '+91 76764 35016',
		'adminName' => 'Dhanapal',
		'address' => '#22 Sankey Road, High Grounds, Bangalore 560020',
		'copyright' => '&copy; %s Consumax Agencies, Bangalore,  All Rights Reserved.',
		'vatRate' => 5.5,
		'mapCenter' => array('lat' => 12.99142, 'lon' => 77.58610,
			'address' => '22, Sankey Road', 'pin' => '560 020'),
	),
);
