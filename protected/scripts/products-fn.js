var prodVarn = null; var prodQty = null;
var searchMsg = "model...";

function colourPopup($prod)
{
  $cbo = $("select", $prod);
  $div = $("#varndiv");
  $cbo = $("select", $prod);
  prodVarn = $(".varn", $prod);
  prodQty = $(".qty", $prod);

  $vals = null;
  if (prodVarn.val() != '')
    $vals = prodVarn.val().split(',');

  $html = '';
  $ix = -1; // why is it -1 indexed?
  $("option", $cbo).each(function()
  {
    if (this.value != 'mixed')
    {
      $val = $vals != null && $vals[$ix] != '0' ? $vals[$ix] : '';
      $html += '<input type="text" name="qty[]" class="price qty" maxlength="1" autocomplete="off" value="' + $val + '" id="mix-' + $ix + '" />' 
        + '<label for="mix-' + $ix + '">' + this.text + '</label><br/>';
    }
    $ix ++;
  });
  $html += '<input type="button" onclick="javascript:colourOk();" title="Press Enter in quantity field to accept changes" value="Ok" /><input type="button" onclick="javascript:colourCancel();" title="Press Escape in quantity field to cancel changes" value="Cancel" />';
  $div.html($html);
  
  $('.qty', $div)
    .keyup(function() {
      $(this).val($(this).val().replace(/[^\d]/, ''));
    })
    .keypress(function(e) { 
      if (e.which == 13) colourOk();
      if (e.which == 27) colourCancel();
    });;

  
  $div.css("top" , (prodQty.offset().top + 25) + "px");
  $div.css("left" , (prodQty.offset().left - 4) + "px");
  $div.show();
}

function colourOk()
{
  $div = $("#varndiv");
  $tot = 0;
  $tots = '';
  $("input[type='text']", $div).each(function()
  {
    $cnt = parseInt(this.value) || 0;
    $tot += $cnt;
    $tots += ($tots == "" ? "" : ",") + $cnt;
  });

  $(prodVarn).val($tots);
  $(prodQty).val($tot);
  $div.hide();
}

function colourCancel()
{
  $div = $("#varndiv");
  $div.hide();
}

var $nonNull; var $prdRadios;
function canSubmitProducts()
{
	$nonNull = false;
	if ($prdRadios)
	{
		if ($("input[name='singlepid']:checked").length > 0) {
			$nonNull = true;
		} else {
			alert('Please select a product using the radio button first.');
		}
		return $nonNull;
	}
	
	$(".qty").each(function(index, value)
	{
		$val = $(this).val();
		$ok = $val != '' && $val != '0';
		$nonNull = $nonNull || $ok;
	});
	if (!$nonNull) alert('Please enter quantity for atleast one product first.');
	return $nonNull;
}

function canSubmitSearch()
{
	$txt = $("#searchprod");
	if ($txt.val() == '' || $txt.val() == searchMsg)
	{
		alert('Please enter a model name');
		$txt.focus();
		return false;
	}
	return true;
}
