  // common
  $('#searchprod')
    .focus(function(){
      if ($(this).val() == searchMsg) $(this).val('');
      $(this).removeClass('txtdeflt');
    })
    .blur(function() {
      if ($(this).val() == '') { $(this).val(searchMsg);
      $(this).addClass('txtdeflt'); }
    });
  $('.qty').keyup(function() { // duplicated in colourPopup
    $(this).val($(this).val().replace(/[^\d]/, ''));
  });
  // tile view
  $(".product select").change(function () {
    $prod = $(this).parent();
    $qty = $(".qty", $prod);
    if ($(this).val() == "mixed")
    {
      colourPopup($(this).parent());
      $qty.attr('readonly','readonly');
    }
    else
    {
      $("#varndiv").hide();
      $qty.removeAttr('readonly');
    }
  });
  $(".product .qty").click(function (e) {
    e.stopPropagation();
    if ($(this).is('[readonly]'))
      colourPopup($(this).parent());
  });
  $(".product").click(function () {
    colourCancel();
  });
  // list view
  var $pvwDiv = $("#pvwdiv");
  $('a.pvwlink').mouseenter(function(e) {
      var src = $(this).attr("data-src");
      $pvwDiv.html('<img src="' + src + '" height="200" />');
      
      var off = $(this).parent().next().offset();
      $pvwTop = off.top + 1;
      if ($pvwTop - $(window).scrollTop() > $(window).height() / 2) $pvwTop -= $pvwDiv.height() - 5;
      
      $pvwDiv.css("top" , $pvwTop + "px");
      $pvwDiv.css("left" , (off.left + 1) + "px");
      
      $(this).parent().addClass("pvwcell");
      $pvwDiv.show();
    }).mouseleave(function() {
      $(this).parent().removeClass("pvwcell");
      $pvwDiv.hide();
      $pvwDiv.html("");
    });
