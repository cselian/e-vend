<?php
/**
 * @author Imran Ali Namazi <imran@cselian.com>
 * @link http://tg.cselian.com/showcase/e-vend
 * @copyright Copyright &copy; 2012-2013 cselian.com
 * @license http://tg.cselian.com/licenses/e-vend
 */

class OrdersController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		$this->menuOnTop();
		return $this->getAccessRules();
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$model = $this->loadModel($id);
		if (isset($_GET['mails']) || isset($_GET['mail']))
		{
			$mails = isset($_GET['mail']) ? array($_GET['mail']) 
				: array('created', 'approval', 'approved', 'distrib', 'paid');
			FlashMessages::AddCurrent('Test emails sent for: ' . implode(', ', $mails));
			foreach ($mails as $mail) AppMails::send('order-' . $mail, $model);
		}
		
		$this->render('view',array(
			'model'=>$model,
			'addr' => '', 'attsOnly' => false, // since called by pay.php
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionAdd()
	{
		$model=new Order;

		$cartItems = Yii::app()->session['cart'];
		if ($cartItems == null || count($cartItems) == 0)
			throw new CHttpException(400, "No cart items to add. Please go back and try again");
		
		$items = array(); $amt = 0;
		foreach ($cartItems as $i) {
			$data = array(
				'order_id' => 0,
				'product_id' => $i['id'],
				'quantity' => $i['qty'],
				'price' => $i['final'],
				'calculation' => $i['calc'],
				'variation' => $i['varn'],
			);
			$amt += $i['qty'] * $i['final'];
			$items[] = $data;
		}
		
		$order = new Order;
		$buyer = Buyer::model()->findByPk(UserIdentity::context('buyer'));
		$address = $_POST['address'] != '' ? Address::model()->findByPk($_POST['address']) : null;
		$order->attributes = array(
			'buyer_id' => UserIdentity::context('buyer'),
			'user_id' => UserIdentity::context('user'),
			'order_date' => date('Y-m-d H:i:s'),
			'status' => 1,
			'amount' => $amt,
			'address_id' => $_POST['address'],
			'billing_address_id' => $_POST['billingAddress'],
			'payment_type' => $_POST['payment_type'],
			'verification'=> AppHelper::generatePassword(4),
			'despatcher_id' => $address != null ? $address->assigned_to_id : $buyer->assigned_to_id, 
			'stock_nos' => '',
		);

		$approve = AppBuyers::isApprovalRequired();
		if ($approve) $order->approved = -1;
		
		if (!$order->save())
			print_r($order->getErrors());

		$orderId = $order->id;
		foreach ($items as $i)
		{
			$i['order_id'] = $orderId;
			$itm = new OrderItem;
			$itm->attributes = $i;
			if (!$itm->save($i))
				print_r($itm->getErrors());
		}
		
		unset(Yii::app()->session['cart']);
		FlashMessages::Add('Your Order has been placed and will be ' . ($approve ? 'approved' : 'delivered') . ' shortly. You should receive an e-mail now, confirming the order.');
		AppMails::send('order-created', $order, array('approval_required' => $approve));
		if ($approve) AppMails::send('order-approval', $order);
		else if ($order->despatcher_id != null) AppMails::send('order-distrib', $order);
		$this->redirect(array($order-payment_type == 1 ? 'pay' : 'view','id'=>$orderId));
	}

	public function actionPay($id)
	{
		$model=$this->loadModel($id);
		
		Yii::import('application.extensions.vpc.PaymentClient');
		PaymentClient::AddPayTestMessage();
		
		if ($model->payment_type != 1)
		{
			FlashMessages::Add('Redirected to View Order. You cannot view Online Payment details (/orders/pay/#nr) unless the payment type is Gateway.');
			$this->redirect(array('view','id'=>$id));
			return;
		}

		$data = array('model'=>$model);
		$isBuyer = $this->user_is('buyer');
		if ($isBuyer && isset($_GET['ret']) && $model->payment_ref == '')
		{
			unset($_GET['id']);
			unset($_GET['ret']); //affects hash calculation otherwise
			$pg = $_GET['pg'];
			unset($_GET['pg']);
			$ret = PaymentClient::GetReturnData($_GET, $pg);
			$det = PaymentClient::GetDetails($ret, $pg);

			$model->payment_details = serialize($det);
			
			if ($ret['hashOk'] && !$ret['errorExists'] && $ret['message'] == 'Approved')
			{
				$model->payment_ref = $ret['transactionNo']; // dont set if fails, used to show links
				$model->payment_confirmed = 1;
				AppStock::UpdateStock($model, $model->despatcher_id);
				AppMails::send('order-paid', $model);
				FlashMessages::AddCurrent('Your payment was succesful', 'success');
			}
			else
			{
				$model->payment_confirmed = 0;
				FlashMessages::AddCurrent(sprintf('%s <br/>Click %s to try again. Hash Status: %s.',
					$ret['message'], CHtml::link('here', array('/orders/pay/' . $id)), $ret['hashValidated']), 'error');
			}
			
			$model->save();
		}
		else if ($isBuyer && $model->isUnpaid())
		{
			$url = Yii::app()->createAbsoluteUrl('/orders/pay/' . $id) . '?ret=1';
			$pgs = PaymentClient::GetPGs();
			$links = array();
			foreach ($pgs as $key=>$name)
			{
				$img = CHtml::image(sprintf('%s/img/pg/%s.png', Yii::app()->baseurl, $key), $name, array('height' => 40));
				$links[] = CHtml::link($img, PaymentClient::GetSendUrl($model, $url . '&pg=' . $key, $key), array('title' => $name));
			}
			$data['payLink'] = '<span>Pay With</span> ' . implode(' <span>or</span> ', $links) . '.';
		}
		
		if ($this->user_is('superadmin') && $model->payment_details != '' && $model->payment_details != null)
		{
			$data['details'] = unserialize($model->payment_details);
		}
		
		$data['pgs'] = PaymentClient::GetPGs();
		$data['dummy'] = PaymentClient::GetDummyLinks();
		
		$this->render('pay', $data);
	}

	public function actionPurge($id)
	{
		if (isset($_GET['export']))
		{
			$file = $this->purgeFile($id);
			header(sprintf('Content-Disposition: attachment; filename=%s', $file['name']));
			echo $file['content'];
			return;
		}
		
		$model=$this->loadModel($id, 1);
		
		if (isset($_GET['delete']))
		{
			//$model = null;
			if ($model == null) {
				FlashMessages::AddCurrent('This order must have already been deleted.', 'error');
			} else {
				$others = $model->otherOrders();
				if ($others) {
					FlashMessages::AddCurrent(sprintf('Cannot delete since user has other order(s): %s.', implode(', ', $others)), 'error');
				} else {
					$deleted = array();
					$log = $this->site_file('protected/data/purged/order-'.$id.'.log');
					foreach ($model->orderItems as $itm) $deleted[] = AppHelper::deleteModel($itm, $log);
					$user = $model->user;
					$deleted[] = AppHelper::deleteModel($model, $log);
					foreach ($user->addresses as $itm) $deleted[] = AppHelper::deleteModel($itm, $log);
					$deleted[] = AppHelper::deleteModel($user, $log);
					FlashMessages::AddCurrent('Deleted: ' . implode(', ', $deleted));
					$model = null; // since just deleted
				}
			}
		}
		
		$can = $model != null && AppBuyers::canPurge($model->buyer_id);
		if ($model != null && !$can)
		{
			FlashMessages::AddCurrent(sprintf("This order cannot be purged. Please check the buyer configuration %s then %s."
					, CHtml::link('here', array('/buyers/update/' . $model->buyer_id))
					, CHtml::link('refresh the cache', array('/site/admin/?action=clear'))
			));
		}

		$this->render('purge',array(
			'id' => $id,
			'model'=>$model,
			'canPurge' => $can
		));
	}
	
	public function purgeFile($id, $data = null)
	{
		$name = sprintf('order-%s-%s.txt',
			$id,
			str_replace(' ', '-', strtolower(AppEnv::name())) );
		$file = $this->site_file('protected/data/purged/' . $name);
		if ($data)
			file_put_contents($file, $data);
		else
			return array('name' => $name, 'content' => file_get_contents($file));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		$errors = null;
		$messages = null;
		//print_r($_POST);
		
		if (isset($_POST['Order']))
		{
			$pb = $_POST['Order'];
			$setter = new FieldSetter;
			$this->_updateOrderItems($setter);
			
			$statuses = AppLookups::$orderStatuses;
			$statusFmt = 'Cannot %s Order if status is %s';
			//1 Created, 2 Under Processing, 3 Despatched, 4 Delivered, 9 Cancelled
			if ($this->user_is('buyernonadmin'))
			{
				if (isset($pb['address']) && $pb['address'] != $model->address_id)
					$model->address_id = $setter->getValue($model->address_id, $pb['address'], $model->status == 1, 'Address Changed', 'Cannot Change Address unless it is just created');
				
				if (isset($pb['status_cancel'])) {
					$model->status = $setter->getValue($model->status, 9, $model->status == 1, 'Order Cancelled', 'Cannot Cancel Order unless it is just created');
					AppStock::UpdateStock($model, $model->despatcher_id, 1);
				}

				if (isset($pb['payment_ref']))
					$model->payment_ref = $setter->getValue($model->payment_ref, $pb['payment_ref'], $model->payment_type == 3, 
						'Set Payment Reference for NEFT', 'Cannot Set Payment Reference unless type is NEFT');
				if (isset($pb['payment_date']))
					$model->payment_date = $setter->getValue($model->payment_date, Formatter::dateForSql($pb['payment_date']), $model->payment_type == 3, 
						'Set Payment Date for NEFT', 'Cannot Set Payment Date unless type is NEFT');
			}
			else if ($this->user_is('distributor'))
			{
				if (isset($pb['status_begin'])) {
					if (!$setter->addErrorIfOneOf('Order Begun', $model->status, array(2, 3, 4, 9), $statusFmt, 'Begin', $statuses))
						$model->status = 2;
				} else if (isset($pb['status_despatch'])) {
					if (!$setter->addErrorIfOneOf('Order Despatched', $model->status, array(3, 4, 9), $statusFmt, 'Despatch', $statuses))
					{
						$model->status = 3;
						$model->despatched_date = Formatter::dateForSql();
					}
				} else if (isset($pb['status_deliver'])) {
					$code = $pb['verification'];
					if ($model->verification != $code)
					{
						$setter->errors[] = 'Cannot Deliver Order unless verification code matches';
					}
					else
					{
						if (!$setter->addErrorIfOneOf('Order Delivered', $model->status, array(4, 9), $statusFmt, 'Deliver', $statuses))
						{
							$model->status = 4;
							if ($model->despatched_date == null) $model->despatched_date=Formatter::dateForSql();
						}
					}
				}
				
				if (isset($pb['despatcher_reference']))
				{
					$changed = $pb['despatcher_reference'] != $model->despatcher_reference;
					if ($changed) $model->despatcher_reference = $setter->getValue($model->despatcher_reference, $pb['despatcher_reference'], 1, 'Changed Despatcher Reference', null);
				}
				
				if (isset($pb['payment_confirmed']))
				{
					$changed = $pb['payment_confirmed'] != $model->payment_confirmed;
					if ($changed) $model->payment_confirmed = $setter->getValue($model->payment_confirmed, 1, 1, 'Changed Payment Confirmation', null);
				}
				
				if (isset($pb['payment_ref']))
					$model->payment_ref = $setter->getValue($model->payment_ref, $pb['payment_ref'], $model->payment_type == 2, 
						'Set Payment Reference for NEFT', 'Cannot Set Payment Reference unless type is NEFT');
				if (isset($pb['payment_date']))
					$model->payment_date = $setter->getValue($model->payment_date, Formatter::dateForSql($pb['payment_date']), $model->payment_type == 2, 
						'Set Payment Date for NEFT', 'Cannot Set Payment Date unless type is NEFT');
			}
			else if ($this->user_is('superorbuyeradmin'))
			{
				$mailDesp = 0;
				if (isset($pb['approved']) && $model->needsApprovalOrRejection())
				{
					$model->approved = $setter->getValue($model->approved, 1, 1, 'Approved order', null);
					AppMails::send('order-approved', $model);
					$mailDesp = 1;
				}
				if (isset($pb['rejected']) && $model->needsApprovalOrRejection())
				{
					$model->approved = $setter->getValue($model->approved, 0, 1, 'Rejected order', null);
					AppMails::send('order-rejected', $model);
					$mailDesp = 1;
				}

				if ($this->user_is('superadmin') && isset($pb['despatcher_id']) && $pb['despatcher_id'] != $model->despatcher_id)
				{
					if (AppStock::UpdateStock($model, $model->despatcher_id, 1))
						$setter->messages[] = 'Stocks added for old despatcher ' . AppLookups::Distributors($model->despatcher_id);

					if (AppStock::UpdateStock($model, $pb['despatcher_id']))
						$setter->messages[] = 'Stocks reduced for new despatcher ' . AppLookups::Distributors($pb['despatcher_id']);
					
					$model->despatcher_id = $setter->getValue($model->despatcher_id, $pb['despatcher_id'], 1, 'Changed Despatcher', null);
					$mailDesp = 1;
				}
				
				if ($mailDesp) AppMails::send('order-distrib', $model);
			}
			
			if (count($setter->errors) > 0) {
				$errors = implode(', ', $setter->errors);
			} else if(!$setter->dirty) {
				$messages = 'Nothing was changed';
			} else {
				$model->save();
				$messages = implode(', ', $setter->messages);
				FlashMessages::Add($messages);
				$this->redirect(array('view','id'=>$model->id));
			}
		}
		
		if ($this->user_is('buyer') && $model->status == 1)
		{
			$adds = array(null => UserIdentity::context('admin') ? 'Self' : 'Office');
			foreach ($model->buyer->addresses as $v)
				$adds[$v->id] = $v->name;
		}
		
		$this->render('update',array(
			'model'=>$model,
			'errors'=>$errors,
			'messages'=>$messages,
			'addresses' => $adds
		));
	}
	
	private function _updateOrderItems(&$setter)
	{
		$itms = $_POST['itmid'];
		$stocks = $_POST['stock_nos'];
		$items = array();

		$cnt = count($itms);
		$changed = array();
		for ($i = 0; $i < $cnt ; $i++) {
			$id = $itms[$i];
			$stk = $stocks[$i];
			$itm = OrderItem::model()->findByPk($id);
			if ($itm != null && $itm->stock_nos != $stk)
			{
				$changed[] = $id;
				$itm->stock_nos = $stk;
				if (!$itm->save())
					$setter->errors[] = sprintf('Order Item: %s, error: %s', $id, Formatter::errors($itm));
			}
		}
		
		if (count($changed) > 0)
		{
			$setter->dirty = 1;
			$setter->messages[] = sprintf('Changed Stock Nos for Items %s.', implode(', ', $changed));
		}
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Order');
		$cri = array('order' => 'id DESC');
		
		if (self::user_is('buyer'))
		{
			if (UserIdentity::context('admin')) {
				$cri['condition'] = 'buyer_id=' . UserIdentity::context('buyer');
				$url = '?approval=1';
				$actions = array('items' => array('' => 'All', $url => 'For Approval'), 'selected' => '');
				if ($_GET['approval'] == '1')
				{
					$cri['condition'] = $cri['condition'] . ' AND approved = -1 AND status !=9';
					$actions['selected'] = $url;
				}
			} else {
				$cri['condition'] = 'user_id=' . UserIdentity::context('user');
			}
		}
		if (self::user_is('distributor') || self::user_is('salesman')) $cri['condition'] = 'despatcher_id=' . UserIdentity::context('distributor');
		
		Order::includeArchival($cri);
		$dataProvider->setCriteria($cri);
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
			'actions' => isset($actions) ? $actions : 0,
		));
	}

	public function actionMap()
	{
		$dataProvider=new CActiveDataProvider('Order');
		if (self::user_is('distributor') || self::user_is('salesman')) $dataProvider->setCriteria(array('condition' => 'despatcher_id=' . UserIdentity::context('distributor')));
		// buyer cannot see
		MapHelper::register();

		$this->render('map',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Order('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Order']))
			$model->attributes=$_GET['Order'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id, $nullable = 0)
	{
		$model=Order::model()->findByPk($id);
		if($model===null && !$nullable)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='order-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
