<?php
/**
 * @author Imran Ali Namazi <imran@cselian.com>
 * @link http://tg.cselian.com/showcase/e-vend
 * @copyright Copyright &copy; 2012-2013 cselian.com
 * @license http://tg.cselian.com/licenses/e-vend
 */

class ApiController extends Controller
{
	private static $salesman;
	
	public function actionDealers($s)
	{
		$this->checkAccess($s);
		$model = new Buyer;
		$data = $model->findAll();
		foreach ($data as $i)
			echo implode(',', array($i->id, $i->name)) . '
';
	}

	public function actionProducts($s)
	{
		$this->checkAccess($s);
		$model = new Product;
		$data = $model->findAll();
		foreach ($data as $i)
			echo implode(',', array($i->id, $i->code, $i->model, $i->description, $i->price)) . '
';
	}

	private function checkAccess($s)
	{
		self::$salesman = User::model()->find("email = '" . $s . "'");
		if (self::$salesman == null) die("salesman not found");
		//print_r(self::$salesman->attributes);
	}
}
