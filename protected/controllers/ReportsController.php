<?php
/**
 * @author Imran Ali Namazi <imran@cselian.com>
 * @link http://tg.cselian.com/showcase/e-vend
 * @copyright Copyright &copy; 2012-2013 cselian.com
 * @license http://tg.cselian.com/licenses/e-vend
 */

class ReportsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		$this->menuOnTop();
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		$can = 0;
		$act = $this->getAction()->getId();
		/*if (AppReports::isAndAssumingDev()) { $can = 1; }
		else*/ if ($act == 'index') { $can = 1; }
		else if ($act == 'update' || $act == 'view') { $can = Controller::user_is('dev'); }
		else if ($act == 'run') {
			$slug = $_GET['name'];
			$report = AppReports::getReport($slug);
			if ($report == null) {
				FlashMessages::AddCurrent('Report "' . $slug . '" found. Go back to index');
			} else {
				$can = Report::canAccess($report);
				if (!$can) FlashMessages::AddCurrent('Report "' . $report->name . '" can be accessed only by "' . $report->accessible_to . '". Go back to index');
			}
		}
		
		return array(array( $can ? 'allow' : 'deny', 'users'=>array('*')));
	}
	
	function getAccessRules($returnList = 0)
	{
		if (AppReports::isAndAssumingDev())
			return array('index', 'admin', 'view', 'update', 'create', 'delete');
		return array('index'); 
	}

	public function actionRun($name)
	{
		if (FlashMessages::HasCurrent()) return;
		$report = AppReports::getReport($name);
		
		//print_r($report); die();
		$viewName = $report->returns;
		$cv = explode('/', $viewName);
		if (count($cv) == 1) $viewName .= '/_view';
		
		$mname = ucfirst(Formatter::Singular($cv[0]));
		$m = new $mname;
		$cond = $report->condition;
		foreach (AppLookups::$userTypeNames as $type)
		{
			if ($this->user_is($type))
				$cond = str_replace(sprintf('{%s}', $type), UserIdentity::context($type), $cond);
		}
		
		$items = $m->findAll($cond);
		
		AppReports::isInReport(1);
		$this->render('run', array(
			'dataProvider'=> new CArrayDataProvider($items),
			'viewName' => '/' . $viewName,
			'report' => $report
		));
	}
	
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Report;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Report']))
		{
			$model->attributes=$_POST['Report'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Report']))
		{
			$model->attributes=$_POST['Report'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Report');
		if (!AppReports::isAndAssumingDev())
		{
			$dataProvider->setPagination(false); // else will list only page 1
			$can = array();
			$items = $dataProvider->getData();
			foreach ($items as $item)
			{
				if (Report::canAccess($item))
					$can[] = $item;
			}
			$dataProvider=new CArrayDataProvider($can);
		}
		
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Report('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Report']))
			$model->attributes=$_GET['Report'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Report::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='report-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
