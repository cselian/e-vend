<?php
/**
 * @author Imran Ali Namazi <imran@cselian.com>
 * @link http://tg.cselian.com/showcase/e-vend
 * @copyright Copyright &copy; 2012-2013 cselian.com
 * @license http://tg.cselian.com/licenses/e-vend
 */

class BuyersController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return $this->getAccessRules();
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$model = $this->loadModel($id);
		$model->adjustPaymentTypes('view');
		$this->render('view',array(
			'model'=> $model,
		));
	}

	public function actionMe()
	{
		$this->actionView(UserIdentity::context('buyer'));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Buyer;
		$model->payment_types = '1';
		$model->assigned_to_id = Distributor::Deflt('id');
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Buyer']))
		{
			$model->attributes=$_POST['Buyer'];
			$model->adjustPaymentTypes('save');
			$model->date_added = Formatter::dateForSql();
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}
		else
		{
			$model->adjustPaymentTypes('edit');
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Buyer']))
		{
			$model->attributes=$_POST['Buyer'];
			$model->adjustPaymentTypes('save');
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}
		else
		{
			$model->adjustPaymentTypes('edit');
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->menuOnTop();
		$dataProvider=new CActiveDataProvider('Buyer');
		if (self::user_is('distributor') || self::user_is('salesman')) $dataProvider->setCriteria(array('condition' => 'assigned_to_id=' . UserIdentity::context('distributor')));
		$dataProvider->setPagination(false);
		
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	public function actionMap()
	{
		$dataProvider=new CActiveDataProvider('Buyer');
		if (self::user_is('distributor') || self::user_is('salesman')) $dataProvider->setCriteria(array('condition' => 'assigned_to_id=' . UserIdentity::context('distributor')));
		MapHelper::register();
		$this->render('map',array(
			'dataProvider'=>$dataProvider,
		));
	}

	public function actionMapOf($id = null)
	{
		if ($this->user_is('buyer')) $id = UserIdentity::context('buyer');
		
		$model=$this->loadModel($id);
		$points = array($model);
		$dis = array();
		if ($model->assigned_to_id != null)
		{
				$dis[$model->assigned_to_id] = 1;
				$points[] = $model->assignedTo;
		}
		foreach ($model->addresses as $itm)
		{
			$points[] = $itm;
			if ($itm->assigned_to_id != null && !isset($dis[$itm->assigned_to_id]))
			{
				$dis[$itm->assigned_to_id] = 1;
				$points[] = $itm->assignedTo;
			}
		}
		
		$dataProvider=new CArrayDataProvider($points);
		MapHelper::register();
		$this->render('map',array(
			'dataProvider'=>$dataProvider, 'buyer' => $model
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Buyer('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Buyer']))
			$model->attributes=$_GET['Buyer'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Buyer::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='buyer-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
