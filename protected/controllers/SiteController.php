<?php
/**
 * @author Imran Ali Namazi <imran@cselian.com>
 * @link http://tg.cselian.com/showcase/e-vend
 * @copyright Copyright &copy; 2012-2013 cselian.com
 * @license http://tg.cselian.com/licenses/e-vend
 */

class SiteController extends Controller
{
	public function filters()
	{
		return array('accessControl');
	}

	public function accessRules()
	{
		$action = $this->getAction()->getId();
		$cant = strtolower($action) == 'admin' && !$this->user_is('superadmin');
		return array(array( !$cant ? 'allow' : 'deny', 'users'=>array('*')));
	}

	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		if (Buyer::isEnteringSite())
		{
			$this->redirect(array('/products'));
		}
		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
	    if($error=Yii::app()->errorHandler->error)
	    {
	    	if(Yii::app()->request->isAjaxRequest)
	    		echo $error['message'];
	    	else
	        	$this->render('error', $error);
	    }
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		
		if (!isset($_POST['ContactForm']))
		{
			$model->name = Yii::app()->user->name;
			if (isset($_GET['email']))
				$model->email = $_GET['email'];
			else if ($this->user_is('loggedin'))
				$model->email = UserIdentity::context('userEmail');
		}
		
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				AppMails::send('contact', $model);
				//$headers="From: {$model->email}\r\nReply-To: {$model->email}";
				// Yii::app()->params['adminEmail']
				//mail('imran@cselian.com', AppEnv::name() . ' -> ' . $model->subject, $model->body, $headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
			{
				$this->redirect($this->getGotoUrl());
			}
		}
		
		$this->render('login',array('model'=>$model));
	}

	public function actionReset()
	{
		$model=new LoginForm;
		$step = 'begin';
		if (isset($_GET['k']))
		{
			$user = User::model()->find('email = "'.$_GET['e'].'" and reset_code = "'.$_GET['k'].'"');
			if ($user == null)
			{
				FlashMessages::AddCurrent(sprintf("Incorrect Email or Code. Perhaps you already reset with this key once? Please go back and %s again", 
					CHtml::link('reset password', array('/site/reset/'))));
				$this->render('empty');
				return;
			}
			else
			{
				// TODO: reset_date expiry
				$step = 'enter';
			}
		}
		
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			if ($step == 'enter')
			{
				$user->password = md5($model->password);
				$user->reset_code = null;
				$user->save();
				FlashMessages::AddCurrent(sprintf("Your password has been reset, kindly %s with it to proceed", 
					CHtml::link('login', array('/site/login/'))));
				$this->render('empty');
				return;
			}
			else if (($user = User::model()->find('email = "'.$model->email.'"')) == null)
			{
				FlashMessages::AddCurrent(sprintf("Email %s not found", $model->email));
			}
			else
			{
				$user->setResetCode();
				$user->save();
				AppMails::send('userreset', $user);
				FlashMessages::AddCurrent(sprintf("We have sent an email to %s with instructions on how to reset your password", $model->email));
				//$this->redirect(Yii::app()->homeUrl);
			}
		}
		
		$this->render('reset', array('model' => $model, 'resetstep' => $step));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		UserIdentity::clear();
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

	/**
	 * Admin page with all sorts of actions.
	 */
	public function actionAdmin()
	{
		$data = array();
		$act = null;
		
		if (isset($_GET['action']))
		{
			$act = $_GET['action'];
			if ($act == 'clear')
			{
				AppLookups::clearCache();
				$msg = 'Pricing, Distributor and Buyer Cache cleared';
			}
			else if ($act == 'clearreport')
			{
				AppReports::clearNamesCache();
				$msg = 'Report Names Cache cleared';
			}
			else if ($act == 'emptydb' || $act == 'emptyord')
			{
				if (!AppEnv::get('nonProduction', 'Is Production')) throw new Exception('Cannot empty a Production Database');
				$msg = AppHelper::EmptyDB($act == 'emptyord');
			}
			else if ($act == 'import_distributor' || $act == 'import_buyer' || $act == 'import_address')
			{
				$name = ucfirst(substr($act, 7));
				if (isset($_POST['mailimport'])) {
					$model = MailImporter::getModel($name, $_POST['mailimport']);
					if ($model->save()) {
						$msg = 'Saved ' . $name . ' #' . $model->id;
					}
					else
					{
						$data['error'] = CHtml::errorSummary($model);
						$data['textArea'] = array(
							'label' => 'and try again',
							'name' => 'mailimport',
							'submit' => 'Create ' . $name,
							'atts' => array('rows' => 12, 'cols' => '60'),
							'val' => $_POST['mailimport']
						);
					}
				} else {
					$data['action'] = $act;
					$sample = MailImporter::getSample($name);
					$hints = MailImporter::getHints($name);
					$link = sprintf('<a href="mailto:%s?subject=Inter-Mart New %s Details&body=%s" target="_new">click here</a>',
						Yii::app()->params['adminEmail'], $name, str_replace('
', '%0d%0A', $sample));
					$data['textArea'] = array(
						'label' => 'Fill the content of the textbox or ' . $link . ' to mail it and have it filled',
						'name' => 'mailimport',
						'submit' => 'Create ' . $name,
						'atts' => array('rows' => 12, 'cols' => '50'),
						'val' => $sample,
						'hints' => $hints);
					$msg = 'Showing input format for ' . $name;
				}
			}
			else if ($act == 'viewconfig')
			{
				$p = Yii::app()->params;
				$map = $p['mapCenter'];
				$data['config'] = array(
					'Site Name'=> Yii::app()->name,
					'Site Language'=> Yii::app()->language,
					'Default Distributor'=> array(Distributor::Deflt('link'), 'Set when adding buyer/address'),
					'Admin Email'=> array($p['adminEmail'], 'Used: contact, email import, from of system emails'),
					'Test Email' => array($p['testEmail'] ? $p['testEmail'] : 'NOT SET', 'Mails are sent here for testing'),
					'Address' => $p['address'],
					'Copyright' => '<small>' . sprintf($p['copyright'], date('Y')) . '</small>',
					'Vat Rate' => array($p['vatRate'] . '%', 'Price = DP - Disount + vat'),
					'Map Info' => array(sprintf('%s, %s. %s', $map['address'], $map['pin'], MapHelper::link($map)), 'Shows as Home on some maps in red'),
					//''=>'',
				);
			}
			else if ($act == 'vieworderstatuses')
			{
				$data['listData'] = array(
					'dataProvider'=>new CArrayDataProvider(Order::SampleStatuses()),
					'itemView'=>'/orders/_admin-status',
				);
				$data['listPostData'] = '*What are we trying to display<br/>** Computed Status, based on the condition of the remaining fields. When Status is not created (1), other fields are ignored!';
			}
			else
			{
				$err = 'Action ' . $act . ' undefined';
			}
			
			if (isset($err))
				FlashMessages::AddCurrent($err, 'error');
			else if (isset($msg))
				FlashMessages::AddCurrent($msg);
		}
		
		$actions = array(
			'clear' => 'Clear Cache of Pricing, Distributor and Buyer',
			'import_distributor' => 'Distributor Import via Direct Edit / Email',
			'import_buyer' => 'Buyer Import via Direct Edit / Email',
			'import_address' => 'Buyer Address Import via Direct Edit / Email',
			'viewconfig' => 'View Configurations',
			'vieworderstatuses' => 'View Statuses of Sample Orders',
			);
		if ($this->user_is('dev')) $actions['clearreport'] = 'Clear Cache of Report Names';
		if (AppEnv::get('nonProduction', 'Is Production', false))
		{
			$actions['emptydb'] = 'Empty the Non-production db';
			$actions['emptyord'] = 'Empty Orders, Users + Addresses from the Non-production db';
		}
			
		$data['actions'] = $actions;
		$data['action'] = $act;
		
		$this->render('admin', $data);
	}
}
