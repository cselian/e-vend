<?php
/**
 * @author Imran Ali Namazi <imran@cselian.com>
 * @link http://tg.cselian.com/showcase/e-vend
 * @copyright Copyright &copy; 2012-2013 cselian.com
 * @license http://tg.cselian.com/licenses/e-vend
 */

class ProductsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return $this->getAccessRules();
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$model = $this->loadModel($id);
		$stock = Controller::user_is('buyer') ? false : AppStock::displayStock($model);
			
		$model->adjustVariations('view');
		$this->render('view',array(
			'model'=> $model,
			'stock' => $stock,
			'isBuyer' => $this->user_is('buyer'),
			'radios' => $this->user_is('buyer') && AppBuyers::isSponsoredWithRadio(),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Product;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Product']))
		{
			$model->attributes=$_POST['Product'];
			if (AppImport::hasImport($this, $model))
			{
				if (AppImport::import($this, $model))
					$this->redirect(array('index'));
			}
			else
			{
				$model->adjustVariations('save');
				if($model->save())
					$this->redirect(array('view','id'=>$model->id));
			}
		}
		else
		{
			$model->adjustVariations('edit');
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Product']))
		{
			$model->attributes=$_POST['Product'];
			$model->adjustVariations('save');
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}
		else
		{
			$model->adjustVariations('edit');
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex($type = "all")
	{
		$this->menuOnTop();
		$cri = array('order' => 'model asc');
		$actions = array('' => 'All', 'smart' => 'Smart', 'accessories' => 'Accessories');
		
		if (AppBuyers::getBool('removeaccessories'))
			unset($actions['accessories']);
		
		if ($type == "smart") $cri['condition'] = "category = 'smart'";
		else if ($type == "accessories") $cri['condition'] = "category = 'accessories'";
		else $cri['condition'] = "category != 'accessories'";
		
		if ($type == "search") $cri['condition'] = "(model like '%". $_GET['s'] ."%')";
		if ($type == "search")
		{
			$type = 'search?s=' . $_GET['s'] . '#';
			$actions[$type] = 'Search';
		}
		
		if ($this->user_is('buyer') && $pids = AppBuyers::productCriteria())
			$cri['condition'] = (isset($cri['condition']) ? $cri['condition'] . ' AND ' : '') . $pids;
		
		$dataProvider=new CActiveDataProvider('Product');
		$dataProvider->setCriteria($cri);
		$dataProvider->setPagination(false);
		
		$data=$dataProvider->getData();
		
		$isBuyer = $this->user_is('buyer');
		if ($isBuyer) FlashMessages::AddCurrent('Click the quantity textbox next to the colours dropdown to enter multiple colours, or choose one from the dropdown and enter the quantity.' . AppBuyers::ifSponsoredMessage());
		
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
			'action' => $type,
			'actions' => $actions,
			'isTile' => $isBuyer,
			'isBuyer' => $isBuyer,
			'radios' => $this->user_is('buyer') && AppBuyers::isSponsoredWithRadio(),
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$this->menuOnTop();
		$model=new Product('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Product']))
			$model->attributes=$_GET['Product'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Product::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='product-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
