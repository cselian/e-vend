<?php
/**
 * @author Imran Ali Namazi <imran@cselian.com>
 * @link http://tg.cselian.com/showcase/e-vend
 * @copyright Copyright &copy; 2012-2013 cselian.com
 * @license http://tg.cselian.com/licenses/e-vend
 */

class AddressesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return $this->getAccessRules();
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$model = $this->loadModel($id);
		if ($this->user_is('buyer') && $model->buyer_id != UserIdentity::context('buyer'))
			throw new Exception("You can only view your own addresses");

		$this->render('view',array(
			'model'=> $model,
		));
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionDistances($id)
	{
		if ($_GET['type'] == 'buyer') {
			$model = Buyer::model()->findByPk($id);
		} else {
			$model = $this->loadModel($id);
			if ($this->user_is('buyer') && $model->buyer_id != UserIdentity::context('buyer'))
				throw new Exception("You can only view your own addresses");
		}
		
		$dists = Distributor::model()->findAll();
		$distances = array();
		$sorter = array();
		//print_r(count($dists)); die();
		foreach ($dists as $item)
		{
			$dist = MapHelper::getDistance($model, $item);
			$fake = floatval($dist . $model->id);
			$distances[$item->id] = MapHelper::getDistance_r($dist);
			$sorter[$fake] = $item;
		}
		sort($sorter, 1);

		$points = array();
		foreach ($sorter as $item) $points[] = $item;
		$dataProvider=new CArrayDataProvider($points);

		MapHelper::register();
		$this->render('map',array(
			'model'=> $model,
			'dataProvider'=>$dataProvider,
			'distances'=>$distances
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Address;
		$model->buyer_id = $this->user_is('buyer') ? UserIdentity::context('buyer') : $_GET['for'];
		$model->user_id = $this->user_is('buyer') ? UserIdentity::context('user') : null;
		$model->assigned_to_id = Distributor::Deflt('id');
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Address']))
		{
			$model->attributes=$_POST['Address'];
			$model->user_id = UserIdentity::context('user');
			if($model->save())
				$this->redirect(AppLookups::Cart('count') ? array('/cart') : array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Address']))
		{
			$model->attributes=$_POST['Address'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Address');
		if (self::user_is('buyer'))
		{
			$cri = 'user_id=' . UserIdentity::context('user');
			if (UserIdentity::context('admin'))
				$cri .= ' or (user_id=null and buyer_id=' . UserIdentity::context('buyer') . ')';

			$dataProvider->setCriteria(array('condition' => $cri));
		}
		
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Address('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Address']))
			$model->attributes=$_GET['Address'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Address::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='address-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
