<?php
/**
 * @author Imran Ali Namazi <imran@cselian.com>
 * @link http://tg.cselian.com/showcase/e-vend
 * @copyright Copyright &copy; 2012-2013 cselian.com
 * @license http://tg.cselian.com/licenses/e-vend
 */

class MailsController extends Controller
{
	public $layout='//layouts/column2';
	
	public function accessRules()
	{
		$can = $this->user_is('superadmin');
		return array(array( $can ? 'allow' : 'deny', 'users'=>array('*')));
	}

	public function actionIndex()
	{
		$this->menuOnTop();
		$items = Mail::findAll();
		$this->render('index', array(
			'dataProvider'=> new CArrayDataProvider($items),
		));
	}

	public function actionView($id)
	{
		$model = Mail::load($id, null, 'view');
		$this->render('view',array(
			'model'=>$model,
		));
	}

	public function actionCreate()
	{
		$model = new Mail;

		if(isset($_POST['Mail']))
		{
				$model->attributes=$_POST['Mail'];
				if($model->save())
				{
					$this->redirect(array('index'));
					return;
				}
		}
		else if (isset($_GET['name']))
		{
			$model->name = $_GET['name'];
			$model->loadBody('create');
		}
		$this->render('create',array('model'=>$model));
	}

	public function actionUpdate($id)
	{
		$model = Mail::load($id, null, 'edit');

		if(isset($_POST['Mail']))
		{
				$model->attributes=$_POST['Mail'];
				if($model->save())
				{
					$this->redirect(array('index'));
					return;
				}
		}
		$this->render('update',array('model'=>$model));
	}
}