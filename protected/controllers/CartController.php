<?php
/**
 * @author Imran Ali Namazi <imran@cselian.com>
 * @link http://tg.cselian.com/showcase/e-vend
 * @copyright Copyright &copy; 2012-2013 cselian.com
 * @license http://tg.cselian.com/licenses/e-vend
 */

class CartController extends Controller
{
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return $this->getAccessRules();
	}

	public function actionAdd()
	{
		if (isset($_POST['singlepid'])) {
			AppLookups::Cart(array()); // clear it
			$items = array($_POST['singlepid'] => array(1, '', ''));
		} else {
		if (!isset($_POST['pid'])) { $this->actionIndex(); return; }
		$pid = $_POST['pid'];
		$qty = $_POST['qty'];
		$varn = $_POST['varn'];
		$mixed = $_POST['mixed'];
		$items = array();

		$cnt = count($qty);
		for ($i = 0; $i < $cnt ; $i++) {
			if ($qty[$i] == "") continue;
			$items[$pid[$i]] = array($qty[$i], $varn[$i], $mixed[$i]);
		}
		} // end not singlepid
		
		//if ($this->officeEmail != null && isset($_POST['buyerName'])) $this->setOrderFor($_POST['buyerName']);
		$model = new Product;
		$sql = 'select * from products where id in ('.implode(", ", array_keys($items)).')';
		$data = $model->findAllBySql($sql);
		$ps = array(); foreach ($data as $i) $ps[$i->id] = $i;
		
		$data = AppLookups::Cart(); // existing items
		foreach ($items as $id=>$itm) {
			$p = $ps[$id];
			$new = array(
				'id'      => $id,
				'qty'     => $itm[0],
				'price'   => AppPricing::getComputed($p),
				'model'   => $p->model,
				'final'   => AppPricing::getComputed($p, 'final'),
				'calc'    => AppPricing::getComputed($p, 'calc'),
				//'options' => array('Size' => 'L', 'Color' => 'Red')
			);
			if ($itm[1] != 'mixed')
			{
				$new['varn'] = $itm[1];
				$data[$id . '-' . $new['varn']] = $new;
			}
			else
			{
				$mix = explode(',', $itm[2]);
				$varns = $p->adjustVariations('array');
				foreach ($mix as $ix=>$v)
				{
					if ($v == '0') continue;
					$new['varn'] = $varns[$ix];
					$new['qty'] = $v;
					$data[$id . '-' . $new['varn']] = $new;
				}
			}
		}
		
		AppLookups::Cart($data);
		$this->actionIndex();
	}

	public function actionIndex()
	{
		if (isset($_POST['edit']))
			$this->_edit();
		
		$buyer = Buyer::model()->findByPk(UserIdentity::context('buyer'));
		$adds = $buyer->myAddresses();
		
		FlashMessages::AddCurrent('Click on ' . CHtml::link('Products', array('/products')) . ' to go back and add more items.' . AppEnv::get('deliveryNotice', 'Delivery Notice') . AppBuyers::ifSponsoredMessage());
		
		$cart = AppLookups::Cart();
		$canBill = 1; $cantBillReason = ''; $billExcl = '';
		
		if (count($adds['bill']) == 0)
		{
			$canBill = 0;
			$billExcl = '<span class="excl">*</span>';
			// TODO: Office addresses in multi city buyers - billing address different from the office 
			$cantBillReason = sprintf('%s You need to have an address. Please  %s and then come back to the cart to proceed.', $billExcl, Controller::user_is('loggedin') ? CHtml::link('add one', array('/addresses/create')) : UserIdentity::signupOrLogin() . ' first');
		}
		
		if (AppBuyers::isSponsored())
		{
			$sponsoredRxn = array();
			$itemRxn = AppBuyers::sponsoredConfig('restrictItems');
			if ($itemRxn && count($cart) > $itemRxn)
				$sponsoredRxn[] = 'total order items must not exceed ' . $itemRxn;

			$qtyRxn = AppBuyers::sponsoredConfig('restrictQuantity');
			if ($qtyRxn)
			{
				foreach ($cart as $i)
				{
					if ($i['qty'] > $qtyRxn)
					{
						$sponsoredRxn[] = ($itemRxn === 1 ? 'item quantity' : 'all item quantities') . ' must not exceed ' . $qtyRxn;
						break;
					}
				}
			}

			if (count($sponsoredRxn) > 0)
			{
				$canBill = 0;
				$cantBillReason .= ($cantBillReason != '' ? '<br /><br />' : '')
					. sprintf('When purchase is sponsored by the company, the %s.', implode(' and the ', $sponsoredRxn) );
			}
		}
		
		$info = array('buyer' => $buyer, 'list' => $adds['obj'], 'caption' => $buyer->nullAddressCaption());
		$this->render('index', array('cart' => $cart, 'addressSidebarInfo' => $info,
			'addresses' => $adds['cbo'], 'userAddresses' => $adds['bill'], 'paymentTypes' => AppBuyers::PaymentTypes(),
			'canBill' => $canBill, 'cantBillReason' => $cantBillReason, 'billExcl' => $billExcl,
			'isGuest' => $this->user_is('guest'),
			'radios' => $this->user_is('buyer') && AppBuyers::isSponsoredWithRadio(),
			));
	}

	public function actionClear()
	{
		unset(Yii::app()->session['cart']);
		FlashMessages::Add('Your Cart has been emptied.');
		$this->redirect(array('/site/index'));
	}

	function _edit()
	{
		$list = AppLookups::Cart();
		$items = array();
		foreach ($list as $i) $items[$i['id'] . '-' . $i['varn']] = $i;
		
		$pid = $_POST['pid'];
		$qty = $_POST['qty'];
		$cnt = count($qty);
		for ($i = 0; $i < $cnt ; $i++) {
			$id = $pid[$i];
			if (!isset($items[$id])) continue; 
			if ($qty[$i] == $items[$id]['qty']) continue;
			$items[$id]['qty'] = $qty[$i];
		}
		
		if (isset($_POST['del']))
		{
			$del = $_POST['del'];
			$cnt = count($del);
			for ($i = 0; $i < $cnt ; $i++) {
				$id = $del[$i];
				if (isset($items[$id])) unset($items[$id]);
			}
		}
		
		//echo "<pre>" . print_r($items, 1) . "</pre>";
		
		// update seems to fail. lets use destroy
		AppLookups::Cart($items);
	}
}
