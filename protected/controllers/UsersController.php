<?php
/**
 * @author Imran Ali Namazi <imran@cselian.com>
 * @link http://tg.cselian.com/showcase/e-vend
 * @copyright Copyright &copy; 2012-2013 cselian.com
 * @license http://tg.cselian.com/licenses/e-vend
 */

class UsersController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return $this->getAccessRules();
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionMe()
	{
		$this->actionView(UserIdentity::context('user'));
	}

	public function actionSignup()
	{
		$model=new User;

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			$model->date_added = Formatter::dateForSql();
			$model->type = 3;
			
			$model->clearErrors(); // normally save clears errors
			$byr = UserIdentity::context('buyer');
			$purge = is_numeric($byr) && AppBuyers::canPurge($byr);
			// TODO: Setting for bypassing signupTo. For now, its linked
			if (!$purge) $byr =  AppBuyers::signupTo($model->email);
			if (is_numeric($byr)) {
				$model->buyer_id = $byr;
				if (!$purge) $purge = AppBuyers::canPurge($byr); // if we didnt know the buyer, try again
			} else { //if (!$this->user_is('buyer')) {
				$model->addError('buyer_id', $byr);
			}
			
			if ($purge) {
				$pwd = AppHelper::generatePassword(8, 1);
				$model->password = md5($pwd);
			} else {
				$model->setResetCode();
				$model->password = 'notset';
			}

			if($model->validate(null, false) && $model->save(false))
			{
				if ($purge) {
					$msg = AppEnv::get('no-activation-note', 'Note about No Activation email', "No email activation has been sent since your data will not be stored on our site after processing as per our Agreement with your Employer.");
					$login = new LoginForm;
					$login->attributes = array('email' => $model->email, 'password' => $pwd);
					if($login->validate() && $login->login()) {
						//FlashMessages::Add($msg);
						$this->redirect($this->getGotoUrl());
					} else {
						FlashMessages::AddCurrent(CHtml::errorSummary($login, 'Errors in User Authentication:'), 'error');
					}
				} else {
					$msg = sprintf("An email has been sent to '%s' to activate your account and set your password. Kindly check", $model->email);
					AppMails::send('usersignup', $model);
					FlashMessages::Add($msg);
					$this->redirect(array('/site/login'));
				}
			}
		}

		$this->render('signup',array(
			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($id = 1, $name = null, $for = null)
	{
		$model=new User;

		$model->type = $id;
		if ($model->type == 2)
			$model->distributor_id = $for;
		else if ($model->type == 3)
			$model->buyer_id = $for;

		if ($model->type == 3)
			FlashMessages::AddCurrent('Make sure you have added the pricing for the buyer first!<br /> 
To do this, impersonate buyer and visit the products page');
		

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			if (AppImport::hasImport($this, $model))
			{
				if (AppImport::import($this, $model))
					$this->redirect(array('index'));
			}
			else
			{
				$model->attributes=$_POST['User'];
				$model->date_added = Formatter::dateForSql();
				$pwd = AppHelper::generatePassword(8, 1);
				$model->password = md5($pwd);
				if($model->save())
				{
					$msg = sprintf("User '%s' of type %s created with password '%s'", $model->name, AppLookups::UserType($model->type, $model->buyer), $pwd); // TODO: Buyer should be known here. let param allow int / buyer obj since only int will be known here
					AppMails::send('usercreation', $model, array('pwd' => $pwd));
					FlashMessages::Add($msg);
					$this->redirect(array('view','id'=>$model->id));
				}
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	public function actionImpersonate($id, $what, $name, $type, $a)
	{
		UserIdentity::setContext($what, $id);
		UserIdentity::setContext($what . 'Name', $name);
		UserIdentity::setContext('admin', intval($a));
		
		Yii::app()->session['userType'] = $type;
		FlashMessages::Add(sprintf('%s #%s Impersonated', ucfirst($name), $id));
		$this->redirect(array('/' . $what . 's/me'));
	}
	
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$old = $model->deactive;
			$model->attributes=$_POST['User'];
			if ($old != $model->deactive)
				AppMails::send('user-' . ($old ? 're' : 'de') . 'activated', $model, array('reason' => $_POST['reason']));
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('User');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new User('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['User']))
			$model->attributes=$_GET['User'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=User::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
		
	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
